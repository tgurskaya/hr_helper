$('#decisionModal').on('show.bs.modal', function (e) {
    var applicationId = $(e.relatedTarget).data('application-id');
    $(e.currentTarget).find('input[name="applicationId"]').val(applicationId);
    var decision = $(e.relatedTarget).data('application-decision');
    $(e.currentTarget).find('input[name="decision"]').val(decision);
});