<%@ page contentType="text/html;charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<c:set var="language"
       value="${not empty param.language ? param.language : not empty language ? language : pageContext.request.locale}"
       scope="session"/>
<fmt:setLocale value="${language}" scope="session"/>
<fmt:setBundle basename="property.label" var="loc"/>
<fmt:message bundle="${loc}" key="label.obligatoryField" var="obligatoryfield"/>
<fmt:message bundle="${loc}" key="label.email" var="email"/>
<fmt:message bundle="${loc}" key="label.password" var="password"/>
<fmt:message bundle="${loc}" key="label.name" var="name"/>
<fmt:message bundle="${loc}" key="label.photo" var="photo"/>
<fmt:message bundle="${loc}" key="label.birthday" var="birthday"/>
<fmt:message bundle="${loc}" key="label.phone" var="phone"/>
<fmt:message bundle="${loc}" key="label.registration" var="registration"/>
<fmt:message bundle="${loc}" key="button.register" var="register"/>
<fmt:message bundle="${loc}" key="label.repeatPassword" var="repeatPassword"/>
<fmt:message bundle="${loc}" key="error.wrongPhone" var="wrongPhone"/>
<fmt:message bundle="${loc}" key="error.wrongName" var="wrongName"/>
<fmt:message bundle="${loc}" key="error.wrongEmail" var="wrongEmail"/>
<fmt:message bundle="${loc}" key="error.wrongPassword" var="wrongPassword"/>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <title>${registration}</title>
    <link href="../css/registration.css" rel="stylesheet" type="text/css"/>
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Open+Sans:400,700">
    <link rel="stylesheet" href="../css/bootstrap.min.css"/>
    <script src="../js/repeatPassword.js"></script>
    <script src="../js/bootstrap.js" type="text/javascript"></script>
    <script src="../js/jquery-3.2.1.min.js"></script>
</head>
<body>
<div class="container">
    <c:choose>
        <c:when test="${empty sessionScope.user}">
            <%@ include file="header.jsp" %>
        </c:when>
        <c:when test="${sessionScope.user.userType=='ADMIN'}">
            <%@ include file="headerAuthorized.jsp" %>
        </c:when>
        <c:otherwise>
           <jsp:forward page="error/error403.jsp"/>
        </c:otherwise>
    </c:choose>
    <h1 class="title">${registration}</h1>

    <c:choose>
        <c:when test="${not empty requestScope.message}">
            <h2 class="error">${requestScope.message}</h2>
        </c:when>
    </c:choose>
    <div id="login">
        <form name='form-login' method="POST" action="${pageContext.servletContext.contextPath}/controller"
              class="form">
            <c:choose>
                <c:when test="${empty sessionScope.user}">
                    <input type="hidden" name="command" value="register_candidate"/>
                </c:when>
                <c:when test="${sessionScope.user.userType=='ADMIN'}">
                    <input type="hidden" name="command" value="register_hr_manager"/>
                </c:when>
                <c:otherwise>
                    <jsp:forward page="error/error403.jsp"/>
                </c:otherwise>
            </c:choose>
            <div class="form__field">
                <input type="text" id="name" name="name" required
                       pattern="^[А-ЯA-Z][a-zA-ZА-Яа-я\-іў'\s]{4,50}$" placeholder="*${name}"/>
                <span class="form__error">${wrongName}</span>
            </div>
            <div class="form__field">
                <input type="date" name="birthday" id="birthday" min="1930-01-01" max="2000-01-01" required
                       placeholder="*${birthday}">
            </div>
            <div class="form__field">
                <input type="email" id="email" name="email" required
                       pattern="[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,4}$" placeholder="*${email}"/>
                <span class="form__error">${wrongEmail}</span>
            </div>
            <div class="form__field">
                <input type="text" id="phone" name="phone" pattern="[0-9\+\-]{4,20}$" placeholder="${phone}"/>
                <span class="form__error">${wrongPhone}</span>
            </div>
            <div class="form__field">
                <input type="password" id="password" name="password" required
                       pattern="(?=^.{8,45}$)((?=.*\d)|(?=.*\W+))(?![.\n])(?=.*[A-Z])(?=.*[a-z]).*$"
                       placeholder="*${password}"/>
                <span class="form__error">${wrongPassword}</span>
            </div>
            <div class="form__field">
                <input type="password" id="repeatPassword" name="repeatPassword"
                       required
                       pattern="(?=^.{8,45}$)((?=.*\d)|(?=.*\W+))(?![.\n])(?=.*[A-Z])(?=.*[a-z]).*$"
                       placeholder="*${repeatPassword}"/>
                <span class="form__error">${wrongPassword}</span>
            </div>
            <p>*-${obligatoryfield}</p>
            <input type="submit" value="${register}">
        </form>
    </div>
</div>
</body>
</html>