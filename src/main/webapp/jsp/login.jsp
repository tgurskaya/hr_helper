<%@ page contentType="text/html;charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<c:set var="language" value="${not empty param.language ? param.language : not empty language ? language : pageContext.request.locale}"
       scope="session"/>
<fmt:setLocale value="${language}" scope="session"/>
<fmt:setBundle basename="property.label" var="loc"/>
<fmt:message bundle="${loc}" key="label.email" var="email"/>
<fmt:message bundle="${loc}" key="label.password" var="password"/>
<fmt:message bundle="${loc}" key="label.authorization" var="authorization"/>
<fmt:message bundle="${loc}" key="error.wrongEmail" var="wrongEmail"/>
<fmt:message bundle="${loc}" key="error.wrongPassword" var="wrongPassword"/>
<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <title>${authorization}</title>
    <link href="../css/registration.css" rel="stylesheet" type="text/css"/>
    <link href="../css/bootstrap.css" rel="stylesheet" type="text/css"/>
    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Open+Sans:400,700">
    <link rel="stylesheet" href="../css/bootstrap.min.css" />
    <script src="../js/bootstrap.js" type="text/javascript"></script>
    <script src="../js/jquery-3.2.1.min.js"></script>
</head>
<body>


<%@ include file="header.jsp" %>
<c:choose>
    <c:when test="${not empty message}">
        <div class="title"><h1>${message}</h1></div>
    </c:when>
</c:choose>
<div id="login">
    <form name='form-login' method="POST" action="${pageContext.servletContext.contextPath}/controller">
        <input type="hidden" name="command" value="login"/>
        <div class="form__field">
            <input type="email" id="email" name="login" required
                   pattern="[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,4}$" placeholder="*${email}"/>
            <span class="form__error">${wrongEmail}</span>
        </div>
        <div class="form__field">
            <input type="password" id="password" name="password" required
                   pattern="(?=^.{8,}$)((?=.*\d)|(?=.*\W+))(?![.\n])(?=.*[A-Z])(?=.*[a-z]).*$" placeholder="*${password}"/>
            <span class="form__error">${wrongPassword}</span>
        </div>
        <input type="submit" value="${login}">
    </form>
</div>
</body>
</html>
