<%@ page contentType="text/html;charset=UTF-8" session="true" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<c:set var="language"
       value="${not empty param.language ? param.language : not empty language ? language : pageContext.request.locale}"
       scope="session"/>
<c:set var="currentPage" value="${(pageNumber==null) ? 1 : pageNumber}"/>
<c:set var="lastPage" value="${(pageCount==null) ? 1 : pageCount}"/>
<fmt:setLocale value="${language}" scope="session"/>
<fmt:setBundle basename="property.label" var="loc"/>
<fmt:message bundle="${loc}" key="label.changeProfile" var="changeProfile"/>
<fmt:message bundle="${loc}" key="label.apply" var="apply"/>
<fmt:message bundle="${loc}" key="label.vacancyName" var="vacancyName"/>
<fmt:message bundle="${loc}" key="label.description" var="description"/>
<fmt:message bundle="${loc}" key="label.requirements" var="requirements"/>
<fmt:message bundle="${loc}" key="label.responsibilities" var="responsibilities"/>
<fmt:message bundle="${loc}" key="label.salary" var="salary"/>
<fmt:message bundle="${loc}" key="label.type" var="type"/>
<fmt:message bundle="${loc}" key="button.blockUser" var="blockUser"/>
<fmt:message bundle="${loc}" key="button.unblockUser" var="unblockUser"/>
<html>
<head>
    <link href="${pageContext.servletContext.contextPath}/css/bootstrap.css" rel="stylesheet" type="text/css"/>
    <link href="${pageContext.servletContext.contextPath}/css/bootstrap.min.css" rel="stylesheet" type="text/css">
    <link href="${pageContext.servletContext.contextPath}/font-awesome-4.7.0/css/font-awesome.min.css" rel="stylesheet" type="text/css"/>
    <script src="${pageContext.servletContext.contextPath}/js/jquery-3.2.1.min.js"></script>
    <script src="${pageContext.servletContext.contextPath}/js/bootstrap.min.js" type="text/javascript"></script>
    <link href="${pageContext.servletContext.contextPath}/css/all_users.css" rel="stylesheet" type="text/css">
    <title>${vacancy}</title>
</head>
<body>
<c:choose>
    <c:when test="${(not empty sessionScope.user)and (sessionScope.user.userType=='ADMIN') }">
    <div class="container">
        <div id="wrapper">
            <div class="overlay"></div>
            <%@ include file="../leftMenu.jsp" %>
            <div id="page-content-wrapper">
                <button type="button" class="hamburger is-closed" data-toggle="offcanvas">
                    <span class="hamb-top"></span>
                    <span class="hamb-middle"></span>
                    <span class="hamb-bottom"></span>
                </button>
                <div class="container">
                    <div class="row">
                        <c:if test="${requestScope.userList.size() > 0}">
                            <c:forEach var="i" begin="0" end="${requestScope.userList.size()-1}">
                                <div class="col-md-4">
                                    <div class="panel block-profile">
                                        <div style="background-image: url('http://bootstraptema.ru/images/bg/bg-4.png'); height:500px;"
                                             class="panel-body text-center bg-center">
                                            <div class="row profile-name">
                                                <div class="col-xs-12 text-white">
                                                    <ctg:image userId="${requestScope.userList.get(i).id}"/>
                                                    <h3>${requestScope.userList.get(i).name}</h3>
                                                </div>
                                            </div>

                                            <div class="list-group">
                                                <div class="list-group-item">
                                                    <span class="label label-danger pull-right">${requestScope.userList.get(i).phone}</span>
                                                    <em class="fa fas fa-phone text-muted"></em>${phone}</div>
                                                <div class="list-group-item">
                                                    <span class="label label-danger pull-right">${requestScope.userList.get(i).email}</span>
                                                    <em class="fa fa-fw fa-globe text-muted"></em>${email}</div>
                                                <div class="list-group-item">
                                                    <span class="label label-danger pull-right">${requestScope.userList.get(i).birthday}</span>
                                                    <em class="fa fas fa-birthday-cake text-muted"></em>${birthday}
                                                </div>
                                                <div class="list-group-item">
                                                    <span class="label label-danger pull-right">${requestScope.userList.get(i).userType}</span>
                                                    <em class="text-muted"></em>${type}
                                                </div>
                                                <div class="list-group-item">
                                                    <c:choose>
                                                    <c:when test="${requestScope.userList.get(i).blocked}">
                                                    <a class="btn btn-block btn-danger"
                                                       href="${pageContext.servletContext.contextPath}/controller?command=unblock_user&userId=${requestScope.userList.get(i).id}">${unblockUser}</a>
                                                    </c:when>
                                                        <c:otherwise>
                                                            <a class="btn btn-block btn-danger"
                                                               href="${pageContext.servletContext.contextPath}/controller?command=block_user&userId=${requestScope.userList.get(i).id}">${blockUser}</a>

                                                        </c:otherwise>
                                                    </c:choose>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </c:forEach>
                        </c:if>
                    </div>

                    <nav aria-label="Page navigation example">
                        <ul class="pagination justify-content-center">
                            <c:choose>
                                <c:when test="${currentPage==1}">
                                    <li class="page-item disabled">
                                        <a class="page-link" href="#" aria-label="Previous">
                                            <span aria-hidden="true">&laquo;</span>
                                            <span class="sr-only">Previous</span>
                                        </a>
                                    </li>
                                </c:when>
                                <c:otherwise>
                                    <li class="page-item">
                                        <a class="page-link"
                                           href="controller?command=all_users&pageNumber=${currentPage-1}"
                                           aria-label="Previous">
                                            <span aria-hidden="true">&laquo;</span>
                                            <span class="sr-only">Previous</span>
                                        </a>
                                    </li>
                                </c:otherwise>
                            </c:choose>
                            <c:forEach begin="${(currentPage-3<0) ? 1 : currentPage-1}"
                                       end="${(currentPage+1>pageCount) ? pageCount : currentPage+1}" var="i">
                                <c:choose>
                                    <c:when test="${currentPage eq i}">
                                        <li class="page-item active">
                                            <a class="page-link"
                                               href="controller?command=all_users&pageNumber=${i}">${i}</a>
                                        </li>
                                    </c:when>
                                    <c:otherwise>
                                        <li class="page-item">
                                            <a class="page-link"
                                               href="controller?command=all_users&pageNumber=${i}">${i}</a>
                                        </li>
                                    </c:otherwise>
                                </c:choose>
                            </c:forEach>
                            <c:choose>
                                <c:when test="${currentPage eq lastPage}">
                                    <li class="page-item disabled">
                                        <a class="page-link" href="#" aria-label="Next">
                                            <span aria-hidden="true">&raquo;</span>
                                            <span class="sr-only">Next</span>
                                        </a>
                                    </li>
                                </c:when>
                                <c:otherwise>
                                    <li class="page-item">
                                        <a class="page-link"
                                           href="controller?command=all_users&pageNumber=${currentPage+1}"
                                           aria-label="Next">
                                            <span aria-hidden="true">&raquo;</span>
                                            <span class="sr-only">Next</span>
                                        </a>
                                    </li>
                                </c:otherwise>
                            </c:choose>
                        </ul>
                    </nav>
                </div>

            </div>
        </div>
    </div>
    </c:when>
    <c:otherwise>
        <jsp:forward page="${pageContext.servletContext.contextPath}/error/error403.jsp"/>
    </c:otherwise>
</c:choose>
</body>
</html>

