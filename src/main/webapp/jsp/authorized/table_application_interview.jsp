<%@ page contentType="text/html;charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="ctg" uri="customtags" %>
<c:set var="jobInterviewList" value="${requestScope.jobInterviewList}" scope="page"/>
<c:set var="currentPage" value="${(requestScope.pageNumber==null) ? 1 : requestScope.pageNumber}" scope="page"/>
<c:set var="lastPage" value="${(requestScope.pageCount==null) ? 1 : requestScope.pageCount}" scope="page"/>
<c:set var="language"
       value="${not empty param.language ? param.language : not empty language ? language : pageContext.request.locale}"
       scope="session"/>
<fmt:setBundle basename="property.label" var="loc"/>
<fmt:message bundle="${loc}" key="label.newJobInterview" var="newJobInterview"/>
<fmt:message bundle="${loc}" key="label.opinion" var="opinion"/>
<fmt:message bundle="${loc}" key="label.dateTimeOfInterview" var="dateTimeOfInterview"/>
<fmt:message bundle="${loc}" key="label.dateTimeOfInterview" var="dateTimeOfInterview"/>
<fmt:message bundle="${loc}" key="label.jobInterviewType" var="jobInterviewType"/>
<fmt:message bundle="${loc}" key="label.place" var="place"/>
<fmt:message bundle="${loc}" key="button.changeJobInterview" var="changeJobInterview"/>
<fmt:message bundle="${loc}" key="button.deleteJobInterview" var="deleteJobInterview"/>
<fmt:message bundle="${loc}" key="label.applicationInterviews" var="applicationInterviews"/>
<fmt:message bundle="${loc}" key="label.place" var="place"/>
<fmt:message bundle="${loc}" key="button.addJobInterview" var="addJobInterview"/>
<fmt:message bundle="${loc}" key="label.noInterviews" var="noInterviews"/>
<fmt:message bundle="${loc}" key="label.close" var="close"/>
<fmt:message bundle="${loc}" key="label.addOpinion" var="addOpinion"/>
<fmt:message bundle="${loc}" key="label.changeOpinion" var="changeOpinion"/>
<fmt:message bundle="${loc}" key="label.setOpinion" var="setOpinion"/>

<script src="https://code.jquery.com/jquery.min.js"></script>
<link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css" rel="stylesheet" type="text/css"/>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
<link href="../../css/table.css" rel="stylesheet" type="text/css"/>
<h1>${applicationInterviews}</h1>
<div class="table-responsive">
    <table class="table table-hover ">
        <thead>
        <tr>
            <th>${jobInterviewType}</th>
            <th>${dateTimeOfInterview}</th>
            <th>${place}</th>
            <th>${opinion}</th>
            <c:if test="${(sessionScope.user.userType=='HR')}">
                <th>${deleteJobInterview}</th>
                <th>${changeJobInterview}</th>
            </c:if>
        </tr>
        </thead>
        <tbody>
        <c:forEach var="i" begin="0" end="${jobInterviewList.size()-1}">
            <tr>
                <td>${jobInterviewList.get(i).jobInterviewType}</td>
                <td>
                    <fmt:parseDate value="${ jobInterviewList.get(i).dateAndTimeOfInterview }"
                                   pattern="yyyy-MM-dd'T'HH:mm" var="parsedDateTime" type="both"/>
                    <fmt:formatDate pattern="dd.MM.yyyy HH:mm" value="${ parsedDateTime }"/>
                </td>
                <td>${jobInterviewList.get(i).place}</td>
                <td><c:choose>
                    <c:when test="${not empty jobInterviewList.get(i).opinionAboutCandidate}">
                        ${jobInterviewList.get(i).opinionAboutCandidate}
                        <c:if test="${(sessionScope.user.userType=='HR')}">
                            <button class="btn btn-warning btn-block"
                                    data-interview-opinion="${jobInterviewList.get(i).opinionAboutCandidate}"
                                    data-interview-id="${jobInterviewList.get(i).id}"
                                    data-toggle="modal"
                                    data-target="#opinionModal">${changeOpinion}</button>
                        </c:if>
                    </c:when>
                    <c:otherwise>
                        <c:if test="${(sessionScope.user.userType=='HR')}">
                            <button class="btn btn-info btn-block"
                                    data-interview-id="${jobInterviewList.get(i).id}"
                                    data-toggle="modal"
                                    data-target="#opinionModal">${addOpinion}</button>
                        </c:if>
                    </c:otherwise>
                </c:choose>
                </td>
                <c:if test="${(sessionScope.user.userType=='HR')}">
                    <td>
                        <a class="btn btn-danger"
                           href="${pageContext.servletContext.contextPath}/controller?command=delete_job_interview&applicationId=   &jobInterviewId=${jobInterviewList.get(i).id}">
                                ${deleteJobInterview}
                        </a>
                    </td>
                    <td>
                        <button class="btn btn-warning btn-block"
                                data-interview-id="${jobInterviewList.get(i).id}"
                                data-interview-dateTime="${jobInterviewList.get(i).dateAndTimeOfInterview}"
                                data-interview-place="${jobInterviewList.get(i).place}"
                                data-interview-type="${jobInterviewList.get(i).jobInterviewType}"
                                data-toggle="modal"
                                data-target="#changeInterviewModal">${changeJobInterview}
                        </button>
                    </td>
                </c:if>
            </tr>
        </c:forEach>
        </tbody>
    </table>
</div>


<div class="modal fade" id="opinionModal" tabindex="-1" role="dialog"
     aria-labelledby="opinionModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <form name='form-login' method="POST" action="controller" class="form">
                <input type="hidden" name="command" value="change_opinion">
                <input type="hidden" name="jobInterviewId" value="">
                <div class="modal-header">
                    <h5 class="modal-title" id="opinionModalLabel">${setOpinion}</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="form-group">
                        <label for="newOpinion">${opinion}</label>
                        <input type="text" id="newOpinion" name="opinion" required>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">${close}
                    </button>
                    <input type="submit" class="btn btn-secondary" value="${setOpinion}">
                </div>
            </form>
        </div>
    </div>
</div>


<div class="modal fade" id="changeInterviewModal" tabindex="-1" role="dialog"
     aria-labelledby="changeInterviewModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <form method="POST" action="controller" class="form">
                <input type="hidden" name="command" value="change_job_interview">
                <input type="hidden" name="jobInterviewId" value="">
                <input type="hidden" name="applicationId" value=${jobInterviewList.get(0).application.id}>
                <div class="modal-header">
                    <h5 class="modal-title" id="changeInterviewModalLabel">${setOpinion}</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="form-group">
                        <label for="dateTime"><p>${dateTimeOfInterview}</p></label>
                        <input type="datetime-local"  name="dateTime" id="dateTime" required>
                    </div>
                    <div class="form-group">
                        <label for="type"><p>${jobInterviewType}</p></label>
                        <input type="text"  name="jobInterviewType" id="type">
                    </div>
                    <div class="form-group">
                        <label for="place"><p>${place}</p></label>
                        <input type="text"  name="place" id="place">
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">${close}
                    </button>
                    <input type="submit" class="btn btn-secondary" value="${setOpinion}">
                </div>
            </form>
        </div>
    </div>
</div>


<script>
    $('#opinionModal').on('show.bs.modal', function (e) {
        var jobInterviewId = $(e.relatedTarget).data('interview-id');
        $(e.currentTarget).find('input[name="jobInterviewId"]').val(jobInterviewId);
        var jobInterviewOpinion = $(e.relatedTarget).data('interview-opinion');
        $(e.currentTarget).find('input[name="opinion"]').val(jobInterviewOpinion);
    });

    $('#changeInterviewModal').on('show.bs.modal', function (e) {
        var jobInterviewId = $(e.relatedTarget).data('interview-id');
        $(e.currentTarget).find('input[name="jobInterviewId"]').val(jobInterviewId);
        var dateTime = $(e.relatedTarget).data('interview-dateTime');
        $(e.currentTarget).find('input[name="dateTime"]').val(dateTime);
        var place = $(e.relatedTarget).data('interview-place');
        $(e.currentTarget).find('input[name="place"]').val(place);
        var jobInterviewType = $(e.relatedTarget).data('interview-type');
        $(e.currentTarget).find('input[name="jobInterviewType"]').val(jobInterviewType);
    });
</script>