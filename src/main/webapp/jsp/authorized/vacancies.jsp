<%@ page contentType="text/html;charset=UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<c:set var="language"
       value="${not empty param.language ? param.language : not empty language ? language : pageContext.request.locale}"
       scope="session"/>
<c:set var="currentPage" value="${(requestScope.pageNumber==null) ? 1 : requestScope.pageNumber}" scope="page"/>
<c:set var="lastPage" value="${(requestScope.pageCount==null) ? 1 : requestScope.pageCount}" scope="page"/>
<c:set var="vacancyList" value="${pageContext.request.getAttribute(vacancyList)}" scope="page"/>
<fmt:setLocale value="${language}" scope="session"/>
<fmt:setBundle basename="property.label" var="loc"/>
<fmt:message bundle="${loc}" key="label.vacancy" var="vacancy"/>
<fmt:message bundle="${loc}" key="label.apply" var="apply"/>
<fmt:message bundle="${loc}" key="label.vacancyName" var="vacancyName"/>
<fmt:message bundle="${loc}" key="label.description" var="description"/>
<fmt:message bundle="${loc}" key="label.requirements" var="requirements"/>
<fmt:message bundle="${loc}" key="label.responsibilities" var="responsibilities"/>
<fmt:message bundle="${loc}" key="label.salary" var="salary"/>
<html>
<head>
    <link href="../../css/bootstrap.css" rel="stylesheet" type="text/css"/>
    <link href="../../css/bootstrap.min.css" rel="stylesheet" type="text/css">
    <script src="../../js/jquery-3.2.1.min.js"></script>
    <script src="../../js/bootstrap.min.js" type="text/javascript"></script>
    <link href="../../css/vacancies.css" rel="stylesheet">
    <title>${vacancy}</title>
</head>
<body>
<c:choose>
    <c:when test="${not empty sessionScope.user}">
        <div class="container">
            <div id="wrapper">
                <div class="overlay"></div>
                <%@ include file="leftMenu.jsp" %>
                <div id="page-content-wrapper">
                    <button type="button" class="hamburger is-closed" data-toggle="offcanvas">
                        <span class="hamb-top"></span>
                        <span class="hamb-middle"></span>
                        <span class="hamb-bottom"></span>
                    </button>
                    <div class="container">
                        <table class="table table-hover ">
                            <thead>
                            <tr>
                                <th>${vacancyName}</th>
                                <th>${salary}</th>
                                <th>${description}</th>
                                <th>${requirements}</th>
                                <th>${responsibilities}</th>
                            </tr>
                            </thead>
                            <tbody>
                            <c:if test="${vacancyList.size() > 0}">
                                <c:forEach var="i" begin="0" end="${vacancyList.size()-1}">
                                    <tr>
                                        <td>${vacancyList.get(i).name}
                                            <c:if test="${sessionScope.user.userType=='CANDIDATE' }">
                                                <a class="btn btn-info"
                                                   href="controller?command=apply&vacancyId=${vacancyList.get(i).id}">
                                                        ${apply}
                                                </a>
                                            </c:if>
                                        </td>
                                        <td>${vacancyList.get(i).salary}</td>
                                        <td>${vacancyList.get(i).description}</td>
                                        <td>${vacancyList.get(i).requirements}</td>
                                        <td>${vacancyList.get(i).responsibilities}</td>
                                    </tr>
                                </c:forEach>
                            </c:if>
                            </tbody>
                        </table>
                        <nav aria-label="Page navigation example">
                            <ul class="pagination justify-content-center">
                                <c:choose>
                                    <c:when test="${currentPage==1}">
                                        <li class="page-item disabled">
                                            <a class="page-link" href="#" aria-label="Previous">
                                                <span aria-hidden="true">&laquo;</span>
                                                <span class="sr-only">Previous</span>
                                            </a>
                                        </li>
                                    </c:when>
                                    <c:otherwise>
                                        <li class="page-item">
                                            <a class="page-link"
                                               href="controller?command=all_open_vacancies&pageNumber=${currentPage-1}"
                                               aria-label="Previous">
                                                <span aria-hidden="true">&laquo;</span>
                                                <span class="sr-only">Previous</span>
                                            </a>
                                        </li>
                                    </c:otherwise>
                                </c:choose>
                                    <%--For displaying all available pages--%>
                                <c:forEach begin="${(currentPage-3<0) ? 1 : currentPage-1}"
                                           end="${(currentPage+1>pageCount) ? pageCount : currentPage+1}" var="i">
                                    <c:choose>
                                        <c:when test="${currentPage eq i}">
                                            <li class="page-item active">
                                                <a class="page-link"
                                                   href="controller?command=all_open_vacancies&pageNumber=${i}">${i}</a>
                                            </li>
                                        </c:when>
                                        <c:otherwise>
                                            <li class="page-item">
                                                <a class="page-link"
                                                   href="controller?command=all_open_vacancies&pageNumber=${i}">${i}</a>
                                            </li>
                                        </c:otherwise>
                                    </c:choose>
                                </c:forEach>
                                <c:choose>
                                    <c:when test="${currentPage eq lastPage}">
                                        <li class="page-item disabled">
                                            <a class="page-link" href="#" aria-label="Next">
                                                <span aria-hidden="true">&raquo;</span>
                                                <span class="sr-only">Next</span>
                                            </a>
                                        </li>
                                    </c:when>
                                    <c:otherwise>
                                        <li class="page-item">
                                            <a class="page-link"
                                               href="controller?command=all_open_vacancies&pageNumber=${currentPage+1}"
                                               aria-label="Next">
                                                <span aria-hidden="true">&raquo;</span>
                                                <span class="sr-only">Next</span>
                                            </a>
                                        </li>
                                    </c:otherwise>
                                </c:choose>
                            </ul>
                        </nav>
                    </div>
                </div>
            </div>
        </div>
    </c:when>
    <c:otherwise>
        <jsp:forward page="../error/error403.jsp"/>
    </c:otherwise>
</c:choose>
</body>

</html>
