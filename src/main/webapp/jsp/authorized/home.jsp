<%@ page contentType="text/html;charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="ctg" uri="customtags" %>
<c:set var="language"
       value="${not empty param.language ? param.language : not empty language ? language : pageContext.request.locale}"
       scope="session"/>
<c:set var="currentPage" value="${(requestScope.pageNumber==null) ? 1 : requestScope.pageNumber}" scope="page"/>
<c:set var="lastPage" value="${(requestScope.pageCount==null) ? 1 : requestScope.pageCount}" scope="page"/>
<c:set var="vacancyList" value="${requestScope.vacancyList}" scope="page"/>
<c:set var="applicationList" value="${requestScope.applicationList}" scope="page"/>
<fmt:setLocale value="${language}" scope="session"/>
<fmt:setBundle basename="property.label" var="loc"/>
<fmt:message bundle="${loc}" key="label.title" var="title"/>
<fmt:message bundle="${loc}" key="label.logout" var="logout"/>
<fmt:message bundle="${loc}" key="label.home" var="home"/>
<fmt:message bundle="${loc}" key="label.vacancy" var="vacancy"/>
<fmt:message bundle="${loc}" key="label.email" var="email"/>
<fmt:message bundle="${loc}" key="label.photo" var="photo"/>
<fmt:message bundle="${loc}" key="label.birthday" var="birthday"/>
<fmt:message bundle="${loc}" key="label.phone" var="phone"/>
<fmt:message bundle="${loc}" key="button.uploadPhoto" var="uploadPhoto"/>
<fmt:message bundle="${loc}" key="label.password" var="password"/>
<fmt:message bundle="${loc}" key="label.name" var="name"/>
<fmt:message bundle="${loc}" key="label.close" var="close"/>
<fmt:message bundle="${loc}" key="label.choseFile" var="choseFile"/>
<fmt:message bundle="${loc}" key="label.changeProfile" var="changeProfile"/>
<fmt:message bundle="${loc}" key="button.deleteProfile" var="deleteProfile"/>
<fmt:message bundle="${loc}" key="button.changePassword" var="changePassword"/>
<fmt:message bundle="${loc}" key="label.blocked" var="blocked"/>

<html>
<head>
    <title>${home}</title>
    <script src="${pageContext.servletContext.contextPath}/js/jquery-3.2.1.min.js"></script>
    <link href="${pageContext.servletContext.contextPath}/css/bootstrap.css" rel="stylesheet" type="text/css"/>
    <link href="${pageContext.servletContext.contextPath}/css/bootstrap.min.css" rel="stylesheet" type="text/css">
    <script src="${pageContext.servletContext.contextPath}/js/bootstrap.min.js" type="text/javascript"></script>
</head>
<body>
<c:choose>
    <c:when test="${not empty sessionScope.user}">
                <div class="container">
                    <div id="wrapper">
                        <div class="overlay"></div>
                        <%@ include file="leftMenu.jsp" %>
                        <div id="page-content-wrapper">
                            <button type="button" class="hamburger is-closed" data-toggle="offcanvas">
                                <span class="hamb-top"></span>
                                <span class="hamb-middle"></span>
                                <span class="hamb-bottom"></span>
                            </button>
                            <div class="container">
                                <div class="row">
                                    <div class="col-md-offset-1 col-md-10 col-lg-offset-2 col-lg-8">
                                        <div class="well profile">
                                            <div class="col-sm-12">
                                                <div class="col-xs-12 col-sm-7">
                                                    <h2>${sessionScope.user.name}</h2>
                                                    <p><strong>${email}</strong> ${sessionScope.user.email} </p>
                                                    <p><strong>${phone} </strong> ${sessionScope.user.phone} </p>
                                                    <p><strong>${birthday}</strong>${sessionScope.user.birthday}</p>
                                                </div>
                                                <div class="col-xs-12 col-sm-5 text-center">
                                                    <figure>
                                                        <ctg:image userId="${sessionScope.user.id}"/>
                                                        <button class="btn btn-info btn-block" data-toggle="modal"
                                                                data-target="#photoModal">
                                                                ${uploadPhoto}
                                                        </button>
                                                    </figure>
                                                </div>
                                                <c:if test="${sessionScope.user.blocked}">
                                                    <h1>${blocked}</h1>
                                                </c:if>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <c:choose>
                                    <c:when test="${not empty requestScope.message}">
                                        <h2 class="error">${requestScope.message}</h2>
                                    </c:when>
                                </c:choose>

                                <c:if test="${not sessionScope.user.blocked}">
                                    <c:choose>
                                        <c:when test="${sessionScope.user.userType=='CANDIDATE'}">
                                            <jsp:include page="table_application.jsp"/>
                                        </c:when>
                                        <c:when test="${sessionScope.user.userType=='HR'}">
                                            <jsp:include page="hr_manager/hr_vacancies_table.jsp"/>
                                        </c:when>
                                    </c:choose>
                                </c:if>
                            </div>
                        </div>
                    </div>
                </div>


                <div class="modal fade" id="photoModal" tabindex="-1" role="dialog"
                     aria-labelledby="exampleModalLabel" aria-hidden="true">
                    <div class="modal-dialog" role="document">
                        <div class="modal-content">
                            <form method="POST" action="upload" enctype="multipart/form-data">
                                <div class="modal-header">
                                    <h5 class="modal-title" id="photoModalLabel">${uploadPhoto}</h5>
                                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                        <span aria-hidden="true">&times;</span>
                                    </button>
                                </div>
                                <div class="modal-body">
                                    <label for="photo">${choseFile}</label>
                                    <input type="file" accept="image/jpeg,image/png,image/gif" id="photo" name="file"
                                           required>
                                </div>
                                <div class="modal-footer">
                                    <button type="button" class="btn btn-secondary"
                                            data-dismiss="modal">${close} </button>
                                    <input type="submit" class="btn btn-secondary" value="${uploadPhoto}">
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
    </c:when>
    <c:otherwise>
        <jsp:forward page="../error/error403.jsp"/>
    </c:otherwise>
</c:choose>

</body>
</html>
