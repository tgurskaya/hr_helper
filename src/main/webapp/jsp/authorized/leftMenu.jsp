<%@ page contentType="text/html;charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="ctg" uri="customtags" %>
<c:set var="language"
       value="${not empty param.language ? param.language : not empty language ? language : pageContext.request.locale}"
       scope="session"/>
<c:set var="lastCommand" value="${not empty sessionScope.lastCommand ?sessionScope.lastCommand: \"home\"}"/>
<fmt:setLocale value="${language}" scope="session"/>
<fmt:setBundle basename="property.label" var="loc"/>
<fmt:message bundle="${loc}" key="label.title" var="title"/>
<fmt:message bundle="${loc}" key="label.logout" var="logout"/>
<fmt:message bundle="${loc}" key="label.home" var="home"/>
<fmt:message bundle="${loc}" key="label.vacancy" var="vacancy"/>
<fmt:message bundle="${loc}" key="label.email" var="email"/>
<fmt:message bundle="${loc}" key="label.photo" var="photo"/>
<fmt:message bundle="${loc}" key="label.birthday" var="birthday"/>
<fmt:message bundle="${loc}" key="label.phone" var="phone"/>
<fmt:message bundle="${loc}" key="button.uploadPhoto" var="uploadPhoto"/>
<fmt:message bundle="${loc}" key="label.password" var="password"/>
<fmt:message bundle="${loc}" key="label.name" var="name"/>
<fmt:message bundle="${loc}" key="label.phone" var="phone"/>
<fmt:message bundle="${loc}" key="label.close" var="close"/>
<fmt:message bundle="${loc}" key="label.choseFile" var="choseFile"/>
<fmt:message bundle="${loc}" key="label.changeProfile" var="changeProfile"/>
<fmt:message bundle="${loc}" key="button.deleteProfile" var="deleteProfile"/>
<fmt:message bundle="${loc}" key="button.changePassword" var="changePassword"/>
<fmt:message bundle="${loc}" key="button.registerHrManager" var="registerHrManager"/>
<fmt:message bundle="${loc}" key="button.viewUsers" var="viewUsers"/>
<fmt:message bundle="${loc}" key="language.russian" var="russian"/>
<fmt:message bundle="${loc}" key="language.english" var="english"/>
<fmt:message bundle="${loc}" key="label.language" var="language"/>
<fmt:message bundle="${loc}" key="language.belorussian" var="belorussian"/>
<fmt:message bundle="${loc}" key="label.no" var="no"/>
<fmt:message bundle="${loc}" key="label.yes" var="yes"/>
<fmt:message bundle="${loc}" key="label.areYouSure" var="areYouShure"/>
<fmt:message bundle="${loc}" key="label.changeInformation" var="changeInformation"/>
<fmt:message bundle="${loc}" key="label.changeLanguage" var="changeLanguage"/>
<fmt:message bundle="${loc}" key="label.newPassword" var="newPassword"/>
<fmt:message bundle="${loc}" key="label.oldPassword" var="oldPassword"/>
<fmt:message bundle="${loc}" key="label.repeatNewPassword" var="repeatNewPassword"/>
<fmt:message bundle="${loc}" key="button.addVacancy" var="addVacancy"/>

<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
<script src="${pageContext.servletContext.contextPath}/js/jquery-3.2.1.min.js"></script>
<script src="${pageContext.servletContext.contextPath}/js/bootstrap.min.js"></script>
<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
<link href="${pageContext.servletContext.contextPath}/css/leftMenu.css" rel="stylesheet" type="text/css"/>
<script src="${pageContext.servletContext.contextPath}/js/leftMenu.js"></script>
<script src="${pageContext.servletContext.contextPath}/js/repeatPassword.js"></script>
<nav class="navbar navbar-inverse navbar-fixed-top" id="sidebar-wrapper" role="navigation">
    <ul class="nav sidebar-nav">
        <li class="sidebar-brand">
            <a href="${pageContext.servletContext.contextPath}/index.jsp">
                <header>${title}</header>
            </a>
        </li>

        <li>
            <a href="${pageContext.servletContext.contextPath}/controller?command=home">${home}</a>
        </li>
        <c:if test="${not sessionScope.user.blocked}">
            <li>
                <a href="${pageContext.servletContext.contextPath}/controller?command=all_open_vacancies&pageNumber=1">${vacancy}</a>
            </li>
        </c:if>
        <c:choose>

            <c:when test="${sessionScope.user.userType=='ADMIN' and not sessionScope.user.blocked}">
                <li>
                    <a href="${pageContext.servletContext.contextPath}/controller?command=all_users&pageNumber=1">${viewUsers}</a>
                </li>
                <li>
                    <a href="${pageContext.servletContext.contextPath}/jsp/registration.jsp">${registerHrManager}</a>
                </li>
            </c:when>
            <c:when test="${sessionScope.user.userType=='HR' and not sessionScope.user.blocked}">
                <li>
                    <a href="${pageContext.servletContext.contextPath}/controller?command=add_vacancy">${addVacancy}</a>
                </li>
            </c:when>

        </c:choose>
        <li class="dropdown">
            <a href="#" class="dropdown-toggle" data-toggle="dropdown">${changeProfile} <span
                    class="caret"></span></a>
            <ul class="dropdown-menu" role="menu">
                <li class="dropdown-header"></li>
                <li><a href="#changeProfileModal" data-toggle="modal">${changeInformation}</a></li>
                <li><a href="#changePasswordModal" data-toggle="modal">${changePassword}</a></li>
                <li><a href="#deleteProfileModal" data-toggle="modal">${deleteProfile}</a></li>
            </ul>
        </li>
        <li class="dropdown">
            <a href="#" class="dropdown-toggle" data-toggle="dropdown">${changeLanguage} <span
                    class="caret"></span></a>
            <ul class="dropdown-menu" role="menu">
                <li class="dropdown-header"></li>
                <a href="${pageContext.servletContext.contextPath}/controller?command=${lastCommand}&language=en_US">
                    <img class="lang"
                         src="../../img/gbr_flag.png">
                    ${english}</a>
                <a href="${pageContext.servletContext.contextPath}/controller?command=${lastCommand}&language=ru_RU"><img
                        src="../../img/rus_flag.png"
                        class="lang">
                    ${russian}</a>
                <a href="${pageContext.servletContext.contextPath}/controller?command=${lastCommand}&language=be_BY"><img
                        src="../../img/bel_flag.png"
                        class="lang">
                    ${belorussian}</a>
            </ul>
        </li>
        <li>
            <a href="${pageContext.servletContext.contextPath}/controller?command=logout">${logout}</a>
        </li>
    </ul>
</nav>


<div class="modal fade" id="changePasswordModal" tabindex="-1" role="dialog"
     aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <form method="POST" action="controller">
                <input type="hidden" name="command" value="change_password"/>
                <div class="modal-header">
                    <h5 class="modal-title" id="changePasswordModalLabel">${changePassword}</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="form-group">
                        <label for="oldPassword">${oldPassword}</label>
                        <input type="password"
                               pattern="(?=^.{8,45}$)((?=.*\d)|(?=.*\W+))(?![.\n])(?=.*[A-Z])(?=.*[a-z]).*$"
                               id="oldPassword" name="oldPassword" required/>
                    </div>
                    <div class="form-group">
                        <label for="password">${newPassword}</label>
                        <input type="password"
                               pattern="(?=^.{8,45}$)((?=.*\d)|(?=.*\W+))(?![.\n])(?=.*[A-Z])(?=.*[a-z]).*$"
                               id="password" name="password" required>
                    </div>
                    <div class="form-group">
                        <label for="repeatPassword">${repeatNewPassword}</label>
                        <input type="password"
                               pattern="(?=^.{8,45}$)((?=.*\d)|(?=.*\W+))(?![.\n])(?=.*[A-Z])(?=.*[a-z]).*$"
                               id="repeatPassword" name="repeatPassword" required>
                    </div>

                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">${close} </button>
                    <input type="submit" class="btn btn-secondary" value="${changePassword}">
                </div>
            </form>
        </div>
    </div>
</div>


<div class="modal fade" id="deleteProfileModal" tabindex="-1" role="dialog"
     aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <form method="POST" action="controller">
                <input type="hidden" name="command" value="delete_profile"/>
                <div class="modal-header">
                    <h5 class="modal-title" id="deleteProfileLabel">${deleteProfile}</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <p>
                        ${areYouShure}
                    </p>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">${close} </button>
                    <input type="submit" class="btn btn-secondary" value="${deleteProfile}">
                </div>
            </form>
        </div>
    </div>
</div>


<div class="modal fade" id="changeProfileModal" tabindex="-1" role="dialog"
     aria-labelledby="changeProfileModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <form name='form-login' method="POST" action="controller" class="form">
                <input type="hidden" name="command" value="update_user"/>
                <div class="modal-header">
                    <h5 class="modal-title" id="changeProfileModalLabel">${changeProfile}</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="form-group">
                        <label for="newName">${name}</label>
                        <input type="text" id="newName" name="name" required
                               pattern="^[А-ЯA-Z][a-zA-ZА-Яа-я\-іў'\s]{4,50}$" value="${user.name}">
                    </div>
                    <div class="form-group">
                        <label for="newBirthday">${birthday}</label>
                        <input type="date" id="newBirthday" name="birthday" min="1930-01-01" max="2000-01-01"
                               value="${user.birthday}">
                    </div>
                    <div class="form-group">
                        <label for="newPhone">${phone}</label>
                        <input type="text" id="newPhone" name="phone" pattern="[0-9\+\-]{4,20}$"
                               value="${user.phone}">
                    </div>
                    <div class="form-group">
                        <label for="newEmail">${email}</label>
                        <input type="email" id="newEmail" name="email" required
                               pattern="[a-z0-9._%+-]+@[a-z0-9.-]+\.[a-z]{2,4}$" value="${user.email}">
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">${close}
                    </button>
                    <input type="submit" class="btn btn-secondary" value="${changeProfile}">
                </div>
            </form>
        </div>
    </div>
</div>

