<%@ page contentType="text/html;charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="ctg" uri="customtags" %>

<c:set var="applicationList" value="${requestScope.applicationList}" scope="page"/>
<c:set var="currentPage" value="${(requestScope.pageNumber==null) ? 1 : requestScope.pageNumber}" scope="page"/>
<c:set var="lastPage" value="${(requestScope.pageCount==null) ? 1 : requestScope.pageCount}" scope="page"/>
<fmt:setBundle basename="property.label" var="loc"/>
<fmt:message bundle="${loc}" key="label.vacancyName" var="vacancyName"/>
<fmt:message bundle="${loc}" key="label.description" var="description"/>
<fmt:message bundle="${loc}" key="label.coveringLetter" var="coveringLetter"/>
<fmt:message bundle="${loc}" key="label.resume" var="resume"/>
<fmt:message bundle="${loc}" key="button.viewJobInterviews" var="viewJobInterviews"/>
<fmt:message bundle="${loc}" key="button.deleteApplication" var="deleteApplication"/>
<fmt:message bundle="${loc}" key="button.viewResume" var="viewResume"/>
<fmt:message bundle="${loc}" key="label.candidateApplications" var="candidateApplications"/>
<fmt:message bundle="${loc}" key="button.changeResume" var="changeResume"/>
<fmt:message bundle="${loc}" key="button.changeCoveringLetter" var="changeCoveringLetter"/>
<fmt:message bundle="${loc}" key="label.close" var="close"/>
<fmt:message bundle="${loc}" key="label.choseFile" var="choseFile"/>
<fmt:message bundle="${loc}" key="label.decision" var="decision"/>
<script src="https://code.jquery.com/jquery.min.js"></script>
<link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>
<c:if test="${applicationList.size() > 0}">
    <h1>${candidateApplications}</h1>
    <div class="table-responsive">
    <table id ="grid" class="table table-hover ">
        <thead>
        <tr>
            <th >${vacancyName}</th>
            <th>${description}</th>
            <th>${coveringLetter}</th>
            <th>${resume}</th>
            <th>${decision}</th>
        </tr>
        </thead>
        <tbody>
        <c:forEach var="i" begin="0" end="${applicationList.size()-1}">
            <tr>
                <td>${applicationList.get(i).vacancy.name}
                    <form method="POST" action="controller">
                        <input type="hidden" name="command"
                               value="view_job_interviews"/>
                        <input type="hidden" name="applicationId" value="${applicationList.get(i).id}"/>
                        <button class="btn btn-block btn-success">${viewJobInterviews}</button>
                    </form>
                    <a class="btn btn-block btn-danger"
                       href="${pageContext.servletContext.contextPath}/controller?command=delete_application&applicationId=${applicationList.get(i).id}">${deleteApplication}</a>

                </td>
                <td>${applicationList.get(i).vacancy.description}</td>
                <td>${applicationList.get(i).coveringLetter}
                    <button class="btn btn-warning btn-block" data-application-id="${applicationList.get(i).id}"
                            data-toggle="modal" data-application-id="${applicationList.get(i).id}"
                            data-covering-letter="${applicationList.get(i).coveringLetter}"
                            data-target="#changeCoveringLetterModal">
                            ${changeCoveringLetter}
                    </button>
                    <div class="modal fade" id="changeCoveringLetterModal" tabindex="-1" role="dialog"
                         aria-labelledby="exampleModalLabel" aria-hidden="true">
                        <div class="modal-dialog" role="document">
                            <div class="modal-content">
                                <form method="POST" action="controller">
                                    <div class="modal-header">
                                        <h5 class="modal-title" id="changeApplicationLabel">${changeCoveringLetter}</h5>
                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                            <span aria-hidden="true">&times;</span>
                                        </button>
                                    </div>
                                    <div class="modal-body">
                                        <input type="hidden" name="command" value="change_covering_letter">
                                        <input type="hidden" id="application" name="applicationId"
                                               value="">
                                        <label for="covering-letter"><p>${coveringLetter}</p></label>
                                        <textarea class="form-control" name="coveringLetter" rows="5"
                                                  id="covering-letter">${applicationList.get(i).coveringLetter}</textarea>
                                    </div>
                                    <div class="modal-footer">
                                        <button type="button" class="btn btn-secondary"
                                                data-dismiss="modal"
                                                data-covering-letter="${applicationList.get(i).coveringLetter}">${close} </button>
                                        <input type="submit" class="btn btn-secondary" value="${changeCoveringLetter}">
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>

                </td>
                <td>
                    <a class="btn btn-success btn-block"
                            href="${pageContext.servletContext.contextPath}/controller?command=view_resume&applicationId=${applicationList.get(i).id}">
                            ${viewResume}</a>
                    <button class="btn btn-warning btn-block" data-application-id="${applicationList.get(i).id}"
                            data-toggle="modal" data-target="#changeResumeModal">
                            ${changeResume}
                    </button>

                    <div class="modal fade" id="changeResumeModal" tabindex="-1" role="dialog"
                         aria-labelledby="exampleModalLabel" aria-hidden="true">
                        <div class="modal-dialog" role="document">
                            <div class="modal-content">
                                <form method="POST" action="newApplication" enctype="multipart/form-data">
                                    <div class="modal-header">
                                        <h5 class="modal-title" id="changeResumeModalLabel">${changeResume}</h5>
                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                            <span aria-hidden="true">&times;</span>
                                        </button>
                                    </div>
                                    <div class="modal-body">
                                        <label for="photo">${choseFile}</label>
                                        <input type="hidden" id="applicationId" name="applicationId"
                                               value="">
                                        <input type="file" accept="application/pdf" id="photo" name="file" required>
                                    </div>
                                    <div class="modal-footer">
                                        <button type="button" class="btn btn-secondary"
                                                data-dismiss="modal">${close} </button>
                                        <input type="submit" class="btn btn-secondary" value="${changeResume}">
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </td>
                <td>
                        ${applicationList.get(i).decision}
                </td>
            </tr>
        </c:forEach>

        </tbody>
    </table>
    </div>
    <nav aria-label="Page navigation example">
        <ul class="pagination justify-content-center">
            <c:choose>
                <c:when test="${currentPage==1}">
                    <li class="page-item disabled">
                        <a class="page-link" href="#" aria-label="Previous">
                            <span aria-hidden="true">&laquo;</span>
                            <span class="sr-only">Previous</span>
                        </a>
                    </li>
                </c:when>
                <c:otherwise>
                    <li class="page-item">
                        <a class="page-link"
                           href="${pageContext.servletContext.contextPath}/controller?command=all_candidate_applications&candidateId=${user.id}&pageNumber=${currentPage-1}"
                           aria-label="Previous">
                            <span aria-hidden="true">&laquo;</span>
                            <span class="sr-only">Previous</span>
                        </a>
                    </li>
                </c:otherwise>
            </c:choose>
            <c:forEach begin="${(currentPage-3<0) ? 1 : currentPage-1}"
                       end="${(currentPage+1>lastPage) ? lastPage : currentPage+1}"
                       var="i">
                <c:choose>
                    <c:when test="${currentPage eq i}">
                        <li class="page-item active">
                            <a class="page-link"
                               href="${pageContext.servletContext.contextPath}/controller?command=all_candidate_applications&candidateId=${user.id}&pageNumber=${i}">${i}</a>
                        </li>
                    </c:when>
                    <c:otherwise>
                        <li class="page-item">
                            <a class="page-link"
                               href="${pageContext.servletContext.contextPath}/controller?command=all_candidate_applications&candidateId=${user.id}&pageNumber=${i}">${i}</a>
                        </li>
                    </c:otherwise>
                </c:choose>
            </c:forEach>
            <c:choose>
                <c:when test="${currentPage eq lastPage}">
                    <li class="page-item disabled">
                        <a class="page-link" href="#" aria-label="Next">
                            <span aria-hidden="true">&raquo;</span>
                            <span class="sr-only">Next</span>
                        </a>
                    </li>
                </c:when>
                <c:otherwise>
                    <li class="page-item">
                        <a class="page-link"
                           href="${pageContext.servletContext.contextPath}/controller?command=all_candidate_applications&candidateId=${sessionScope.user.id}&pageNumber=${currentPage+1}"
                           aria-label="Next">
                            <span aria-hidden="true">&raquo;</span>
                            <span class="sr-only">Next</span>
                        </a>
                    </li>
                </c:otherwise>
            </c:choose>
        </ul>
    </nav>
</c:if>


<script>
    $('#changeResumeModal').on('show.bs.modal', function (e) {
        var applicationId = $(e.relatedTarget).data('application-id');
        $(e.currentTarget).find('input[name="applicationId"]').val(applicationId);
    });

    $('#changeCoveringLetterModal').on('show.bs.modal', function (e) {
        var applicationId = $(e.relatedTarget).data('application-id');
        $(e.currentTarget).find('input[name="applicationId"]').val(applicationId);
        var coveringLetter = $(e.relatedTarget).data('covering-letter');
        $(e.currentTarget).find('textarea[name="coveringLetter"]').val(coveringLetter);
    });


</script>