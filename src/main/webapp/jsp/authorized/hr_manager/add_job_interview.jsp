<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<c:set var="language"
       value="${not empty param.language ? param.language : not empty language ? language : pageContext.request.locale}"
       scope="session"/>
<fmt:setLocale value="${language}" scope="session"/>
<fmt:setBundle basename="property.label" var="loc"/>
<fmt:message bundle="${loc}" key="label.requirements" var="requirements"/>
<fmt:message bundle="${loc}" key="label.responsibilities" var="responsibilities"/>
<fmt:message bundle="${loc}" key="label.salary" var="salary"/>
<fmt:message bundle="${loc}" key="label.vacancyName" var="vacancyName"/>
<fmt:message bundle="${loc}" key="label.description" var="description"/>
<fmt:message bundle="${loc}" key="label.newVacancy" var="newVacancy"/>
<fmt:message bundle="${loc}" key="button.createVacancy" var="createVacancy"/>
<fmt:message bundle="${loc}" key="label.newJobInterview" var="newJobInterview"/>
<fmt:message bundle="${loc}" key="label.opinion" var="opinion"/>
<fmt:message bundle="${loc}" key="label.dateTimeOfInterview" var="dateTimeOfInterview"/>
<fmt:message bundle="${loc}" key="label.dateTimeOfInterview" var="dateTimeOfInterview"/>
<fmt:message bundle="${loc}" key="label.jobInterviewType" var="jobInterviewType"/>
<fmt:message bundle="${loc}" key="label.place" var="place"/>
<fmt:message bundle="${loc}" key="button.createJobInterview" var="createJobInterview"/>


<html>
<head>
    <title>${newJobInterview}</title>
    <link rel="stylesheet" href="${pageContext.servletContext.contextPath}/css/bootstrap.min.css"/>
    <link rel="stylesheet" href="${pageContext.servletContext.contextPath}/css/registration.css"/>
</head>
<body>
<c:choose>
    <c:when test="${(not empty sessionScope.user)and (sessionScope.user.userType=='HR') }">
        <div class="container">
            <div id="wrapper">
                <div class="overlay"></div>
                <%@ include file="../leftMenu.jsp" %>
                <div id="page-content-wrapper">
                    <button type="button" class="hamburger is-closed" data-toggle="offcanvas">
                        <span class="hamb-top"></span>
                        <span class="hamb-middle"></span>
                        <span class="hamb-bottom"></span>
                    </button>
                    <div class="container">
                        <h1 class="title">${newJobInterview}</h1>
                        <div id="login">
                            <form method="POST" action="controller">
                                <input type="hidden" name="command" value="new_job_interview">
                                <input type="hidden" name="applicationId" value="${param.applicationId}">
                                <div class="form-group">
                                    <label for="dateTime"><p>${dateTimeOfInterview}</p></label>
                                    <input type="datetime-local" name="dateTime" id="dateTime" required>
                                </div>
                                <div class="form-group">
                                    <label for="type"><p>${jobInterviewType}</p></label>
                                    <input type="text" name="jobInterviewType" id="type">
                                </div>
                                <div class="form-group">
                                    <label for="place"><p>${place}</p></label>
                                    <input type="text" name="place" id="place">
                                </div>

                                <input type="submit" class="btn btn-secondary" value="${createJobInterview}">
                            </form>
                        </div>
                    </div>

                </div>
            </div>
        </div>
    </c:when>
    <c:otherwise>
        <jsp:forward page="${pageContext.servletContext.contextPath}/error/error403.jsp"/>
    </c:otherwise>
</c:choose>
</body>
</html>
