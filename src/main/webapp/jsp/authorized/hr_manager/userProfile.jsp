<%@ page contentType="text/html;charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="ctg" uri="customtags" %>
<c:set var="language"
       value="${not empty param.language ? param.language : not empty language ? language : pageContext.request.locale}"
       scope="session"/>
<c:set var="currentPage" value="${(requestScope.pageNumber==null) ? 1 : requestScope.pageNumber}" scope="page"/>
<c:set var="lastPage" value="${(requestScope.pageCount==null) ? 1 : requestScope.pageCount}" scope="page"/>
<c:set var="vacancyList" value="${requestScope.vacancyList}" scope="page"/>
<c:set var="applicationList" value="${requestScope.applicationList}" scope="page"/>
<fmt:setLocale value="${language}" scope="session"/>
<fmt:setBundle basename="property.label" var="loc"/>
<fmt:message bundle="${loc}" key="label.email" var="email"/>
<fmt:message bundle="${loc}" key="label.photo" var="photo"/>
<fmt:message bundle="${loc}" key="label.birthday" var="birthday"/>
<fmt:message bundle="${loc}" key="label.phone" var="phone"/>
<fmt:message bundle="${loc}" key="label.name" var="name"/>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>

<html>
<head>
    <title>${userProfile}</title>
    <script src="${pageContext.servletContext.contextPath}/js/jquery-3.2.1.min.js"></script>
    <link href="${pageContext.servletContext.contextPath}/css/bootstrap.css" rel="stylesheet" type="text/css"/>
    <link href="${pageContext.servletContext.contextPath}/css/bootstrap.min.css" rel="stylesheet" type="text/css">
    <script src="${pageContext.servletContext.contextPath}/js/bootstrap.min.js" type="text/javascript"></script>
</head>
<body>
<c:choose>
    <c:when test="${(not empty sessionScope.user)and (sessionScope.user.userType=='HR') }">
        <div class="container">
            <div id="wrapper">
                <div class="overlay"></div>
                <%@ include file="../leftMenu.jsp" %>
                <div id="page-content-wrapper">
                    <button type="button" class="hamburger is-closed" data-toggle="offcanvas">
                        <span class="hamb-top"></span>
                        <span class="hamb-middle"></span>
                        <span class="hamb-bottom"></span>
                    </button>
                    <div class="container">
                        <div class="row">
                            <div class="col-md-offset-1 col-md-10 col-lg-offset-2 col-lg-8">
                                <div class="well profile">
                                    <div class="col-sm-12">
                                        <div class="col-xs-12 col-sm-7">
                                            <h2>${requestScope.candidate.name}</h2>
                                            <p><strong>${email}</strong> ${requestScope.candidate.email} </p>
                                            <p><strong>${phone} </strong> ${requestScope.candidate.phone} </p>
                                            <p><strong>${birthday}</strong>${requestScope.candidate.birthday}</p>
                                        </div>
                                        <div class="col-xs-12 col-sm-5 text-center">
                                            <figure>
                                                <ctg:image userId="${requestScope.candidate.id}"/>
                                            </figure>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

    </c:when>
    <c:otherwise>
        <jsp:forward page="${pageContext.servletContext.contextPath}/error/error403.jsp"/>
    </c:otherwise>
</c:choose>

</body>
</html>
