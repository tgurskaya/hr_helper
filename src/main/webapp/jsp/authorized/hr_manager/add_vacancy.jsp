<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<c:set var="language"
       value="${not empty param.language ? param.language : not empty language ? language : pageContext.request.locale}"
       scope="session"/>
<fmt:setLocale value="${language}" scope="session"/>
<fmt:setBundle basename="property.label" var="loc"/>
<fmt:message bundle="${loc}" key="label.requirements" var="requirements"/>
<fmt:message bundle="${loc}" key="label.responsibilities" var="responsibilities"/>
<fmt:message bundle="${loc}" key="label.salary" var="salary"/>
<fmt:message bundle="${loc}" key="label.vacancyName" var="vacancyName"/>
<fmt:message bundle="${loc}" key="label.description" var="description"/>
<fmt:message bundle="${loc}" key="label.newVacancy" var="newVacancy"/>
<fmt:message bundle="${loc}" key="button.createVacancy" var="createVacancy"/>

<html>
<head>
    <title>${newVacancy}</title>
    <link rel="stylesheet" href="${pageContext.servletContext.contextPath}/css/bootstrap.min.css"/>
    <link rel="stylesheet" href="${pageContext.servletContext.contextPath}/css/registration.css"/>
</head>
<body>
<c:choose>
    <c:when test="${(not empty sessionScope.user)and (sessionScope.user.userType=='HR') }">
        <div class="container">
            <div id="wrapper">
                <div class="overlay"></div>
                <%@ include file="../leftMenu.jsp" %>
                <div id="page-content-wrapper">
                    <button type="button" class="hamburger is-closed" data-toggle="offcanvas">
                        <span class="hamb-top"></span>
                        <span class="hamb-middle"></span>
                        <span class="hamb-bottom"></span>
                    </button>
                    <div class="container">
                        <h1 class="title">${newVacancy}</h1>
                        <div id="login">
                            <form method="POST" action="controller">
                                <input type="hidden" name="command" value="new_vacancy">

                                <label for="name"><p>${vacancyName}</p></label>
                                <input type="text" maxlength="45" name="name" id="name" required>

                                <label for="salary"><p>${salary}</p></label>
                                <input type="text" maxSize="15" name="salary" id="salary">

                                <label for="description"><p>${description}</p></label>
                                <textarea class="form-control" maxlength="2000" name="description" rows="5" id="description"
                                          required></textarea>

                                <label for="responsibilities"><p>${responsibilities}</p></label>
                                <textarea class="form-control" maxlength="2000" name="responsibilities" rows="5"
                                          id="responsibilities"></textarea>
                                <label for="requirements"><p>${requirements}</p></label>
                                <textarea class="form-control" name="requirements" rows="5"
                                          id="requirements" maxlength="2000"></textarea>
                                <input type="submit" class="btn btn-secondary" value="${createVacancy}">
                            </form>
                        </div>
                    </div>

                </div>
            </div>
        </div>
    </c:when>
    <c:otherwise>
        <jsp:forward page="${pageContext.servletContext.contextPath}/error/error403.jsp"/>
    </c:otherwise>
</c:choose>
</body>
</html>
