<%@ page contentType="text/html;charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="ctg" uri="customtags" %>
<c:set var="applicationList" value="${requestScope.applicationList}" scope="page"/>
<c:set var="currentPage" value="${(requestScope.pageNumber==null) ? 1 : requestScope.pageNumber}" scope="page"/>
<c:set var="lastPage" value="${(requestScope.pageCount==null) ? 1 : requestScope.pageCount}" scope="page"/>
<fmt:setBundle basename="property.label" var="loc"/>
<fmt:message bundle="${loc}" key="label.vacancyName" var="vacancyName"/>
<fmt:message bundle="${loc}" key="label.description" var="description"/>
<fmt:message bundle="${loc}" key="label.coveringLetter" var="coveringLetter"/>
<fmt:message bundle="${loc}" key="label.resume" var="resume"/>
<fmt:message bundle="${loc}" key="button.viewJobInterviews" var="viewJobInterviews"/>
<fmt:message bundle="${loc}" key="button.deleteApplication" var="deleteApplication"/>
<fmt:message bundle="${loc}" key="button.viewResume" var="viewResume"/>
<fmt:message bundle="${loc}" key="button.viewCandidateProfile" var="viewCandidateProfile"/>
<fmt:message bundle="${loc}" key="label.close" var="close"/>
<fmt:message bundle="${loc}" key="label.vacancyApplications" var="vacancyApplications"/>
<fmt:message bundle="${loc}" key="label.decision" var="decision"/>
<fmt:message bundle="${loc}" key="label.noApplications" var="noApplications"/>
<fmt:message bundle="${loc}" key="label.newDecision" var="newDecision"/>
<fmt:message bundle="${loc}" key="label.noApplications" var="noApplications"/>
<fmt:message bundle="${loc}" key="label.changeDecision" var="changeDecision"/>

<script src="https://code.jquery.com/jquery.min.js"></script>
<link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" rel="stylesheet" type="text/css"/>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>

<h1>${vacancyApplications}</h1>
<div class="table-responsive">
<table class="table table-hover ">
    <thead>
    <tr>
        <th>${coveringLetter}</th>
        <th>${resume}</th>
        <th>${viewCandidateProfile}</th>
        <th>${decision}</th>
        <th>${viewJobInterviews}</th>
        <th></th>
    </tr>
    </thead>
    <tbody>

    <c:forEach var="i" begin="0" end="${applicationList.size()-1}">
        <tr>
            <td>${applicationList.get(i).coveringLetter}</td>
            <td>
                <a class="btn btn-info"
                   href="${pageContext.servletContext.contextPath}/controller?command=view_resume&applicationId=${applicationList.get(i).id}">${viewResume}</a>
            </td>
            <td>
                <a class="btn btn-info"
                   href="${pageContext.servletContext.contextPath}/controller?command=view_candidate&candidateId=${applicationList.get(i).candidate.id}">${applicationList.get(i).candidate.name}</a>
            </td>
            <td>
                <c:choose>
                    <c:when test="${not empty applicationList.get(i).decision}">
                        ${applicationList.get(i).decision}
                        <button class="btn btn-warning btn-block"
                                data-application-id="${applicationList.get(i).id}"
                                data-application-decision="${applicationList.get(i).decision}"
                                data-toggle="modal"
                                data-target="#decisionModal">
                                ${changeDecision}
                        </button>
                    </c:when>
                    <c:otherwise>
                        <button class="btn btn-info btn-block"
                                data-application-id="${applicationList.get(i).id}"
                                data-toggle="modal"
                                data-target="#decisionModal">
                            ${newDecision}
                        </button>
                    </c:otherwise>
                </c:choose>
            </td>
            <td>
                <a class="btn btn-info"
                   href="${pageContext.servletContext.contextPath}/controller?command=view_job_interviews&applicationId=${applicationList.get(i).id}">${viewJobInterviews}</a>
            </td>
        </tr>
    </c:forEach>
    </tbody>
</table>
</div>
<nav aria-label="Page navigation example">
    <ul class="pagination justify-content-center">
        <c:choose>
            <c:when test="${currentPage==1}">
                <li class="page-item disabled">
                    <a class="page-link" href="#" aria-label="Previous">
                        <span aria-hidden="true">&laquo;</span>
                        <span class="sr-only">Previous</span>
                    </a>
                </li>
            </c:when>
            <c:otherwise>
                <li class="page-item">
                    <a class="page-link"
                       href="${pageContext.servletContext.contextPath}/controller?command=view_vacancy_applications&vacancyId=${requestScope.vacancy.id}&pageNumber=${currentPage-1}"
                       aria-label="Previous">
                        <span aria-hidden="true">&laquo;</span>
                        <span class="sr-only">Previous</span>
                    </a>
                </li>
            </c:otherwise>
        </c:choose>
        <c:forEach begin="${(currentPage-3<0) ? 1 : currentPage-1}"
                   end="${(currentPage+1>lastPage) ? lastPage : currentPage+1}"
                   var="i">
            <c:choose>
                <c:when test="${currentPage eq i}">
                    <li class="page-item active">
                        <a class="page-link"
                           href="${pageContext.servletContext.contextPath}/controller?command=view_vacancy_applications&vacancyId=${requestScope.vacancy.id}&pageNumber=${i}">${i}</a>
                    </li>
                </c:when>
                <c:otherwise>
                    <li class="page-item">
                        <a class="page-link"
                           href="${pageContext.servletContext.contextPath}/controller?command=view_vacancy_applications&vacancyId=${requestScope.vacancy.id}&pageNumber=${i}">${i}</a>
                    </li>
                </c:otherwise>
            </c:choose>
        </c:forEach>
        <c:choose>
            <c:when test="${currentPage eq lastPage}">
                <li class="page-item disabled">
                    <a class="page-link" href="#" aria-label="Next">
                        <span aria-hidden="true">&raquo;</span>
                        <span class="sr-only">Next</span>
                    </a>
                </li>

            </c:when>
            <c:otherwise>
                <li class="page-item">
                    <a class="page-link"
                       href="${pageContext.servletContext.contextPath}/controller?controller?command=view_vacancy_applications&vacancyId=${requestScope.vacancy.id}&pageNumber=${currentPage+1}"
                       aria-label="Next">
                        <span aria-hidden="true">&raquo;</span>
                        <span class="sr-only">Next</span>
                    </a>
                </li>
            </c:otherwise>
        </c:choose>
    </ul>
</nav>

<div class="modal fade" id="decisionModal" tabindex="-1"
     role="dialog"
     aria-labelledby="decisionModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <form method="POST" action="controller">
                <input type="hidden" name="command"
                       value="new_decision"/>
                <input type="hidden" name="vacancyId" value="${requestScope.vacancy.id}">
                <input type="hidden" name="applicationId" value="">
                <div class="modal-header">
                    <h5 class="modal-title"
                        id="decisionModalLabel">${newDecision}</h5>
                    <button type="button" class="close"
                            data-dismiss="modal"
                            aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <div class="form-group">
                        <input type="hidden" name="vacancyId" value="${requestScope.vacancy.id}">
                        <label for="decision">${decision}</label>
                        <input type="text" id="decision" name="decision" value=""
                               required>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">${close}
                    </button>
                    <input type="submit" class="btn btn-secondary" value="${newDecision}">
                </div>
            </form>
        </div>
    </div>
</div>


<script>
    $('#decisionModal').on('show.bs.modal', function (e) {
        var applicationId = $(e.relatedTarget).data('application-id');
        $(e.currentTarget).find('input[name="applicationId"]').val(applicationId);
        var decision = $(e.relatedTarget).data('application-decision');
        $(e.currentTarget).find('input[name="decision"]').val(decision);
    });
</script>