<%@ page contentType="text/html;charset=UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<c:set var="language"
       value="${not empty param.language ? param.language : not empty language ? language : pageContext.request.locale}"
       scope="session"/>

<c:set var="nextCommand" value="${sessionScope.nextCommand}"/>
<c:set var="nextCommandPart" value="${pageContext.servletContext.contextPath}/controller?command=${nextCommand}"/>
<c:set var="nextPage" value="${not empty nextCommand?nextCommandPart : \"/jsp/main.jsp\"}"/>
<fmt:setLocale value="${language}" scope="session"/>
<fmt:setBundle basename="property.label" var="loc"/>
<fmt:message bundle="${loc}" key="label.operationFailed" var="operationFailed"/>
<fmt:message bundle="${loc}" key="label.goingHome" var="goingHome"/>
<html>
<head>
    <title>${operationFailed}</title>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/2.0.2/jquery.min.js"></script>
    <script src="../js/gradient.js"></script>
    <link href="../css/result.css" rel="stylesheet" type="text/css"/>
</head>
<body style="color: white" >
<div id="gradient">
    <h1>${operationFailed}</h1>
    <h2>${goingHome}</h2>
    <h1>${sessionScope.message}</h1>
</div>
<script>
    var delay = 2000;
    setTimeout(function () {
        window.location = "${nextPage}";}, delay);
</script>
</body>
</html>
