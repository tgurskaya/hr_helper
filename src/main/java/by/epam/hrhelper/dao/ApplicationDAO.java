package by.epam.hrhelper.dao;

import by.epam.hrhelper.entity.Application;
import by.epam.hrhelper.entity.User;
import by.epam.hrhelper.exception.DAOException;

import java.util.ArrayList;

public interface ApplicationDAO extends BaseDAO<Integer, Application> {
    ArrayList<Application> showCandidateApplication(User candidate, int first, int quantity);

    byte[] uploadResume(int applicationId) throws DAOException;

    Application findApplicationByCandidateAndVacancy(int candidateId, int vacancyId) throws DAOException;

    ArrayList<Application> showVacancyApplications(int vacancyId, int first, int quantity);

    int countApplicationWithVacancy(int vacancyId);

    boolean changeResume(int applicationId, byte[] resume);

    boolean addDecision(int applicationId, String decision);

    boolean updateCoveringLetter(String coveringLetter, int applicationId);
}
