package by.epam.hrhelper.dao;


import by.epam.hrhelper.entity.JobInterview;
import by.epam.hrhelper.exception.DAOException;

import java.util.ArrayList;

public interface JobInterviewDAO extends BaseDAO<Integer,JobInterview> {
    ArrayList<JobInterview> showApplicationInterviews(int applicationId,int first, int quantity) throws DAOException;
    boolean updateOpinion(String opinion, int jobInterviewId);
}
