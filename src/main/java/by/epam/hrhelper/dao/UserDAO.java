package by.epam.hrhelper.dao;

import by.epam.hrhelper.entity.User;
import by.epam.hrhelper.exception.DAOException;

import java.util.List;

public interface UserDAO extends BaseDAO<Integer,User>{
    User findByEmail(String email) throws DAOException;
    byte[] uploadPhoto(int userId) throws DAOException;
    boolean insertPhoto(int userId, byte[] photo);
    boolean changePassword(int userId, String password);
    int countAllUsers();
    List<User> showUsers(int i, int recordsPerPage) throws DAOException;
    boolean blockUser(int userId) throws DAOException;
    boolean unblockUser(int userId) throws DAOException;
}
