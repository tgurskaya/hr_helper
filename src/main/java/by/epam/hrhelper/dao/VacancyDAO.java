package by.epam.hrhelper.dao;

import by.epam.hrhelper.entity.User;
import by.epam.hrhelper.entity.Vacancy;
import by.epam.hrhelper.exception.DAOException;

import java.util.List;

public interface VacancyDAO extends BaseDAO<Integer,Vacancy> {
    List<Vacancy> showVacancies(int first, int last,String status) throws DAOException;
    int countAllVacancies(String status) throws DAOException;
    List<Vacancy> showHrVacancies(User hrManager, int first, int quantity) throws DAOException;
}
