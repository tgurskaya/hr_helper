package by.epam.hrhelper.dao.impl;

import by.epam.hrhelper.connectionpool.ProxyConnection;
import by.epam.hrhelper.dao.ApplicationDAO;
import by.epam.hrhelper.entity.Application;
import by.epam.hrhelper.entity.User;
import by.epam.hrhelper.entity.Vacancy;
import by.epam.hrhelper.exception.DAOException;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

public class ApplicationDAOImpl implements ApplicationDAO {

    private static final String SQL_SELECT_CANDIDATE_APPLICATION = "SELECT a.application_id,  a.covering_letter,a.decision, v.vacancy_id, v.vacancy_name,v.description FROM application a inner join vacancy v on v.vacancy_id=a.vacancy_id where candidate_id=? ORDER BY v.vacancy_id DESC LIMIT ?,?";
    private static final String SQL_UPLOAD_RESUME = "SELECT resume FROM application where application_id=?";
    private static final String SQL_COUNT_CANDIDATE_APPLICATIONS = "SELECT count(*) FROM application where candidate_id=?";
    private static final String SQL_CREATE_APPLICATION = "INSERT INTO application (candidate_id, covering_letter, vacancy_id, resume) VALUES (?,?,?,?)";
    private static final String SQL_DELETE_APPLICATION = "DELETE FROM application where application_id=?";
    private static final String SQL_FIND_APPLICATION_BY_ID = "SELECT candidate_id,covering_letter,vacancy_id,decision FROM application where application_id=?";
    private static final String SQL_UPDATE_COVERING_LETTER = "UPDATE application SET covering_letter=? WHERE application_id=?";
    private static final String SQL_UPDATE_DECISION = "UPDATE application SET decision=? WHERE application_id=?";
    private static final String SQL_FIND_BY_CANDIDATE_AND_VACANCY = "SELECT application_id from application where candidate_id=? and vacancy_id=?";
    private static final String SQL_SELECT_VACANCY_APPLICATION = "SELECT application_id,candidate_id,name,covering_letter,decision FROM application a inner join user on user_id=candidate_id where vacancy_id=? LIMIT ?,?";
    private static final String SQL_COUNT_VACANCY_APPLICATION = "SELECT count(*) FROM application where vacancy_id=?";
    private static final String SQL_UPDATE_RESUME = "UPDATE application SET resume=? WHERE application_id=?";
    private static final String SQL_UPDATE_APPLICATION = "UPDATE application SET covering_letter=,decision=? WHERE application_id=?";
    private static final String APPLICATION_ID = "application_id";
    private static final String DECISION = "decision";
    private static final String COVERING_LETTER = "covering_letter";
    private static final String CANDIDATE_ID = "candidate_id";
    private static final String VACANCY_ID = "vacancyId";
    private static final String VACANCY_NAME = "vacancy_name";
    private static final String DESCRIPTION = "description";
    private static final String NAME = "name";
    private static final String RESUME = "resume";


    @Override
    public Application createEntityFromResultSet(ResultSet resultSet) throws DAOException {
        Application application = new Application();
        try {
            application.setCoveringLetter(resultSet.getString(COVERING_LETTER));
            application.setDecision(resultSet.getString(DECISION));
            Vacancy vacancy = new Vacancy();
            vacancy.setId(resultSet.getInt(VACANCY_ID));
            application.setVacancy(vacancy);
            User user = new User();
            user.setUserType(User.UserType.CANDIDATE);
            user.setId(resultSet.getInt(CANDIDATE_ID));
            application.setCandidate(user);
        } catch (SQLException e) {
            throw new DAOException(e);
        }
        return application;
    }

    @Override
    public String getFindByIdSQLStatement() {
        return SQL_FIND_APPLICATION_BY_ID;
    }

    @Override
    public String getDeleteEntitySQLStatement() {
        return SQL_DELETE_APPLICATION;
    }

    @Override
    public boolean create(Application entity) throws DAOException {
        ProxyConnection connection = getConnection();
        PreparedStatement preparedStatement = null;
        try {
            preparedStatement = connection.prepareStatement(SQL_CREATE_APPLICATION);
            preparedStatement.setInt(1, entity.getCandidate().getId());
            preparedStatement.setString(2, entity.getCoveringLetter());
            preparedStatement.setInt(3, entity.getVacancy().getId());
            preparedStatement.setBytes(4, entity.getResume());
            if (preparedStatement.executeUpdate() == 1) {
                return true;
            } else {
                return false;
            }
        } catch (SQLException e) {
            throw new DAOException(e, e.getErrorCode());
        } finally {
            closeStatement(preparedStatement);
            closeConnection(connection);
        }
    }

    @Override
    public boolean update(Application entity) throws DAOException {
        ProxyConnection connection = getConnection();
        PreparedStatement preparedStatement = null;
        try {
            preparedStatement = connection.prepareStatement(SQL_UPDATE_APPLICATION);
            preparedStatement.setString(1, entity.getCoveringLetter());
            preparedStatement.setString(2, entity.getDecision());
            preparedStatement.setInt(3, entity.getId());
            return preparedStatement.executeUpdate() == 1;
        } catch (SQLException e) {
            throw new DAOException(e);
        } finally {
            closeStatement(preparedStatement);
            closeConnection(connection);
        }
    }

    @Override
    public String getCountWithIdSQLStatement() {
        return SQL_COUNT_CANDIDATE_APPLICATIONS;
    }


    @Override
    public ArrayList<Application> showCandidateApplication(User candidate, int first, int quantity) {
        ArrayList<Application> result = new ArrayList<>();
        PreparedStatement statement = null;
        ProxyConnection proxyConnection = getConnection();
        try {
            statement = proxyConnection.prepareStatement(SQL_SELECT_CANDIDATE_APPLICATION);
            statement.setInt(1, candidate.getId());
            statement.setInt(2, first);
            statement.setInt(3, quantity);
            ResultSet resultSet = statement.executeQuery();
            while (resultSet.next()) {
                Application application = new Application();
                application.setCandidate(candidate);
                application.setCoveringLetter(resultSet.getString(COVERING_LETTER));
                application.setId(resultSet.getInt(APPLICATION_ID));
                application.setDecision(resultSet.getString(DECISION));
                Vacancy vacancy = new Vacancy();
                vacancy.setId(resultSet.getInt(VACANCY_ID));
                vacancy.setName(resultSet.getString(VACANCY_NAME));
                vacancy.setDescription(resultSet.getString(DESCRIPTION));
                application.setVacancy(vacancy);
                result.add(application);
            }
        } catch (SQLException e) {
            LOGGER.catching(e);

        } finally {
            closeStatement(statement);
            closeConnection(proxyConnection);
        }
        return result;
    }

    @Override
    public ArrayList<Application> showVacancyApplications(int vacancyId, int first, int quantity) {
        ArrayList<Application> result = new ArrayList<>();
        PreparedStatement statement = null;
        ProxyConnection proxyConnection = getConnection();
        try {
            statement = proxyConnection.prepareStatement(SQL_SELECT_VACANCY_APPLICATION);
            statement.setInt(1, vacancyId);
            statement.setInt(2, first);
            statement.setInt(3, quantity);
            ResultSet resultSet = statement.executeQuery();
            while (resultSet.next()) {
                Application application = new Application();
                User user = new User();
                user.setId(resultSet.getInt(CANDIDATE_ID));
                user.setName(resultSet.getString(NAME));
                application.setCandidate(user);
                application.setCoveringLetter(resultSet.getString(COVERING_LETTER));
                application.setId(resultSet.getInt(APPLICATION_ID));
                application.setDecision(resultSet.getString(DECISION));
                Vacancy vacancy = new Vacancy();
                vacancy.setId(vacancyId);
                application.setVacancy(vacancy);
                result.add(application);
            }
        } catch (SQLException e) {
            LOGGER.catching(e);

        } finally {
            closeStatement(statement);
            closeConnection(proxyConnection);
        }
        return result;
    }

    @Override
    public int countApplicationWithVacancy(int vacancyId) {
        PreparedStatement statement = null;
        ProxyConnection proxyConnection = getConnection();
        try {
            statement = proxyConnection.prepareStatement(SQL_COUNT_VACANCY_APPLICATION);
            statement.setInt(1, vacancyId);
            ResultSet resultSet = statement.executeQuery();
            if (resultSet.next()) {
                return resultSet.getInt(1);
            } else {
                return 0;
            }
        } catch (SQLException e) {
            LOGGER.catching(e);
            return 0;
        } finally {
            closeStatement(statement);
            closeConnection(proxyConnection);
        }
    }

    @Override
    public boolean changeResume(int applicationId, byte[] resume) {
        ProxyConnection connection = getConnection();
        PreparedStatement preparedStatement = null;
        try {
            preparedStatement = connection.prepareStatement(SQL_UPDATE_RESUME);
            preparedStatement.setBytes(1, resume);
            preparedStatement.setInt(2, applicationId);
            return preparedStatement.executeUpdate() == 1;
        } catch (SQLException e) {
            return false;
        } finally {
            closeStatement(preparedStatement);
            closeConnection(connection);
        }
    }

    @Override
    public boolean addDecision(int applicationId, String decision) {
        ProxyConnection connection = getConnection();
        PreparedStatement preparedStatement = null;
        try {
            preparedStatement = connection.prepareStatement(SQL_UPDATE_DECISION);
            preparedStatement.setString(1, decision);
            preparedStatement.setInt(2, applicationId);
            return preparedStatement.executeUpdate() == 1;
        } catch (SQLException e) {
            LOGGER.catching(e);
            return false;
        } finally {
            closeStatement(preparedStatement);
            closeConnection(connection);
        }
    }

    @Override
    public boolean updateCoveringLetter(String coveringLetter, int applicationId) {
        ProxyConnection connection = getConnection();
        PreparedStatement preparedStatement = null;
        try {
            preparedStatement = connection.prepareStatement(SQL_UPDATE_COVERING_LETTER);
            preparedStatement.setString(1, coveringLetter);
            preparedStatement.setInt(2, applicationId);
            return preparedStatement.executeUpdate() == 1;
        } catch (SQLException e) {
            return false;
        } finally {
            closeStatement(preparedStatement);
            closeConnection(connection);
        }
    }

    @Override
    public byte[] uploadResume(int applicationId) throws DAOException {
        ProxyConnection connection = getConnection();
        PreparedStatement preparedStatement = null;
        try {
            preparedStatement = connection.prepareStatement(SQL_UPLOAD_RESUME);
            preparedStatement.setInt(1, applicationId);
            ResultSet resultSet = preparedStatement.executeQuery();
            if (resultSet.next()) {
                return resultSet.getBytes(RESUME);
            } else {
                return null;
            }
        } catch (SQLException e) {
            throw new DAOException(e);
        } finally {
            if (preparedStatement != null) {
                closeStatement(preparedStatement);
                closeConnection(connection);
            }
        }
    }

    @Override
    public Application findApplicationByCandidateAndVacancy(int candidateId, int vacancyId) throws DAOException {
        ProxyConnection connection = getConnection();
        PreparedStatement preparedStatement = null;
        try {
            preparedStatement = connection.prepareStatement(SQL_FIND_BY_CANDIDATE_AND_VACANCY);
            preparedStatement.setInt(1, candidateId);
            preparedStatement.setInt(2, vacancyId);
            ResultSet resultSet = preparedStatement.executeQuery();
            if (resultSet.next()) {
                Application application = new Application();
                application.setId(resultSet.getInt(APPLICATION_ID));
                return application;
            } else {
                return null;
            }
        } catch (SQLException e) {
            throw new DAOException(e, e.getErrorCode());
        } finally {
            closeStatement(preparedStatement);
            closeConnection(connection);
        }
    }
}
