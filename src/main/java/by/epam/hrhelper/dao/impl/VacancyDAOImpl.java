package by.epam.hrhelper.dao.impl;

import by.epam.hrhelper.connectionpool.ProxyConnection;
import by.epam.hrhelper.dao.VacancyDAO;
import by.epam.hrhelper.entity.User;
import by.epam.hrhelper.entity.Vacancy;
import by.epam.hrhelper.exception.DAOException;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

public class VacancyDAOImpl implements VacancyDAO {

    private static final String SQL_SELECT_OPEN_VACANCIES = "SELECT vacancy_id,vacancy_name,salary,requirements,responsibilities,description,status FROM vacancy Where status='OPEN' ORDER BY vacancy_id DESC LIMIT ?,? ;";
    private static final String SQL_SELECT_ALL_VACANCIES = "SELECT vacancy_id,vacancy_name,salary,requirements,responsibilities,description,status FROM vacancy ORDER BY vacancy_id DESC LIMIT ?,? ;";
    private static final String SQL_COUNT_ALL_VACANCIES = "SELECT count(*) FROM vacancy;";
    private static final String SQL_COUNT_ALL_OPEN_VACANCIES = "SELECT count(*) FROM vacancy where status='OPEN';";
    private static final String SQL_SELECT_HR_VACANCIES = "SELECT vacancy_id,vacancy_name,salary,description,requirements,responsibilities, status FROM vacancy where hr_id=? ORDER BY vacancy_id DESC LIMIT ?,? ";
    private static final String SQL_COUNT_HR_VACANCIES = "SELECT count(*) FROM vacancy where hr_id=?";
    private static final String SQL_UPDATE_VACANCY = "UPDATE vacancy SET vacancy_name=?,salary=?,description=?, responsibilities=?,requirements=?,status=?  WHERE vacancy_id=?";
    private static final String SQL_DELETE_VACANCY = "DELETE FROM vacancy where vacancy_id=?";
    private static final String SQL_FIND_VACANCY_BY_ID = "SELECT vacancy_id,vacancy_name,salary,description,requirements,responsibilities,hr_id, status FROM vacancy where vacancy_id=?";
    private static final String SQL_CREATE_VACANCY = "INSERT INTO vacancy(vacancy_name,salary,description,responsibilities,requirements,hr_id, status) VALUES (?,?,?,?,?,?,?)";


    public int countAllVacancies(String status) {
        Statement statement = null;
        ProxyConnection proxyConnection = getConnection();
        try {
            statement = proxyConnection.createStatement();
            ResultSet resultSet;
            if (status.toUpperCase().equals("OPEN"))
                resultSet = statement.executeQuery(SQL_COUNT_ALL_OPEN_VACANCIES);
            else resultSet = statement.executeQuery(SQL_COUNT_ALL_VACANCIES);
            if (resultSet.next()) {
                return resultSet.getInt(1);
            } else {
                return 0;
            }
        } catch (SQLException e) {
            LOGGER.catching(e);
            return 0;
        } finally {
            closeStatement(statement);
            closeConnection(proxyConnection);
        }
    }

    @Override
    public List<Vacancy> showHrVacancies(User hrManager, int first, int quantity) throws DAOException {
        ArrayList<Vacancy> result = new ArrayList<>();
        PreparedStatement statement = null;
        ProxyConnection proxyConnection = getConnection();
        try {
            statement = proxyConnection.prepareStatement(SQL_SELECT_HR_VACANCIES);
            statement.setInt(1, hrManager.getId());
            statement.setInt(2, first);
            statement.setInt(3, quantity);
            ResultSet resultSet = statement.executeQuery();
            while (resultSet.next()) {
                Vacancy vacancy = createEntityFromResultSet(resultSet);
                vacancy.setHrId(hrManager.getId());
                result.add(vacancy);
            }
        } catch (SQLException e) {
            throw new DAOException(e);
        } finally {
            closeStatement(statement);
            closeConnection(proxyConnection);
        }
        return result;
    }


    @Override
    public Vacancy createEntityFromResultSet(ResultSet resultSet) throws DAOException {
        Vacancy vacancy = new Vacancy();
        try {
            vacancy.setId(resultSet.getInt("vacancy_id"));
            vacancy.setName(resultSet.getString("vacancy_name"));
            vacancy.setDescription(resultSet.getString("description"));
            vacancy.setRequirements(resultSet.getString("requirements"));
            vacancy.setResponsibilities(resultSet.getString("responsibilities"));
            vacancy.setSalary(resultSet.getString("salary"));
            vacancy.setStatus(Vacancy.VacancyStatus.valueOf(resultSet.getString("status").toUpperCase()));
            return vacancy;
        } catch (SQLException e) {
            throw new DAOException(e);
        }
    }

    @Override
    public String getFindByIdSQLStatement() {
        return SQL_FIND_VACANCY_BY_ID;
    }

    @Override
    public String getDeleteEntitySQLStatement() {
        return SQL_DELETE_VACANCY;
    }

    @Override
    public boolean create(Vacancy entity) throws DAOException {
        ProxyConnection connection = getConnection();
        PreparedStatement preparedStatement = null;
        try {
            preparedStatement = connection.prepareStatement(SQL_CREATE_VACANCY);
            preparedStatement.setString(1, entity.getName());
            preparedStatement.setString(2, entity.getSalary());
            preparedStatement.setString(3, entity.getDescription());
            preparedStatement.setString(4, entity.getResponsibilities());
            preparedStatement.setString(5, entity.getRequirements());
            preparedStatement.setInt(6, entity.getHrId());
            preparedStatement.setString(7, entity.getStatus().name());
            if (preparedStatement.executeUpdate() == 1) {
                return true;
            } else {
                return false;
            }
        } catch (SQLException e) {
            throw new DAOException(e, e.getErrorCode());
        } finally {
            closeStatement(preparedStatement);
            closeConnection(connection);
        }
    }

    @Override
    public boolean update(Vacancy entity) throws DAOException {
        ProxyConnection connection = getConnection();
        PreparedStatement preparedStatement = null;
        try {
            preparedStatement = connection.prepareStatement(SQL_UPDATE_VACANCY);
            preparedStatement.setString(1, entity.getName());
            preparedStatement.setString(2, entity.getSalary());
            preparedStatement.setString(3, entity.getDescription());
            preparedStatement.setString(4, entity.getResponsibilities());
            preparedStatement.setString(5, entity.getRequirements());
            preparedStatement.setString(6, entity.getStatus().name());
            preparedStatement.setInt(7, entity.getId());
            return preparedStatement.executeUpdate() == 1;
        } catch (SQLException e) {
            throw new DAOException(e);
        } finally {
            closeStatement(preparedStatement);
            closeConnection(connection);
        }
    }

    @Override
    public String getCountWithIdSQLStatement() {
        return SQL_COUNT_HR_VACANCIES;
    }


    @Override
    public List<Vacancy> showVacancies(int first, int last, String status) throws DAOException {
        List<Vacancy> allVacancies = new ArrayList();
        PreparedStatement preparedStatement = null;
        ProxyConnection connection = getConnection();
        try {
            if (status.toUpperCase().equals("OPEN")) {
                preparedStatement = connection.prepareStatement(SQL_SELECT_OPEN_VACANCIES);
            } else {
                preparedStatement = connection.prepareStatement(SQL_SELECT_ALL_VACANCIES);
            }
            preparedStatement.setInt(1, first);
            preparedStatement.setInt(2, last);
            ResultSet resultSet = preparedStatement.executeQuery();
            while (resultSet.next()) {
                Vacancy vacancy = createEntityFromResultSet(resultSet);
                allVacancies.add(vacancy);
            }
        } catch (SQLException e) {
            throw new DAOException(e);
        } finally {
            closeStatement(preparedStatement);
            closeConnection(connection);
        }
        return allVacancies;
    }
}
