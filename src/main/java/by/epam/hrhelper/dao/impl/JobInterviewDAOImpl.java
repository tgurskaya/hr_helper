package by.epam.hrhelper.dao.impl;

import by.epam.hrhelper.connectionpool.ProxyConnection;
import by.epam.hrhelper.dao.JobInterviewDAO;
import by.epam.hrhelper.entity.Application;
import by.epam.hrhelper.entity.JobInterview;
import by.epam.hrhelper.exception.DAOException;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;

public class JobInterviewDAOImpl implements JobInterviewDAO {

    private static final String SQL_SELECT_APPLICATION_INTERVIEWS = "SELECT job_interview_id,interview_type, interview_datetime, opinion_about_candidate, job_interview_place FROM job_interview where job_interview_application_id=? LIMIT ?,?";
    private static final String SQL_COUNT_APPLICATION_INTERVIEWS = "SELECT count(*) FROM job_interview where job_interview_application_id=?";
    private static final String SQL_CREATE_JOB_INTERVIEW = "INSERT INTO job_interview (interview_type, interview_datetime, job_interview_application_id, job_interview_place) VALUES (?,?,?,?)";
    private static final String SQL_DELETE_JOB_INTERVIEW = "DELETE FROM job_interview where job_interview_id=?";
    private static final String SQL_FIND_JOB_INTERVIEW_BY_ID = "SELECT job_interview_id,interview_type, interview_datetime, opinion_about_candidate, job_interview_application_id, job_interview_place FROM job_interview where job_interview_id=?";
    private static final String SQL_UPDATE_JOB_INTERVIEW = "UPDATE job_interview SET interview_type=?,interview_datetime=?,job_interview_place=? WHERE job_interview_id=?";
    private static final String SQL_SET_OPINION = "UPDATE job_interview SET opinion_about_candidate=? WHERE job_interview_id=?";

    @Override
    public JobInterview createEntityFromResultSet(ResultSet resultSet) throws DAOException {
        JobInterview jobInterview = new JobInterview();
        try {
            jobInterview.setId(resultSet.getInt("job_interview_id"));
            jobInterview.setDateAndTimeOfInterview(resultSet.getString("interview_datetime"), DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss.S"));
            jobInterview.setJobInterviewType(resultSet.getString("interview_type"));
            jobInterview.setPlace(resultSet.getString("job_interview_place"));
            jobInterview.setOpinionAboutCandidate("opinion_about_candidate");
            return jobInterview;
        } catch (SQLException e) {
            throw new DAOException(e);
        }
    }

    @Override
    public String getFindByIdSQLStatement() {
        return SQL_FIND_JOB_INTERVIEW_BY_ID;
    }

    @Override
    public String getDeleteEntitySQLStatement() {
        return SQL_DELETE_JOB_INTERVIEW;
    }

    @Override
    public boolean create(JobInterview entity) throws DAOException {
        ProxyConnection connection = getConnection();
        PreparedStatement preparedStatement = null;
        try {
            preparedStatement = connection.prepareStatement(SQL_CREATE_JOB_INTERVIEW);
            preparedStatement.setString(1, entity.getJobInterviewType());
            preparedStatement.setString(2, entity.getDateAndTimeOfInterview().toString());
            preparedStatement.setInt(3, entity.getApplication().getId());
            preparedStatement.setString(4, entity.getPlace());
            if (preparedStatement.executeUpdate() == 1) {
                return true;
            } else {
                return false;
            }
        } catch (SQLException e) {
            throw new DAOException(e, e.getErrorCode());
        } finally {
            closeStatement(preparedStatement);
            closeConnection(connection);
        }
    }

    @Override
    public boolean update(JobInterview entity) throws DAOException {
        ProxyConnection connection = getConnection();
        PreparedStatement preparedStatement = null;
        try {
            preparedStatement = connection.prepareStatement(SQL_UPDATE_JOB_INTERVIEW);
            preparedStatement.setString(1, entity.getJobInterviewType());
            preparedStatement.setString(2, String.valueOf(entity.getDateAndTimeOfInterview()));
            preparedStatement.setString(3, entity.getPlace());
            preparedStatement.setInt(4, entity.getId());
            if (preparedStatement.executeUpdate() == 1) {
                return true;
            } else {
                return false;
            }
        } catch (SQLException e) {
            throw new DAOException(e);
        } finally {
            closeStatement(preparedStatement);
            closeConnection(connection);
        }
    }

    @Override
    public String getCountWithIdSQLStatement() {
        return SQL_COUNT_APPLICATION_INTERVIEWS;
    }

    @Override
    public ArrayList<JobInterview> showApplicationInterviews(int applicationId, int first, int quantity) throws DAOException {
        ArrayList<JobInterview> result = new ArrayList<>();
        PreparedStatement statement = null;
        ProxyConnection proxyConnection = getConnection();
        try {
            statement = proxyConnection.prepareStatement(SQL_SELECT_APPLICATION_INTERVIEWS);
            statement.setInt(1, applicationId);
            statement.setInt(2, first);
            statement.setInt(3, quantity);
            ResultSet resultSet = statement.executeQuery();
            while (resultSet.next()) {
                JobInterview jobInterview = createEntityFromResultSet(resultSet);
                Application application = new Application();
                application.setId(applicationId);
                jobInterview.setApplication(application);
                jobInterview.setOpinionAboutCandidate(resultSet.getString("opinion_about_candidate"));
                jobInterview.setId(resultSet.getInt("job_interview_id"));
                result.add(jobInterview);
            }
        } catch (SQLException e) {
            LOGGER.catching(e);

        } finally {
            closeStatement(statement);
            closeConnection(proxyConnection);
        }
        return result;
    }

    @Override
    public boolean updateOpinion(String opinion, int jobInterviewId) {
        ProxyConnection connection = getConnection();
        PreparedStatement preparedStatement = null;
        try {
            preparedStatement = connection.prepareStatement(SQL_SET_OPINION);
            preparedStatement.setString(1, opinion);
            preparedStatement.setInt(2, jobInterviewId);
            return preparedStatement.executeUpdate() == 1;
        } catch (SQLException e) {
            LOGGER.catching(e);
            return false;
        } finally {
            closeStatement(preparedStatement);
            closeConnection(connection);
        }
    }

}
