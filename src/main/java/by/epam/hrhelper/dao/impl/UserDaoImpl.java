package by.epam.hrhelper.dao.impl;

import by.epam.hrhelper.connectionpool.ProxyConnection;
import by.epam.hrhelper.dao.UserDAO;
import by.epam.hrhelper.entity.User;
import by.epam.hrhelper.exception.DAOException;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

public class UserDaoImpl implements UserDAO {

    private static final String SQL_FIND_USER_BY_EMAIL = "SELECT user_id,name,photo,birthday,type,email,password,phone,is_blocked FROM user where email=?";
    private static final String SQL_CREATE_USER = "INSERT INTO user(name, birthday,email,password,phone,type) VALUES (?,?,?,?,?,?)";
    private static final String SQL_UPLOAD_PHOTO = "SELECT photo FROM user where user_id=?";
    private static final String SQL_DELETE_USER = "DELETE FROM user where user_id=?";
    private static final String SQL_INSERT_PHOTO = "UPDATE user SET photo=? WHERE user_id=?";
    private static final String SQL_UPDATE_USER = "UPDATE user SET birthday=?, name=?,email=?,phone=? WHERE user_id=?";
    private static final String SQL_FIND_USER_BY_ID = "SELECT user_id,birthday,name,type,email,password,phone,is_blocked  FROM user where user_id=?";
    private static final String SQL_UPDATE_PASSWORD = "UPDATE user SET password=? WHERE user_id=?";
    private static final String SQL_SELECT_USERS = "SELECT user_id,name,birthday,phone,email,phone,type,password,is_blocked FROM user LIMIT ?,? ";
    private static final String SQL_COUNT_USERS = "SELECT count(*) FROM user";
    private static final String SQL_UPDATE_BLOCK = "UPDATE user SET is_blocked=? WHERE user_id=?";

    @Override
    public User findByEmail(String email) {
        User user;
        PreparedStatement preparedStatement = null;
        ProxyConnection proxyConnection = getConnection();
        try {
            preparedStatement = proxyConnection.prepareStatement(SQL_FIND_USER_BY_EMAIL);
            preparedStatement.setString(1, email);
            ResultSet resultSet = preparedStatement.executeQuery();
            if (resultSet.next()) {
                user = createEntityFromResultSet(resultSet);
                user.setPassword(resultSet.getString("password"));
            } else {
                return null;
            }
        } catch (SQLException | DAOException e) {
            LOGGER.catching(e);
            return null;
        } finally {
            closeStatement(preparedStatement);
            closeConnection(proxyConnection);
        }
        return user;
    }

    public int countAllUsers() {
        Statement statement = null;
        ProxyConnection proxyConnection = getConnection();
        try {
            statement = proxyConnection.createStatement();
            ResultSet resultSet;
            resultSet = statement.executeQuery(SQL_COUNT_USERS);
            if (resultSet.next()) {
                return resultSet.getInt(1);
            } else {
                return 0;
            }
        } catch (SQLException e) {
            LOGGER.catching(e);
            return 0;
        } finally {
            closeStatement(statement);
            closeConnection(proxyConnection);
        }
    }

    @Override
    public byte[] uploadPhoto(int userId) throws DAOException {
        ProxyConnection connection = getConnection();
        PreparedStatement preparedStatement = null;
        try {
            preparedStatement = connection.prepareStatement(SQL_UPLOAD_PHOTO);
            preparedStatement.setInt(1, userId);
            ResultSet resultSet = preparedStatement.executeQuery();
            if (resultSet.next()) {
                return resultSet.getBytes("photo");
            } else {
                return null;
            }
        } catch (SQLException e) {
            throw new DAOException(e);
        } finally {
            if (preparedStatement != null) {
                closeStatement(preparedStatement);
                closeConnection(connection);
            }
        }
    }

    @Override
    public boolean insertPhoto(int userId, byte[] photo) {
        PreparedStatement preparedStatement = null;
        ProxyConnection proxyConnection = getConnection();
        try {
            preparedStatement = proxyConnection.prepareStatement(SQL_INSERT_PHOTO);
            preparedStatement.setBytes(1, photo);
            preparedStatement.setInt(2, userId);
            return preparedStatement.executeUpdate() == 1;
        } catch (SQLException ex) {
            return false;
        } finally {
            closeStatement(preparedStatement);
            closeConnection(proxyConnection);
        }
    }

    @Override
    public boolean changePassword(int userId, String password) {
        PreparedStatement preparedStatement = null;
        ProxyConnection proxyConnection = getConnection();
        try {
            preparedStatement = proxyConnection.prepareStatement(SQL_UPDATE_PASSWORD);
            preparedStatement.setString(1, password);
            preparedStatement.setInt(2, userId);
            return preparedStatement.executeUpdate() == 1;
        } catch (SQLException e) {
            return false;
        } finally {
            if (preparedStatement != null) {
                closeStatement(preparedStatement);
                closeConnection(proxyConnection);
            }
        }

    }

    public List<User> showUsers(int first, int last) throws DAOException {
        List<User> allUsers = new ArrayList();
        PreparedStatement preparedStatement = null;
        ProxyConnection connection = getConnection();
        try {
            preparedStatement = connection.prepareStatement(SQL_SELECT_USERS);
            preparedStatement.setInt(1, first);
            preparedStatement.setInt(2, last);
            ResultSet resultSet = preparedStatement.executeQuery();
            while (resultSet.next()) {
                User user = createEntityFromResultSet(resultSet);
                allUsers.add(user);
            }
        } catch (SQLException e) {
            throw new DAOException(e);
        } finally {
            closeStatement(preparedStatement);
            closeConnection(connection);
        }
        return allUsers;

    }

    @Override
    public boolean blockUser(int userId) throws DAOException {
        return changeBlock(userId, 0);
    }

    @Override
    public boolean unblockUser(int userId) throws DAOException {
        return changeBlock(userId, 1);
    }

    public boolean changeBlock(int userId, int block) throws DAOException {
        PreparedStatement preparedStatement = null;
        ProxyConnection connection = getConnection();
        try {
            preparedStatement = connection.prepareStatement(SQL_UPDATE_BLOCK);
            preparedStatement.setInt(1, block);
            preparedStatement.setInt(2, userId);
            return preparedStatement.executeUpdate() == 1;
        } catch (SQLException e) {
            throw new DAOException(e, e.getErrorCode());
        } finally {
            closeStatement(preparedStatement);
            closeConnection(connection);
        }
    }

    @Override
    public User createEntityFromResultSet(ResultSet resultSet) throws DAOException {
        try {
            User user = new User();
            user.setUserType(User.UserType.valueOf(resultSet.getString("type").toUpperCase()));
            user.setEmail(resultSet.getString("email"));
            user.setPhone(resultSet.getString("phone"));
            user.setName(resultSet.getString("name"));
            user.setBirthday(resultSet.getString("birthday"));
            user.setId(resultSet.getInt("user_id"));
            user.setBlocked(resultSet.getInt("is_blocked") == 0);
            return user;
        } catch (SQLException e) {
            throw new DAOException(e);
        }
    }

    @Override
    public User findEntityById(Integer id) throws DAOException {
        PreparedStatement preparedStatement = null;
        ProxyConnection proxyConnection = getConnection();
        try {
            preparedStatement = proxyConnection.prepareStatement(getFindByIdSQLStatement());
            preparedStatement.setInt(1, id.intValue());
            ResultSet resultSet = preparedStatement.executeQuery();
            if (resultSet.next()) {
                User user = createEntityFromResultSet(resultSet);
                user.setPassword(resultSet.getString("password"));
                return user;
            } else {
                return null;
            }
        } catch (SQLException e) {
            throw new DAOException(e);
        } finally {
            if (preparedStatement != null) {
                closeStatement(preparedStatement);
                closeConnection(proxyConnection);
            }
        }
    }

    @Override
    public String getFindByIdSQLStatement() {
        return SQL_FIND_USER_BY_ID;
    }

    @Override
    public String getDeleteEntitySQLStatement() {
        return SQL_DELETE_USER;
    }


    @Override
    public boolean create(User user) throws DAOException {
        ProxyConnection connection = getConnection();
        PreparedStatement preparedStatement = null;
        try {
            preparedStatement = connection.prepareStatement(SQL_CREATE_USER);
            preparedStatement.setString(1, user.getName());
            preparedStatement.setString(2, user.getBirthday().toString());
            preparedStatement.setString(3, user.getEmail());
            preparedStatement.setString(4, user.getPassword());
            preparedStatement.setString(5, user.getPhone());
            preparedStatement.setString(6, user.getUserType().name());
            return preparedStatement.executeUpdate() == 1;
        } catch (SQLException e) {
            throw new DAOException(e, e.getErrorCode());
        } finally {
            closeStatement(preparedStatement);
            closeConnection(connection);
        }
    }


    @Override
    public boolean update(User user) throws DAOException {
        ProxyConnection connection = getConnection();
        PreparedStatement preparedStatement = null;
        try {
            preparedStatement = connection.prepareStatement(SQL_UPDATE_USER);
            preparedStatement.setString(1, user.getBirthday().toString());
            preparedStatement.setString(2, user.getName());
            preparedStatement.setString(3, user.getEmail());
            preparedStatement.setString(4, user.getPhone());
            preparedStatement.setInt(5, user.getId());
            return preparedStatement.executeUpdate() == 1;
        } catch (SQLException e) {
            throw new DAOException(e, e.getErrorCode());
        } finally {
            closeStatement(preparedStatement);
            closeConnection(connection);
        }
    }

    @Override
    public String getCountWithIdSQLStatement() {
        return null;
    }


}
