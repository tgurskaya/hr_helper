package by.epam.hrhelper.dao;

import by.epam.hrhelper.connectionpool.ConnectionPool;
import by.epam.hrhelper.connectionpool.ProxyConnection;
import by.epam.hrhelper.entity.Entity;
import by.epam.hrhelper.exception.DAOException;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

public interface BaseDAO<K extends Number, T extends Entity> {

    Logger LOGGER = LogManager.getLogger(BaseDAO.class);

    T createEntityFromResultSet(ResultSet resultSet) throws DAOException;
    String getFindByIdSQLStatement();
    String getDeleteEntitySQLStatement();
    boolean create(T entity) throws DAOException;
    boolean update(T entity) throws DAOException;

    default boolean delete(K id){
        PreparedStatement preparedStatement = null;
        ProxyConnection proxyConnection = getConnection();
        try {
            preparedStatement = proxyConnection.prepareStatement(getDeleteEntitySQLStatement());
            preparedStatement.setInt(1, id.intValue());
            return preparedStatement.executeUpdate() == 1;
        } catch (SQLException ex) {
            LOGGER.catching(ex);
            return false;
        } finally {
            closeStatement(preparedStatement);
            closeConnection(proxyConnection);
        }
    }

    default int countWithId(int id){
        PreparedStatement statement = null;
        ProxyConnection proxyConnection = getConnection();
        try {
            statement = proxyConnection.prepareStatement(getCountWithIdSQLStatement());
            statement.setInt(1,id);
            ResultSet resultSet=statement.executeQuery();
            if (resultSet.next()) {
                return resultSet.getInt(1);
            } else {
                return 0;
            }
        } catch (SQLException e) {
            LOGGER.catching(e);
            return 0;
        } finally {
            closeStatement(statement);
            closeConnection(proxyConnection);
        }
    }

    String getCountWithIdSQLStatement();


    default T findEntityById(K id) throws DAOException{
        PreparedStatement preparedStatement = null;
        ProxyConnection proxyConnection = getConnection();
        try {
            preparedStatement = proxyConnection.prepareStatement(getFindByIdSQLStatement());
            preparedStatement.setInt(1, id.intValue());
            ResultSet resultSet = preparedStatement.executeQuery();
            if (resultSet.next()) {
                return createEntityFromResultSet(resultSet);
            } else {
                return null;
            }
        } catch (SQLException e) {
            throw new DAOException(e);
        } finally {
            if (preparedStatement != null) {
                closeStatement(preparedStatement);
                closeConnection(proxyConnection);
            }
        }
    }




    default void closeStatement(Statement statement) {
        try {
            if (statement != null) {
                statement.close();
            }
        } catch (SQLException e) {
            LOGGER.error("Can't close statement");
        }
    }

    default void closeConnection(ProxyConnection connection) {
        ConnectionPool.getInstance().retrieveConnection(connection);
    }

    default ProxyConnection getConnection() {
        ConnectionPool connectionPool = ConnectionPool.getInstance();
        return connectionPool.takeConnection();
    }

}
