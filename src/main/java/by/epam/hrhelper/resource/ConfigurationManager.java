package by.epam.hrhelper.resource;

import java.util.ResourceBundle;

/**
 * class that contains paths to jsp pages
 */
public class ConfigurationManager {
    private final static ResourceBundle resourceBundle = ResourceBundle.getBundle("config.config");
    public static final String PAGE_HOME = resourceBundle.getString("path.page.home");
    public static final String PAGE_LOGIN = resourceBundle.getString("path.page.login");
    public static final String PAGE_ERROR = resourceBundle.getString("path.page.error");
    public static final String PAGE_MAIN = resourceBundle.getString("path.page.main");
    public static final String PAGE_REGISTRATION = resourceBundle.getString("path.page.register");
    public static final String PAGE_USERS=resourceBundle.getString("path.page.users");
    public static final String PAGE_VACANCIES = resourceBundle.getString("path.page.vacancies");
    public static final String PAGE_APPLY=resourceBundle.getString("path.page.apply");
    public static final String PAGE_ADD_JOB_INTERVIEW = resourceBundle.getString("path.page.addJobInterview");
    public static final String PAGE_ADD_VACANCY=resourceBundle.getString("path.page.addVacancy");
    public static final String PAGE_VIEW_RESUME=resourceBundle.getString("path.page.viewResume");
    public static final String PAGE_OPERATION_FAILED=resourceBundle.getString("path.page.operationFailed");
    public static final String PAGE_OPERATION_SUCCESSFUL=resourceBundle.getString("path.page.operationSuccessful");


    public static final String PAGE_VACANCY_APPLICATIONS=resourceBundle.getString("path.page.vacancyApplications");
    public static final String PAGE_APPLICATION_INTERVIEWS=resourceBundle.getString("path.page.applicationInterviews");
    public static final String PAGE_ERROR_403=resourceBundle.getString("path.page.error403");
    public static final String PAGE_CANDIDATE_PROFILE = resourceBundle.getString("path.page.candidate");

    private ConfigurationManager() {
    }


}
