package by.epam.hrhelper.resource;

import java.util.Locale;
import java.util.ResourceBundle;


/**
 * class that helps to pulls out Strings with messages in different languages from properties file
 */
public class MessageManager {
    private MessageManager() {
    }

    /**
     * @param key    - key in properties file
     * @param locale - locale for creating resourceBundle that contains required language
     * @return value from properties file
     */
    public static String getProperty(String key, Locale locale) {
        return ResourceBundle.getBundle("property.label", locale).getString(key);
    }
}
