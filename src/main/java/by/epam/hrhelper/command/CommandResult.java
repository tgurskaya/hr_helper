package by.epam.hrhelper.command;

/**
 * class that contains result of executing command. The result is page and name of method of referring to the page
 */
public class CommandResult {

    /**
     * contains 2 names of methods of referring to the page
     */
    public enum ResponseType {
        FORWARD,
        REDIRECT
    }

    private ResponseType responseType;
    private String page;

    public CommandResult(ResponseType responseType, String page) {
        this.responseType = responseType;
        this.page = page;
    }

    public void setPage(String page) {
        this.page = page;
    }

    public ResponseType getResponseType() {
        return responseType;
    }

    public String getPage() {
        return page;
    }
}
