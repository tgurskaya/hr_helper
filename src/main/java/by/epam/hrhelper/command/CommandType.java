package by.epam.hrhelper.command;

import by.epam.hrhelper.receiver.ApplicationReceiver;
import by.epam.hrhelper.receiver.JobInterviewReceiver;
import by.epam.hrhelper.receiver.UserReceiver;
import by.epam.hrhelper.receiver.VacancyReceiver;

public enum CommandType {

    /**
     * commands that are available for all users
     */
    LOGIN(UserReceiver.getInstance()::signIn),
    LOGOUT(UserReceiver.getInstance()::signOut),
    ALL_OPEN_VACANCIES(VacancyReceiver.getInstance()::showAllOpen),
    UPDATE_USER(UserReceiver.getInstance()::updateUser),
    DELETE_PROFILE(UserReceiver.getInstance()::deleteUser),
    CHANGE_PASSWORD(UserReceiver.getInstance()::updatePassword),
    HOME(UserReceiver.getInstance()::goHome),

    /**
     * commands that are available for candidate and hr
     */
    VIEW_RESUME(ApplicationReceiver.getInstance()::showResume),
    VIEW_JOB_INTERVIEWS(JobInterviewReceiver.getInstance()::showAll),

    /**
     * commands that are available for candidate
     */
    APPLY(ApplicationReceiver.getInstance()::apply),
    ALL_CANDIDATE_APPLICATIONS(ApplicationReceiver.getInstance()::showCandidateApplications),
    DELETE_APPLICATION(ApplicationReceiver.getInstance()::deleteApplication),
    CHANGE_COVERING_LETTER(ApplicationReceiver.getInstance()::changeCoveringLetter),

    /**
     * commands that are available for hr
     */
    REGISTER_CANDIDATE(UserReceiver.getInstance()::registerCandidate),
    DELETE_VACANCY(VacancyReceiver.getInstance()::deleteVacancy),
    ALL_HR_VACANCIES(VacancyReceiver.getInstance()::showHRVacancies),
    ADD_VACANCY(VacancyReceiver.getInstance()::addVacancy),
    NEW_VACANCY(VacancyReceiver.getInstance()::createVacancy),
    VIEW_VACANCY_APPLICATIONS(ApplicationReceiver.getInstance()::showVacancyApplications),
    ADD_JOB_INTERVIEW(JobInterviewReceiver.getInstance()::newJobInterview),
    NEW_JOB_INTERVIEW(JobInterviewReceiver.getInstance()::createJobInterview),
    VIEW_CANDIDATE(UserReceiver.getInstance()::viewUser),
    NEW_DECISION(ApplicationReceiver.getInstance()::adjudicate),
    CHANGE_VACANCY(VacancyReceiver.getInstance()::updateVacancy),
    CHANGE_JOB_INTERVIEW(JobInterviewReceiver.getInstance()::updateJobInterview),
    DELETE_JOB_INTERVIEW(JobInterviewReceiver.getInstance()::deleteJobInterview),
    CHANGE_OPINION(JobInterviewReceiver.getInstance()::changeOpinion),
    /**
     * commands that are available for admin
     */
    ALL_USERS(UserReceiver.getInstance()::showAll),
    BLOCK_USER(UserReceiver.getInstance()::blockUser),
    UNBLOCK_USER(UserReceiver.getInstance()::unblockUser),
    REGISTER_HR_MANAGER(UserReceiver.getInstance()::registerHrManager);


    private Command command;

    CommandType(Command command) {
        this.command = command;
    }

    public Command getCommand() {
        return command;
    }

}
