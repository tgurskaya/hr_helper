package by.epam.hrhelper.command;

import by.epam.hrhelper.controller.RequestContent;

/**
 *Class that helps to convert name of command into Command object
 */
public class CommandFactoryClient {

    private static final String COMMAND_PARAMETER ="command";

    /**
     * Function turns string with name of command into Command object
     * @param content contains parameter "command" from HttpRequest
     * @return object of class Command
     */
    public Command initCommand(RequestContent content){
        Command command;
        String commandName= String.valueOf(content.getParameter(COMMAND_PARAMETER));
        CommandType type=CommandType.valueOf(commandName.toUpperCase());
        command=type.getCommand();
        return command;
    }

}
