package by.epam.hrhelper.command;

import by.epam.hrhelper.controller.RequestContent;

@FunctionalInterface
public interface Command {
    /**
     * @param content - contains parameters and attributes from HttpServletRequest
     * @return CommandResult: page and type of response
     */
    CommandResult execute(RequestContent content);
}
