package by.epam.hrhelper.receiver;

import by.epam.hrhelper.command.CommandResult;
import by.epam.hrhelper.controller.RequestContent;
import by.epam.hrhelper.resource.ConfigurationManager;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.Locale;

public interface CommonReceiver {

    String LANGUAGE = "language";
    String EN_LANGUAGE = "en";
    String US_COUNTRY = "US";
    String ATTRIBUTE_MESSAGE = "message";
    String ATTRIBUTE_USER = "user";
    String ERROR_FAIL_DELETE = "error.failDelete";
    String PAGE_NUMBER = "pageNumber";
    String PAGE_COUNT = "pageCount";
    String APPLICATION_LIST = "applicationList";
    String VACANCY_ID = "vacancyId";
    String VACANCY_LIST = "vacancyList";
    String MESSAGE_WRONG_FORMAT = "label.wrongFormat";
    String LAST_COMMAND = "lastCommand";
    String NEXT_COMMAND = "nextCommand";
    String MESSAGE_CANT_UPDATE = "error.cantUpdate";
    String ERROR_CREATION_FAILED = "error.creationFailed";
    String AND_SYMBOL = "&";
    String EQUAL_SYMBOL = "=";
    String APPLICATION_ID = "applicationId";
    String USER_ID = "userId";


    Logger LOGGER = LogManager.getLogger(CommonReceiver.class);

    /**
     * @param requestContent - object that contains attribute of language
     * @return required Locale depended on parameter language from RequestContent
     */
    default Locale getLocale(RequestContent requestContent) {
        String language;
        String country;
        Locale locale;
        Object languageAttribute = requestContent.getSessionAttribute(LANGUAGE);
        if (languageAttribute != null) {
            language = languageAttribute.toString().substring(0, 2);
            if (languageAttribute.toString().length() > 4) {
                country = languageAttribute.toString().substring(3, 5);
                locale = new Locale(language, country);
            } else {
                locale = new Locale(language);
            }
        } else {
            locale = new Locale(EN_LANGUAGE, US_COUNTRY);
        }
        return locale;
    }

    /**
     * @param result         - result of inserting, updating or deleting from database
     * @param requestContent - object in which message will be inserted
     * @param failedMessage  - message that will be shown if result is false
     * @return CommandResult which depended on result of operation
     */
    default CommandResult resultOfOperation(boolean result, RequestContent requestContent, String failedMessage) {
        if (result) {
            return new CommandResult(CommandResult.ResponseType.REDIRECT, ConfigurationManager.PAGE_OPERATION_SUCCESSFUL);
        } else {
            requestContent.setSessionAttribute(ATTRIBUTE_MESSAGE, failedMessage);
            return new CommandResult(CommandResult.ResponseType.REDIRECT, ConfigurationManager.PAGE_OPERATION_FAILED);
        }
    }

    /**
     * @param e              - Exception caused in method
     * @param requestContent - RequestContent object in which message about exception will be inserted
     * @return CommandResult redirecting on page error
     */
    default CommandResult resultAfterException(Exception e, RequestContent requestContent) {
        LOGGER.catching(e);
        requestContent.setSessionAttribute(ATTRIBUTE_MESSAGE, e.getMessage());
        return new CommandResult(CommandResult.ResponseType.FORWARD, ConfigurationManager.PAGE_ERROR);
    }
}
