package by.epam.hrhelper.receiver;

import by.epam.hrhelper.command.CommandResult;
import by.epam.hrhelper.controller.RequestContent;
import by.epam.hrhelper.dao.VacancyDAO;
import by.epam.hrhelper.dao.impl.VacancyDAOImpl;
import by.epam.hrhelper.entity.User;
import by.epam.hrhelper.entity.Vacancy;
import by.epam.hrhelper.exception.DAOException;
import by.epam.hrhelper.resource.ConfigurationManager;
import by.epam.hrhelper.resource.MessageManager;
import by.epam.hrhelper.validation.VacancyValidator;

import java.util.List;

public class VacancyReceiver implements CommonReceiver {
    private static final String PARAM_STATUS = "status";
    private static final String COMMAND_ALL_HR_VACANCIES = "all_hr_vacancies";
    private static final String COMMAND_ALL_OPEN_VACANCIES = "ALL_OPEN_VACANCIES";
    private static final String COMMAND_ADD_VACANCY = "add_vacancy";
    private static final String PARAM_SALARY = "salary";
    private static final String PARAM_DESCRIPTION = "description";
    private static final String PARAM_REQUIREMENTS = "requirements";
    private static final String PARAM_RESPONSIBILITIES = "responsibilities";
    private static final String PARAM_NAME = "name";
    private static final String ERROR_ACCESS_DENIED = "error.accessDenied";
    private static final String OPEN_VACANCIES = "OPEN";
    private static VacancyReceiver vacancyReceiver;

    public static VacancyReceiver getInstance() {
        if (vacancyReceiver == null) {
            vacancyReceiver = new VacancyReceiver();
        }
        return vacancyReceiver;
    }

    private VacancyReceiver() {
    }

    /**
     * methods call for method show vacancies with parameter type Open
     * @return CommandResult @see CommandResult showVacancies(RequestContent content, String status)
     */
    public CommandResult showAllOpen(RequestContent content) {
        content.setSessionAttribute(LAST_COMMAND, COMMAND_ALL_OPEN_VACANCIES);
        return showVacancies(content, OPEN_VACANCIES);
    }


    /**
     * @param content contains number of page with vacancies
     * @param status  wanted vacancy status
     * @return Command result with page where user can see all vacancies that has required type
     */
    private CommandResult showVacancies(RequestContent content, String status) {
        int currentPage = 1;
        int recordsPerPage = 5;
        if (content.getParameter(PAGE_NUMBER) != null) {
            currentPage = Integer.parseInt(content.getParameter(PAGE_NUMBER));
        }
        VacancyDAO vacancyDAO = new VacancyDAOImpl();
        try {
            List<Vacancy> vacancyList = vacancyDAO.showVacancies((currentPage - 1) * recordsPerPage, recordsPerPage, status);
            int numOfPages = (int) Math.ceil((double) vacancyDAO.countAllVacancies(status) / recordsPerPage);
            content.setRequestAttribute(PAGE_NUMBER, currentPage);
            content.setRequestAttribute(PAGE_COUNT, numOfPages);
            content.setRequestAttribute(VACANCY_LIST, vacancyList);
            content.setSessionAttribute(LAST_COMMAND, content.getSessionAttribute(LAST_COMMAND) + AND_SYMBOL + PAGE_NUMBER + EQUAL_SYMBOL + currentPage + AND_SYMBOL + PAGE_COUNT + EQUAL_SYMBOL + numOfPages);
        } catch (DAOException e) {
            LOGGER.catching(e);
            content.setRequestAttribute(ATTRIBUTE_MESSAGE, e.getMessage());
            return new CommandResult(CommandResult.ResponseType.FORWARD, ConfigurationManager.PAGE_ERROR);
        }
        return new CommandResult(CommandResult.ResponseType.FORWARD, ConfigurationManager.PAGE_VACANCIES);
    }


    /**
     * @param content contains id of HR who wants to see all vacancies that he/she add before
     * @return CommandResult contains page with all required vacancies
     */
    public CommandResult showHRVacancies(RequestContent content) {
        int currentPage = 1;
        int recordsPerPage = 2;
        if (content.getParameter(PAGE_NUMBER) != null) {
            currentPage = Integer.parseInt(content.getParameter(PAGE_NUMBER));
        }
        VacancyDAO vacancyDAO = new VacancyDAOImpl();
        User hr = (User) content.getSessionAttribute(ATTRIBUTE_USER);
        try {
            List<Vacancy> vacancyList = vacancyDAO.showHrVacancies(hr, (currentPage - 1) * recordsPerPage, recordsPerPage);
            int numOfPages = (int) Math.ceil((double) vacancyDAO.countWithId(hr.getId()) / recordsPerPage);
            content.setRequestAttribute(PAGE_NUMBER, currentPage);
            content.setRequestAttribute(PAGE_COUNT, numOfPages);
            content.setRequestAttribute(VACANCY_LIST, vacancyList);
            content.setSessionAttribute(LAST_COMMAND, COMMAND_ALL_HR_VACANCIES + AND_SYMBOL + AND_SYMBOL + PAGE_NUMBER + EQUAL_SYMBOL + currentPage + AND_SYMBOL + PAGE_COUNT + EQUAL_SYMBOL + numOfPages);
        } catch (DAOException e) {
            LOGGER.catching(e);
            content.setRequestAttribute(ATTRIBUTE_MESSAGE, e.getMessage());
            return new CommandResult(CommandResult.ResponseType.FORWARD, ConfigurationManager.PAGE_ERROR);
        }
        return new CommandResult(CommandResult.ResponseType.FORWARD, ConfigurationManager.PAGE_HOME);
    }

    /**
     * @param requestContent contain id of vacancy that HR wants to delete
     * @return CommandResult with page that shows result of deleting and than forward to
     * page with other vacancies added bu HR before
     */
    public CommandResult deleteVacancy(RequestContent requestContent) {
        VacancyDAO vacancyDAO = new VacancyDAOImpl();
        int vacancyId = Integer.parseInt(requestContent.getParameter(VACANCY_ID));
        requestContent.setSessionAttribute(NEXT_COMMAND, COMMAND_ALL_HR_VACANCIES);
        return resultOfOperation(vacancyDAO.delete(vacancyId), requestContent, MessageManager.getProperty(ERROR_FAIL_DELETE, getLocale(requestContent)));
    }

    /**
     * @param requestContent contain user HR  who wants to add vacancy
     * @return Command result that contains page, where HR can enter information about vacancy
     */
    public CommandResult addVacancy(RequestContent requestContent) {
        User user = (User) requestContent.getSessionAttribute(ATTRIBUTE_USER);
        requestContent.setSessionAttribute(LAST_COMMAND, COMMAND_ADD_VACANCY);
        if (user.getUserType().equals(User.UserType.HR)) {
            return new CommandResult(CommandResult.ResponseType.FORWARD, ConfigurationManager.PAGE_ADD_VACANCY);
        } else {
            requestContent.setRequestAttribute(ATTRIBUTE_MESSAGE, MessageManager.getProperty(ERROR_ACCESS_DENIED, getLocale(requestContent)));
            return new CommandResult(CommandResult.ResponseType.FORWARD, ConfigurationManager.PAGE_ERROR);
        }
    }

    /**
     * @param requestContent contains identification number of HR user, who wants to add vacancy
     *                       and information about vacancy
     * @return CommandResult with page that shows if the inserting in database information
     * about vacancy was successful and than forwards to home page
     */
    public CommandResult createVacancy(RequestContent requestContent) {
        Vacancy vacancy = new Vacancy();
        String name = requestContent.getParameter(PARAM_NAME);
        String salary = requestContent.getParameter(PARAM_SALARY);
        String description = requestContent.getParameter(PARAM_DESCRIPTION);
        String requirements = requestContent.getParameter(PARAM_REQUIREMENTS);
        String responsibilities = requestContent.getParameter(PARAM_RESPONSIBILITIES);
        VacancyValidator vacancyValidator = new VacancyValidator();
        if (!vacancyValidator.checkVacancyData(name, salary, description, requirements, responsibilities)) {
            requestContent.setSessionAttribute(ATTRIBUTE_MESSAGE, MessageManager.getProperty(MESSAGE_WRONG_FORMAT, getLocale(requestContent)));
            return new CommandResult(CommandResult.ResponseType.REDIRECT, ConfigurationManager.PAGE_OPERATION_FAILED);
        }
        int hrId = ((User) requestContent.getSessionAttribute(ATTRIBUTE_USER)).getId();
        vacancy.setHrId(hrId);
        vacancy.setSalary(salary);
        vacancy.setStatus(Vacancy.VacancyStatus.OPEN);
        vacancy.setDescription(description);
        vacancy.setName(name);
        vacancy.setResponsibilities(responsibilities);
        vacancy.setRequirements(requirements);
        VacancyDAO vacancyDAO = new VacancyDAOImpl();
        requestContent.setSessionAttribute(NEXT_COMMAND, COMMAND_ALL_HR_VACANCIES);
        try {
            return resultOfOperation(vacancyDAO.create(vacancy), requestContent, MessageManager.getProperty(ERROR_CREATION_FAILED, getLocale(requestContent)));
        } catch (DAOException e) {
            return resultAfterException(e, requestContent);
        }
    }

    /**
     * @param requestContent contains parameters of vacancy that HR wants to change
     * @return CommandResult with page that shows result of updating and than forward to
     * page with other vacancies inserted by HR before
     */
    public CommandResult updateVacancy(RequestContent requestContent) {
        Vacancy vacancy = new Vacancy();
        VacancyValidator vacancyValidator = new VacancyValidator();
        vacancy.setRequirements(requestContent.getParameter(PARAM_REQUIREMENTS));
        vacancy.setResponsibilities(requestContent.getParameter(PARAM_RESPONSIBILITIES));
        vacancy.setName(requestContent.getParameter(PARAM_NAME));
        vacancy.setDescription(requestContent.getParameter(PARAM_DESCRIPTION));
        vacancy.setSalary(requestContent.getParameter(PARAM_SALARY));
        vacancy.setId(Integer.parseInt(requestContent.getParameter(VACANCY_ID)));
        String status = requestContent.getParameter(PARAM_STATUS);
        requestContent.setSessionAttribute(NEXT_COMMAND, COMMAND_ALL_HR_VACANCIES);
        if (vacancyValidator.checkVacancy(vacancy) && vacancyValidator.checkStatus(status)) {
            vacancy.setStatus(Vacancy.VacancyStatus.valueOf(status));
        } else {
            requestContent.setSessionAttribute(ATTRIBUTE_MESSAGE, MessageManager.getProperty(MESSAGE_WRONG_FORMAT, getLocale(requestContent)));
            return new CommandResult(CommandResult.ResponseType.REDIRECT, ConfigurationManager.PAGE_OPERATION_FAILED);
        }
        VacancyDAO vacancyDAO = new VacancyDAOImpl();
        try {
            return resultOfOperation(vacancyDAO.update(vacancy), requestContent, MessageManager.getProperty(MESSAGE_CANT_UPDATE, getLocale(requestContent)));
        } catch (DAOException e) {
            LOGGER.catching(e);
            requestContent.setSessionAttribute(ATTRIBUTE_MESSAGE, e.getMessage());
            return new CommandResult(CommandResult.ResponseType.REDIRECT, ConfigurationManager.PAGE_ERROR);
        }

    }
}
