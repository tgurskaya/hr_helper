package by.epam.hrhelper.receiver;

import by.epam.hrhelper.command.CommandResult;
import by.epam.hrhelper.controller.RequestContent;
import by.epam.hrhelper.dao.UserDAO;
import by.epam.hrhelper.dao.impl.UserDaoImpl;
import by.epam.hrhelper.encryption.PasswordEncryption;
import by.epam.hrhelper.entity.User;
import by.epam.hrhelper.exception.DAOException;
import by.epam.hrhelper.resource.ConfigurationManager;
import by.epam.hrhelper.resource.MessageManager;
import by.epam.hrhelper.validation.UserValidator;

import java.util.List;
import java.util.Locale;

public class UserReceiver implements CommonReceiver {

    private static final String MESSAGE_PASSWORDS_DO_NOT_MATCH = "message.passwordsDoNotMatch";
    private static final String MESSAGE_WRONG_DATA_FOR_REGISTRATION = "message.wrongDataForRegistration";
    private static final String MESSAGE_WRONG_EMAIL = "message.wrongEmail";
    private static final String MESSAGE_REGISTRATION_SUCCESSFUL = "label.registrationSuccessful";
    private static final String MESSAGE_OLD_PASSWORD_WRONG = "message.oldPasswordWrong";
    private static final String USER_LIST = "userList";
    private static final String CANDIDATE_ID = "candidateId";
    private static final String COMMAND_HOME = "home";
    private static final String COMMAND_ALL_USERS = "all_users";
    private static final String ATTRIBUTE_CANDIDATE = "candidate";
    private static final String COMMAND_VIEW_CANDIDATE = "view_candidate";
    private static UserReceiver userReceiver;

    private static final String PARAM_OLD_PASSWORD = "oldPassword";
    private static final String PARAM_LOGIN = "login";
    private static final String PARAM_PASSWORD = "password";
    private static final String PARAM_NAME = "name";
    private static final String PARAM_EMAIL = "email";
    private static final String PARAM_PHONE = "phone";
    private static final String PARAM_BIRTHDAY = "birthday";
    private static final String PARAM_REPEAT_PASSWORD = "repeatPassword";
    private static final String MESSAGE_WRONG_PASSWORD_OR_LOGIN = "label.wrongPasswordOrLogin";


    public static UserReceiver getInstance() {
        if (userReceiver == null) {
            userReceiver = new UserReceiver();
        }
        return userReceiver;
    }

    private UserReceiver() {
    }


    /**
     * @param content that contain email and password that user entered to authorized in application
     * @return CommandResult that contains user's home page when authorization is successful and login page when it's not
     */
    public CommandResult signIn(RequestContent content) {
        Locale locale = getLocale(content);
        String login = content.getParameter(PARAM_LOGIN);
        String password = content.getParameter(PARAM_PASSWORD);
        UserValidator userValidator = new UserValidator();
        if (!userValidator.checkLoginAndPassword(login, password)) {
            content.setRequestAttribute(ATTRIBUTE_MESSAGE, MessageManager.getProperty(MESSAGE_WRONG_FORMAT, locale));
            return new CommandResult(CommandResult.ResponseType.FORWARD, ConfigurationManager.PAGE_LOGIN);
        }
        UserDaoImpl userDao = new UserDaoImpl();
        User user = userDao.findByEmail(login);
        if (user == null) {
            content.setRequestAttribute(ATTRIBUTE_MESSAGE, MessageManager.getProperty(MESSAGE_WRONG_PASSWORD_OR_LOGIN, locale));
            return new CommandResult(CommandResult.ResponseType.FORWARD, ConfigurationManager.PAGE_LOGIN);
        }
        if (user.getPassword().equals(PasswordEncryption.encrypt(password))) {
            content.setSessionAttribute(ATTRIBUTE_USER, user);

            if (user.getUserType().equals(User.UserType.CANDIDATE)) {
                return ApplicationReceiver.getInstance().showCandidateApplications(content);
            } else if (user.getUserType().equals(User.UserType.HR)) {
                return VacancyReceiver.getInstance().showHRVacancies(content);
            } else {
                return new CommandResult(CommandResult.ResponseType.FORWARD, ConfigurationManager.PAGE_HOME);
            }
        } else {
            content.setRequestAttribute(ATTRIBUTE_MESSAGE, MessageManager.getProperty(MESSAGE_WRONG_PASSWORD_OR_LOGIN, locale));
            return new CommandResult(CommandResult.ResponseType.FORWARD, ConfigurationManager.PAGE_LOGIN);
        }
    }

    /**
     * method calls method register for user with type candidate
     */
    public CommandResult registerCandidate(RequestContent requestContent) {
        return register(requestContent, User.UserType.CANDIDATE);
    }

    /**
     * method calls method register for user with type HR
     */
    public CommandResult registerHrManager(RequestContent requestContent) {
        return register(requestContent, User.UserType.HR);
    }

    /**
     * @param requestContent contains information about user for registration
     * @param userType       type of user who wants to register in application
     * @return CommandResult with page that shows if the registration was successful
     */
    public CommandResult register(RequestContent requestContent, User.UserType userType) {
        User user = new User();
        String name = requestContent.getParameter(PARAM_NAME);
        String birthday = requestContent.getParameter(PARAM_BIRTHDAY);
        String email = requestContent.getParameter(PARAM_EMAIL);
        String phone = requestContent.getParameter(PARAM_PHONE);
        String password = requestContent.getParameter(PARAM_PASSWORD);
        String repeatPassword = requestContent.getParameter(PARAM_REPEAT_PASSWORD);
        UserValidator userValidator = new UserValidator();
        if (!userValidator.checkPasswords(password, repeatPassword)) {
            requestContent.setRequestAttribute(ATTRIBUTE_MESSAGE, MessageManager.getProperty(MESSAGE_PASSWORDS_DO_NOT_MATCH, getLocale(requestContent)));
            return new CommandResult(CommandResult.ResponseType.FORWARD, ConfigurationManager.PAGE_REGISTRATION);
        }
        if (!userValidator.checkDataForRegistration(name, birthday, email, phone, password)) {
            requestContent.setRequestAttribute(ATTRIBUTE_MESSAGE, MessageManager.getProperty(MESSAGE_WRONG_DATA_FOR_REGISTRATION, getLocale(requestContent)));
            return new CommandResult(CommandResult.ResponseType.FORWARD, ConfigurationManager.PAGE_REGISTRATION);
        }
        user.setName(name);
        user.setBirthday(birthday);
        user.setPassword(PasswordEncryption.encrypt(password));
        user.setEmail(email);
        user.setPhone(phone);
        user.setUserType(userType);
        UserDaoImpl userDao = new UserDaoImpl();
        try {
            if (userDao.create(user)) {
                if (userType.equals(User.UserType.ADMIN)) {
                    requestContent.setSessionAttribute(NEXT_COMMAND, COMMAND_HOME);
                    return new CommandResult(CommandResult.ResponseType.REDIRECT, ConfigurationManager.PAGE_OPERATION_SUCCESSFUL);
                } else {
                    requestContent.setRequestAttribute(ATTRIBUTE_MESSAGE, MessageManager.getProperty(MESSAGE_REGISTRATION_SUCCESSFUL, getLocale(requestContent)));
                    return new CommandResult(CommandResult.ResponseType.FORWARD, ConfigurationManager.PAGE_LOGIN);
                }
            } else {
                requestContent.setSessionAttribute(ATTRIBUTE_MESSAGE, MessageManager.getProperty(ERROR_CREATION_FAILED, getLocale(requestContent)));
                return new CommandResult(CommandResult.ResponseType.REDIRECT, ConfigurationManager.PAGE_OPERATION_FAILED);
            }
        } catch (DAOException e) {
            LOGGER.catching(e);
            if (e.getErrorCode() == 1062) {
                requestContent.setRequestAttribute(ATTRIBUTE_MESSAGE, MessageManager.getProperty(MESSAGE_WRONG_EMAIL, getLocale(requestContent)));
                return new CommandResult(CommandResult.ResponseType.FORWARD, ConfigurationManager.PAGE_REGISTRATION);
            }
            return new CommandResult(CommandResult.ResponseType.REDIRECT, ConfigurationManager.PAGE_ERROR);
        }
    }

    /**
     * @param requestContent contains user id who wants to exit from his account
     * @return CommandResult with main page
     */
    public CommandResult signOut(RequestContent requestContent) {
        requestContent.invalidateSession();
        return new CommandResult(CommandResult.ResponseType.REDIRECT, ConfigurationManager.PAGE_MAIN);
    }

    /**
     * @param requestContent contains new information about user who wants to
     *                       replace information inserted while registration
     * @return CommandResult that contains page that shows if the updating was successful
     */
    public CommandResult updateUser(RequestContent requestContent) {
        User user = (User) requestContent.getSessionAttribute(ATTRIBUTE_USER);
        User newUser = new User();
        newUser.setUserType(user.getUserType());
        newUser.setId(user.getId());
        newUser.setBlocked(user.isBlocked());
        newUser.setName(requestContent.getParameter(PARAM_NAME));
        newUser.setBirthday(requestContent.getParameter(PARAM_BIRTHDAY));
        newUser.setEmail(requestContent.getParameter(PARAM_EMAIL));
        newUser.setPhone(requestContent.getParameter(PARAM_PHONE));
        requestContent.setSessionAttribute(NEXT_COMMAND, COMMAND_HOME);
        UserDaoImpl userDao = new UserDaoImpl();
        try {
            if (userDao.update(newUser)) {
                user = newUser;
            } else {
                requestContent.setSessionAttribute(ATTRIBUTE_MESSAGE, MessageManager.getProperty(ERROR_CREATION_FAILED, getLocale(requestContent)));
                return new CommandResult(CommandResult.ResponseType.REDIRECT, ConfigurationManager.PAGE_OPERATION_FAILED);
            }
        } catch (DAOException e) {
            if (e.getErrorCode() == 1062) {
                requestContent.setSessionAttribute(ATTRIBUTE_MESSAGE, MessageManager.getProperty(MESSAGE_WRONG_EMAIL, getLocale(requestContent)));
;                return new CommandResult(CommandResult.ResponseType.REDIRECT, ConfigurationManager.PAGE_OPERATION_FAILED);
            }
            LOGGER.catching(e);
            return new CommandResult(CommandResult.ResponseType.REDIRECT, ConfigurationManager.PAGE_ERROR);
        }
        requestContent.setSessionAttribute(ATTRIBUTE_USER, user);
        return new CommandResult(CommandResult.ResponseType.REDIRECT, ConfigurationManager.PAGE_OPERATION_SUCCESSFUL);
    }

    /**
     * @param requestContent old password and two times entered new password
     *                       old password will be checked if it's equal to entered in database
     *                       new password will be comparing between each other and checked with pattern
     * @return CommandResult that contains page that shows if the changing password was successful
     */
    public CommandResult updatePassword(RequestContent requestContent) {
        User user = (User) requestContent.getSessionAttribute(ATTRIBUTE_USER);
        String oldPassword = PasswordEncryption.encrypt(requestContent.getParameter(PARAM_OLD_PASSWORD));
        String password = requestContent.getParameter(PARAM_PASSWORD);
        String repeatPassword = requestContent.getParameter(PARAM_REPEAT_PASSWORD);
        UserValidator userValidator = new UserValidator();
        requestContent.setSessionAttribute(NEXT_COMMAND, COMMAND_HOME);
        if (!userValidator.checkPasswords(password, repeatPassword)) {
            requestContent.setSessionAttribute(ATTRIBUTE_MESSAGE, MessageManager.getProperty(MESSAGE_PASSWORDS_DO_NOT_MATCH, getLocale(requestContent)));
            return new CommandResult(CommandResult.ResponseType.REDIRECT, ConfigurationManager.PAGE_OPERATION_FAILED);
        }
        if (!userValidator.checkPasswordWithPattern(password)) {
            requestContent.setSessionAttribute(ATTRIBUTE_MESSAGE, MessageManager.getProperty(MESSAGE_WRONG_FORMAT, getLocale(requestContent)));
            return new CommandResult(CommandResult.ResponseType.REDIRECT, ConfigurationManager.PAGE_OPERATION_FAILED);
        }
        UserDAO userDAO = new UserDaoImpl();
        try {
            String oldValidPassword = userDAO.findEntityById(user.getId()).getPassword();
            if (oldPassword.equals(oldValidPassword)) {
                return resultOfOperation(userDAO.changePassword(user.getId(), PasswordEncryption.encrypt(password)), requestContent, MessageManager.getProperty(MESSAGE_CANT_UPDATE, getLocale(requestContent)));
            } else {
                requestContent.setSessionAttribute(ATTRIBUTE_MESSAGE, MessageManager.getProperty(MESSAGE_OLD_PASSWORD_WRONG, getLocale(requestContent)));
                return new CommandResult(CommandResult.ResponseType.REDIRECT, ConfigurationManager.PAGE_OPERATION_FAILED);
            }
        } catch (DAOException e) {
            LOGGER.catching(e);
            requestContent.setSessionAttribute(ATTRIBUTE_MESSAGE, e.getMessage());
            return new CommandResult(CommandResult.ResponseType.REDIRECT, ConfigurationManager.PAGE_ERROR);
        }
    }

    /**
     * @param requestContent contains identification number of user who want's to delete his/her account
     * @return CommandResult that redirects to page error if deleting wasn't successful and main page if it was
     */
    public CommandResult deleteUser(RequestContent requestContent) {
        UserDAO userDao = new UserDaoImpl();
        User user = (User) requestContent.getSessionAttribute(ATTRIBUTE_USER);
        if (userDao.delete(user.getId())) {
            return signOut(requestContent);
        } else {
            requestContent.setSessionAttribute(ATTRIBUTE_MESSAGE, MessageManager.getProperty(ERROR_FAIL_DELETE, getLocale(requestContent)));
            return new CommandResult(CommandResult.ResponseType.REDIRECT, ConfigurationManager.PAGE_OPERATION_FAILED);
        }
    }

    /**
     * @param requestContent that contains number of page
     * @return CommandResult with page where admin can see all users who register in application
     */
    public CommandResult showAll(RequestContent requestContent) {
        int currentPage = 1;
        int recordsPerPage = 6;
        if (requestContent.getParameter(PAGE_NUMBER) != null) {
            currentPage = Integer.parseInt(requestContent.getParameter(PAGE_NUMBER));
        }
        UserDAO userDAO = new UserDaoImpl();
        try {
            List<User> userList = userDAO.showUsers((currentPage - 1) * recordsPerPage, recordsPerPage);
            int numOfPages = (int) Math.ceil((double) userDAO.countAllUsers() / recordsPerPage);
            requestContent.setRequestAttribute(PAGE_NUMBER, currentPage);
            requestContent.setRequestAttribute(PAGE_COUNT, numOfPages);
            requestContent.setRequestAttribute(USER_LIST, userList);
            requestContent.setSessionAttribute(LAST_COMMAND, COMMAND_ALL_USERS);
        } catch (DAOException e) {
            LOGGER.catching(e);
            requestContent.setSessionAttribute(ATTRIBUTE_MESSAGE, e.getMessage());
            return new CommandResult(CommandResult.ResponseType.REDIRECT, ConfigurationManager.PAGE_ERROR);
        }
        return new CommandResult(CommandResult.ResponseType.FORWARD, ConfigurationManager.PAGE_USERS);

    }

    /**
     * @param content contains user who authorized in application
     * @return CommandResult with home page which depends on type of user
     */
    public CommandResult goHome(RequestContent content) {
        User user = (User) content.getSessionAttribute(ATTRIBUTE_USER);
        content.setSessionAttribute(LAST_COMMAND, COMMAND_HOME);
        if (user != null && user.getUserType() != null) {
            switch (user.getUserType()) {
                case CANDIDATE: {
                    return ApplicationReceiver.getInstance().showCandidateApplications(content);
                }
                case HR: {
                    return VacancyReceiver.getInstance().showHRVacancies(content);
                }
                case ADMIN: {
                    return new CommandResult(CommandResult.ResponseType.FORWARD, ConfigurationManager.PAGE_HOME);
                }
                default: {
                    return new CommandResult(CommandResult.ResponseType.REDIRECT, ConfigurationManager.PAGE_MAIN);
                }
            }
        } else {
            return new CommandResult(CommandResult.ResponseType.REDIRECT, ConfigurationManager.PAGE_MAIN);
        }
    }

    public CommandResult viewUser(RequestContent requestContent) {
        int candidateId = Integer.parseInt(requestContent.getParameter(CANDIDATE_ID));
        UserDAO userDAO = new UserDaoImpl();
        try {
            User candidate = userDAO.findEntityById(candidateId);
            requestContent.setRequestAttribute(ATTRIBUTE_CANDIDATE, candidate);
            requestContent.setSessionAttribute(LAST_COMMAND, COMMAND_VIEW_CANDIDATE);
            return new CommandResult(CommandResult.ResponseType.FORWARD, ConfigurationManager.PAGE_CANDIDATE_PROFILE);
        } catch (DAOException e) {
            return new CommandResult(CommandResult.ResponseType.FORWARD, ConfigurationManager.PAGE_ERROR);
        }
    }

    public CommandResult unblockUser(RequestContent requestContent) {
        UserDAO userDAO = new UserDaoImpl();
        int userId = Integer.parseInt(requestContent.getParameter(USER_ID));
        try {
            if (userDAO.unblockUser(userId)) {
                requestContent.setSessionAttribute(NEXT_COMMAND, COMMAND_ALL_USERS);
                return new CommandResult(CommandResult.ResponseType.REDIRECT, ConfigurationManager.PAGE_OPERATION_SUCCESSFUL);
            } else {
                return new CommandResult(CommandResult.ResponseType.REDIRECT, ConfigurationManager.PAGE_OPERATION_FAILED);
            }
        } catch (DAOException e) {
            LOGGER.catching(e);
            requestContent.setSessionAttribute(ATTRIBUTE_MESSAGE, e.getMessage());
            return new CommandResult(CommandResult.ResponseType.REDIRECT, ConfigurationManager.PAGE_ERROR);
        }

    }

    public CommandResult blockUser(RequestContent requestContent) {
        UserDAO userDAO = new UserDaoImpl();
        int userId = Integer.parseInt(requestContent.getParameter(USER_ID));
        try {
            if (userDAO.blockUser(userId)) {
                requestContent.setSessionAttribute(NEXT_COMMAND, COMMAND_ALL_USERS);
                return new CommandResult(CommandResult.ResponseType.REDIRECT, ConfigurationManager.PAGE_OPERATION_SUCCESSFUL);
            } else {
                return new CommandResult(CommandResult.ResponseType.REDIRECT, ConfigurationManager.PAGE_OPERATION_FAILED);
            }
        } catch (DAOException e) {
            LOGGER.catching(e);
            requestContent.setSessionAttribute(ATTRIBUTE_MESSAGE, e.getMessage());
            return new CommandResult(CommandResult.ResponseType.REDIRECT, ConfigurationManager.PAGE_ERROR);
        }

    }
}
