package by.epam.hrhelper.receiver;

import by.epam.hrhelper.command.CommandResult;
import by.epam.hrhelper.controller.RequestContent;
import by.epam.hrhelper.dao.ApplicationDAO;
import by.epam.hrhelper.dao.VacancyDAO;
import by.epam.hrhelper.dao.impl.ApplicationDAOImpl;
import by.epam.hrhelper.dao.impl.VacancyDAOImpl;
import by.epam.hrhelper.entity.Application;
import by.epam.hrhelper.entity.User;
import by.epam.hrhelper.entity.Vacancy;
import by.epam.hrhelper.exception.DAOException;
import by.epam.hrhelper.resource.ConfigurationManager;
import by.epam.hrhelper.resource.MessageManager;
import by.epam.hrhelper.validation.ApplicationValidator;

import java.util.List;

public class ApplicationReceiver implements CommonReceiver {

    private static ApplicationReceiver applicationReceiver;

    private static final String APPLICATION_ID = "applicationId";
    private static final String APPLICATION_IS_EXISTS = "error.applicationIsExists";
    private static final String DECISION = "decision";
    private static final String COVERING_LETTER = "coveringLetter";
    private static final String COMMAND_ALL_CANDIDATE_APPLICATIONS = "all_candidate_applications";
    private static final String COMMAND_APPLY_WITH_PARAMETER = "apply&vacancyId=";
    private static final String ATTRIBUTE_VACANCY = "vacancy";
    private static final String COMMAND_VIEW_VACANCY_APPLICATIONS = "view_vacancy_applications";
    /**
     * quantity of records per page
     */
    private static final int RECORDS_PER_PAGE = 5;
    private static int currentPage = 1;

    public static ApplicationReceiver getInstance() {
        if (applicationReceiver == null) {
            applicationReceiver = new ApplicationReceiver();
        }
        return applicationReceiver;
    }

    private ApplicationReceiver() {
    }

    /**
     * method that checked if the application on this vacancy from this user is exists and
     * returns command result with page error if its exists and page for creating application if it's not
     */
    public CommandResult apply(RequestContent requestContent) {
        User candidate = (User) requestContent.getSessionAttribute(ATTRIBUTE_USER);
        int vacancyId = Integer.parseInt(requestContent.getParameter(VACANCY_ID));
        ApplicationValidator applicationValidator = new ApplicationValidator();
        if (!applicationValidator.isApplicationExists(candidate.getId(), vacancyId)) {
            requestContent.setRequestAttribute(VACANCY_ID, vacancyId);
            requestContent.setSessionAttribute(LAST_COMMAND, COMMAND_APPLY_WITH_PARAMETER + vacancyId);
            return new CommandResult(CommandResult.ResponseType.FORWARD, ConfigurationManager.PAGE_APPLY);
        } else {
            requestContent.setRequestAttribute(ATTRIBUTE_MESSAGE, MessageManager.getProperty(APPLICATION_IS_EXISTS, getLocale(requestContent)));
            return new CommandResult(CommandResult.ResponseType.FORWARD, ConfigurationManager.PAGE_ERROR);
        }
    }


    /**
     * methods returns CommandResult with home page and inserts applications that user submitted before
     */
    public CommandResult showCandidateApplications(RequestContent requestContent) {
        User user = (User) requestContent.getSessionAttribute(ATTRIBUTE_USER);
        if (requestContent.getParameter(PAGE_NUMBER) != null) {
            currentPage = Integer.parseInt(requestContent.getParameter(PAGE_NUMBER));
        }
        ApplicationDAO applicationDAO = new ApplicationDAOImpl();
        List<Application> applicationList = applicationDAO.showCandidateApplication(user, (currentPage - 1) * RECORDS_PER_PAGE, RECORDS_PER_PAGE);
        int numOfPages = (int) Math.ceil((double) applicationDAO.countWithId(user.getId()) / RECORDS_PER_PAGE);
        requestContent.setRequestAttribute(PAGE_NUMBER, currentPage);
        requestContent.setRequestAttribute(PAGE_COUNT, numOfPages);
        requestContent.setRequestAttribute(APPLICATION_LIST, applicationList);
        requestContent.setSessionAttribute(LAST_COMMAND, COMMAND_ALL_CANDIDATE_APPLICATIONS + AND_SYMBOL + PAGE_NUMBER + EQUAL_SYMBOL + currentPage + AND_SYMBOL + PAGE_COUNT + EQUAL_SYMBOL + numOfPages);
        return new CommandResult(CommandResult.ResponseType.FORWARD, ConfigurationManager.PAGE_HOME);
    }

    /**
     * calls method delete from dao layer and
     * returns CommandResult with page that shows if the operation was successful
     */
    public CommandResult deleteApplication(RequestContent requestContent) {
        ApplicationDAO applicationDAO = new ApplicationDAOImpl();
        int applicationId = Integer.parseInt(requestContent.getParameter(APPLICATION_ID));
        requestContent.setSessionAttribute(NEXT_COMMAND, COMMAND_ALL_CANDIDATE_APPLICATIONS);
        return resultOfOperation(applicationDAO.delete(applicationId), requestContent, MessageManager.getProperty(ERROR_FAIL_DELETE, getLocale(requestContent)));
    }

    /**
     * @param requestContent contain applicationId to get resume of this application
     * @return CommandResult with page that shows resume
     */
    public CommandResult showResume(RequestContent requestContent) {
        if (requestContent.getParameter(APPLICATION_ID) != null) {
            requestContent.setRequestAttribute(APPLICATION_ID, requestContent.getParameter(APPLICATION_ID));
            return new CommandResult(CommandResult.ResponseType.FORWARD, ConfigurationManager.PAGE_VIEW_RESUME);
        } else {
            return new CommandResult(CommandResult.ResponseType.FORWARD, ConfigurationManager.PAGE_ERROR);
        }
    }

    /**
     * @param requestContent contains required current page,vacancyId
     * @return CommandResult that contains page in which are shown applications submitted for this vacancy
     */
    public CommandResult showVacancyApplications(RequestContent requestContent) {

        if (requestContent.getParameter(PAGE_NUMBER) != null) {
            currentPage = Integer.parseInt(requestContent.getParameter(PAGE_NUMBER));
        }
        int vacancyId = Integer.parseInt(requestContent.getParameter(VACANCY_ID));
        ApplicationDAO applicationDAO = new ApplicationDAOImpl();
        List<Application> applicationList = applicationDAO.showVacancyApplications(vacancyId, (currentPage - 1) * RECORDS_PER_PAGE, RECORDS_PER_PAGE);
        int numOfPages = (int) Math.ceil((double) applicationDAO.countApplicationWithVacancy(vacancyId) / RECORDS_PER_PAGE);
        VacancyDAO vacancyDAO = new VacancyDAOImpl();
        try {
            Vacancy vacancy = vacancyDAO.findEntityById(vacancyId);
            requestContent.setRequestAttribute(PAGE_NUMBER, currentPage);
            requestContent.setRequestAttribute(PAGE_COUNT, numOfPages);
            requestContent.setRequestAttribute(ATTRIBUTE_VACANCY, vacancy);
            requestContent.setRequestAttribute(APPLICATION_LIST, applicationList);
            requestContent.setSessionAttribute(LAST_COMMAND, COMMAND_VIEW_VACANCY_APPLICATIONS
                    + AND_SYMBOL + PAGE_NUMBER + EQUAL_SYMBOL + currentPage +
                    AND_SYMBOL + PAGE_COUNT + EQUAL_SYMBOL + numOfPages +
                    AND_SYMBOL + VACANCY_ID + EQUAL_SYMBOL + vacancyId);
            return new CommandResult(CommandResult.ResponseType.FORWARD, ConfigurationManager.PAGE_VACANCY_APPLICATIONS);
        } catch (DAOException e) {
            return resultAfterException(e, requestContent);
        }
    }

    /**
     * methods helps to insert decision into table application
     * @param requestContent that contains applicationId,vacancyId and decision on vacancy
     * @return CommandResult that contains page with result of inserting decision about candidate
     */
    public CommandResult adjudicate(RequestContent requestContent) {
        String decision = requestContent.getParameter(DECISION);
        int applicationId = Integer.parseInt(requestContent.getParameter(APPLICATION_ID));
        int vacancyId = Integer.parseInt(requestContent.getParameter(VACANCY_ID));
        ApplicationDAO applicationDAO = new ApplicationDAOImpl();
        requestContent.setSessionAttribute(NEXT_COMMAND, COMMAND_VIEW_VACANCY_APPLICATIONS + AND_SYMBOL + VACANCY_ID + EQUAL_SYMBOL + vacancyId);
        return resultOfOperation(applicationDAO.addDecision(applicationId, decision), requestContent, MessageManager.getProperty(ERROR_CREATION_FAILED, getLocale(requestContent)));
    }

    /**
     * @param requestContent contains covering letter that user wants to replace
     * @return CommandResult that contains page with result of changing covering letter
     * @return CommandResult that contains page with result of changing covering letter
     */
    public CommandResult changeCoveringLetter(RequestContent requestContent) {
        String coveringLetter = requestContent.getParameter(COVERING_LETTER);
        int applicationId = Integer.parseInt(requestContent.getParameter(APPLICATION_ID));
        ApplicationDAO applicationDAO = new ApplicationDAOImpl();
        requestContent.setSessionAttribute(NEXT_COMMAND, COMMAND_ALL_CANDIDATE_APPLICATIONS);
        return resultOfOperation(applicationDAO.updateCoveringLetter(coveringLetter, applicationId), requestContent, MessageManager.getProperty(MESSAGE_CANT_UPDATE, getLocale(requestContent)));
    }
}
