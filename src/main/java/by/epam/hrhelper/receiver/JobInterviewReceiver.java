package by.epam.hrhelper.receiver;

import by.epam.hrhelper.command.CommandResult;
import by.epam.hrhelper.controller.RequestContent;
import by.epam.hrhelper.dao.JobInterviewDAO;
import by.epam.hrhelper.dao.impl.JobInterviewDAOImpl;
import by.epam.hrhelper.entity.Application;
import by.epam.hrhelper.entity.JobInterview;
import by.epam.hrhelper.entity.User;
import by.epam.hrhelper.exception.DAOException;
import by.epam.hrhelper.resource.ConfigurationManager;
import by.epam.hrhelper.resource.MessageManager;
import by.epam.hrhelper.validation.JobInterviewValidator;

import java.time.format.DateTimeFormatter;
import java.util.ArrayList;

public class JobInterviewReceiver implements CommonReceiver {

    private static final Object COMMAND_VIEW_JOB_INTERVIEWS = "view_job_interviews";
    private static final String COMMAND_ADD_JOB_INTERVIEW = "add_job_interview";
    private static final String JOB_INTERVIEW_ID = "jobInterviewId";
    private static final String OPINION = "opinion";
    private static final String DATETIME = "dateTime";
    private static final String PLACE = "place";
    private static final String JOB_INTERVIEW_TYPE = "jobInterviewType";
    private static final String JOB_INTERVIEW_LIST = "jobInterviewList";
    private static JobInterviewReceiver jobInterviewReceiver;

    public static JobInterviewReceiver getInstance() {
        if (jobInterviewReceiver == null) {
            jobInterviewReceiver = new JobInterviewReceiver();
        }
        return jobInterviewReceiver;
    }

    private JobInterviewReceiver() {
    }

    /**
     * @param content contains parameter identification number of application that candidate sends to HR
     * @return Command result with page where user can see all job interviews
     * scheduled to candidate on position
     */
    public CommandResult showAll(RequestContent content) {
        User user = (User) content.getSessionAttribute(ATTRIBUTE_USER);
        int applicationId = Integer.parseInt(content.getParameter(APPLICATION_ID));
        content.setRequestAttribute(APPLICATION_ID, applicationId);
        JobInterviewDAO jobInterviewDAO = new JobInterviewDAOImpl();
        try {
            ArrayList<JobInterview> jobInterviewList = jobInterviewDAO.showApplicationInterviews(applicationId, 0, 5);
            int numOfPages = (int) Math.ceil((double) jobInterviewDAO.countWithId(applicationId) / 5);
            content.setRequestAttribute(PAGE_COUNT, numOfPages);
            content.setRequestAttribute(JOB_INTERVIEW_LIST, jobInterviewList);
            content.setSessionAttribute(LAST_COMMAND, COMMAND_VIEW_JOB_INTERVIEWS
                    + AND_SYMBOL + APPLICATION_ID + EQUAL_SYMBOL + applicationId);

        } catch (DAOException e) {
            LOGGER.catching(e);
            content.setSessionAttribute(ATTRIBUTE_MESSAGE, e.getMessage());
            return new CommandResult(CommandResult.ResponseType.FORWARD, ConfigurationManager.PAGE_ERROR);
        }
        if (user.getUserType().equals(User.UserType.CANDIDATE) || user.getUserType().equals(User.UserType.HR)) {
            return new CommandResult(CommandResult.ResponseType.FORWARD, ConfigurationManager.PAGE_APPLICATION_INTERVIEWS);
        } else return new CommandResult(CommandResult.ResponseType.FORWARD, ConfigurationManager.PAGE_ERROR_403);
    }

    /**
     * @param requestContent contains identification number of application from user, who will participate
     *                       in job interview
     * @return CommandResult with page that shows if the inserting in database information
     * about job interview was successful
     */
    public CommandResult createJobInterview(RequestContent requestContent) {
        JobInterview jobInterview = new JobInterview();
        String type = requestContent.getParameter(JOB_INTERVIEW_TYPE);
        String dateTime = requestContent.getParameter(DATETIME);
        int applicationId = Integer.parseInt(requestContent.getParameter(APPLICATION_ID));
        Application application = new Application();
        application.setId(applicationId);
        String place = requestContent.getParameter(PLACE);
        JobInterviewValidator jobInterviewValidator = new JobInterviewValidator();
        requestContent.setSessionAttribute(NEXT_COMMAND, COMMAND_VIEW_JOB_INTERVIEWS + AND_SYMBOL + APPLICATION_ID + EQUAL_SYMBOL + applicationId);
        if (!jobInterviewValidator.checkInterviewData(dateTime, place, type)) {
            requestContent.setRequestAttribute(ATTRIBUTE_MESSAGE, MESSAGE_WRONG_FORMAT);
            return new CommandResult(CommandResult.ResponseType.FORWARD, ConfigurationManager.PAGE_OPERATION_FAILED);
        }
        jobInterview.setDateAndTimeOfInterview(dateTime, DateTimeFormatter.ISO_DATE_TIME);
        jobInterview.setPlace(place);
        jobInterview.setApplication(application);
        jobInterview.setJobInterviewType(type);
        JobInterviewDAO jobInterviewDAO = new JobInterviewDAOImpl();
        try {
            return resultOfOperation(jobInterviewDAO.create(jobInterview), requestContent, MessageManager.getProperty(ERROR_CREATION_FAILED, getLocale(requestContent)));
        } catch (DAOException e) {
            return resultAfterException(e, requestContent);
        }
    }

    /**
     * @param requestContent contain application id in answer to which HR schedule a job interview
     * @return Command result that contains page, where HR can enter information about job interview
     */
    public CommandResult newJobInterview(RequestContent requestContent) {
        int applicationId = Integer.parseInt(requestContent.getParameter(APPLICATION_ID));
        requestContent.setRequestAttribute(APPLICATION_ID, applicationId);
        requestContent.setSessionAttribute(LAST_COMMAND, COMMAND_ADD_JOB_INTERVIEW + AND_SYMBOL + APPLICATION_ID + EQUAL_SYMBOL + applicationId);
        return new CommandResult(CommandResult.ResponseType.FORWARD, ConfigurationManager.PAGE_ADD_JOB_INTERVIEW);
    }

    /**
     * @param requestContent contain id of job interview that HR wants to delete
     * @return CommandResult with page that shows result of deleting and than forward to
     * page with other job interviews scheduled for this application
     */
    public CommandResult deleteJobInterview(RequestContent requestContent) {
        JobInterviewDAO jobInterviewDAO = new JobInterviewDAOImpl();
        int jobInterviewId = Integer.parseInt(requestContent.getParameter(JOB_INTERVIEW_ID));
        requestContent.setSessionAttribute(NEXT_COMMAND, requestContent.getSessionAttribute(LAST_COMMAND));//добавіть інфу о заявке
        return resultOfOperation(jobInterviewDAO.delete(jobInterviewId), requestContent, MessageManager.getProperty(ERROR_FAIL_DELETE, getLocale(requestContent)));
    }

    /**
     * @param requestContent contains parameters of job interview that HR wants to change
     * @return CommandResult with page that shows result of updating and than forward to
     * page with other job interviews scheduled for this application
     */
    public CommandResult updateJobInterview(RequestContent requestContent) {
        JobInterviewValidator jobInterviewValidator = new JobInterviewValidator();
        String dateTime = requestContent.getParameter(DATETIME);
        String place = requestContent.getParameter(PLACE);
        String type = requestContent.getParameter(JOB_INTERVIEW_TYPE);
        requestContent.setSessionAttribute(NEXT_COMMAND, requestContent.getSessionAttribute(LAST_COMMAND));
        if (!jobInterviewValidator.checkInterviewData(dateTime, place, type)) {
            return new CommandResult(CommandResult.ResponseType.REDIRECT, ConfigurationManager.PAGE_OPERATION_FAILED);
        }
        JobInterview jobInterview = new JobInterview();
        jobInterview.setJobInterviewType(type);
        jobInterview.setPlace(place);
        jobInterview.setDateAndTimeOfInterview(dateTime, DateTimeFormatter.ISO_DATE_TIME);
        jobInterview.setId(Integer.parseInt(requestContent.getParameter(JOB_INTERVIEW_ID)));
        JobInterviewDAO jobInterviewDAO = new JobInterviewDAOImpl();
        try {
            return resultOfOperation(jobInterviewDAO.update(jobInterview), requestContent, MessageManager.getProperty(MESSAGE_CANT_UPDATE, getLocale(requestContent)));
        } catch (DAOException e) {
            return new CommandResult(CommandResult.ResponseType.REDIRECT, ConfigurationManager.PAGE_ERROR);
        }

    }

    /**
     * @param requestContent contains opinion about candidate on vacancy who took part in job interview
     *                       and id of this interview
     * @return CommandResult with page that shows result of inserting opinion and than forward to
     * page with other job interviews scheduled for this application
     */
    public CommandResult changeOpinion(RequestContent requestContent) {
        String opinion = requestContent.getParameter(OPINION);
        int jobInterviewId = Integer.parseInt(requestContent.getParameter(JOB_INTERVIEW_ID));
        JobInterviewDAO jobInterviewDAO = new JobInterviewDAOImpl();
        requestContent.setSessionAttribute(NEXT_COMMAND, requestContent.getSessionAttribute(LAST_COMMAND));
        if (jobInterviewDAO.updateOpinion(opinion, jobInterviewId)) {
            return new CommandResult(CommandResult.ResponseType.REDIRECT, ConfigurationManager.PAGE_OPERATION_SUCCESSFUL);
        } else {
            return new CommandResult(CommandResult.ResponseType.REDIRECT, ConfigurationManager.PAGE_OPERATION_FAILED);
        }
    }
}
