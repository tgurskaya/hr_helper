package by.epam.hrhelper.controller;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpSession;
import java.util.Enumeration;
import java.util.HashMap;
import java.util.Set;

/**
 * Class that contains attributes and parameters from HttpServletRequest
 */
public class RequestContent {
    private HashMap<String, Object> requestAttributes;
    private HashMap<String, String[]> requestParameters;
    private HashMap<String, Object> sessionAttributes;
    /**
     * variable that shows, is it need to invalidate session
     */
    private boolean newSession = false;

    /**
     * makes variable newSession true
     */
    public void invalidateSession() {
        newSession = true;
    }

    private void extractValues(HttpServletRequest request) {
        requestParameters = (HashMap<String, String[]>) request.getParameterMap();
    }

    private void insertAttributes(HttpServletRequest request) {
        requestAttributes = new HashMap<>();
        Enumeration enumeration = request.getAttributeNames();
        while (enumeration.hasMoreElements()) {
            String attributeName = String.valueOf(enumeration.nextElement());
            requestAttributes.put(attributeName, request.getAttribute(attributeName));
        }
        sessionAttributes = new HashMap<>();
        HttpSession httpSession = request.getSession(true);
        enumeration = httpSession.getAttributeNames();
        while (enumeration.hasMoreElements()) {
            String sessionAttributeName = String.valueOf(enumeration.nextElement());
            sessionAttributes.put(sessionAttributeName, request.getSession().getAttribute(sessionAttributeName));
        }

    }

    RequestContent(HttpServletRequest request) {
        extractValues(request);
        insertAttributes(request);
    }

    /**
     * @param parameterName - name of required parameter
     * @return null if there is no parameter with this name
     */
    public String getParameter(String parameterName) {
        if (requestParameters.get(parameterName) != null)
            return requestParameters.get(parameterName)[0];
        else return null;
    }

    /**
     * @param attributeName - name of attribute
     * @return value of attribute with the required name
     */
    public Object getSessionAttribute(String attributeName) {
        return sessionAttributes.get(attributeName);
    }

    private Object getRequestAttribute(String attributeName) {
        return requestAttributes.get(attributeName);
    }

    /**
     * @param attributeName  - name of request attribute
     * @param attributeValue - value of request attribute
     */
    public void setRequestAttribute(String attributeName, Object attributeValue) {
        requestAttributes.put(attributeName, attributeValue);
    }

    /**
     * @param attributeName  - name of session attribute
     * @param attributeValue - value of session attribute
     */
    public void setSessionAttribute(String attributeName, Object attributeValue) {
        sessionAttributes.put(attributeName, attributeValue);
    }


    private HashMap<String, Object> getRequestAttributes() {
        return requestAttributes;
    }

    private HashMap<String, Object> getSessionAttributes() {
        return sessionAttributes;
    }

    /**
     * @param httpServletRequest - request object in which parameters and attributes will be inserted
     */
    void insertAttributesInServlet(HttpServletRequest httpServletRequest) {
        if (newSession) {
            httpServletRequest.getSession().invalidate();
            newSession = false;
        } else {
            Set<String> sessionAttributesNames = getSessionAttributes().keySet();
            for (Object attributeName : sessionAttributesNames) {
                httpServletRequest.getSession().setAttribute(attributeName.toString(), getSessionAttribute(attributeName.toString()));
            }
            Set<String> requestAttributesNames = getRequestAttributes().keySet();
            for (Object attributeName : requestAttributesNames) {
                httpServletRequest.setAttribute(attributeName.toString(), getRequestAttribute(attributeName.toString()));
            }
        }
    }

}
