package by.epam.hrhelper.controller;

import by.epam.hrhelper.dao.UserDAO;
import by.epam.hrhelper.dao.impl.UserDaoImpl;
import by.epam.hrhelper.entity.User;
import by.epam.hrhelper.resource.ConfigurationManager;
import org.apache.commons.fileupload.FileItem;
import org.apache.commons.fileupload.FileItemFactory;
import org.apache.commons.fileupload.FileUploadException;
import org.apache.commons.fileupload.disk.DiskFileItemFactory;
import org.apache.commons.fileupload.servlet.ServletFileUpload;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.servlet.ServletException;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;

/**
 * servlet that responsible for downloading and inserting into database image
 */
@WebServlet("/upload")
@MultipartConfig(maxFileSize = 1024 * 512 * 3, fileSizeThreshold = 1024 * 512 * 3, maxRequestSize = 1024 * 512 * 3)
public class ImageServlet extends HttpServlet {
    private static final Logger LOGGER = LogManager.getLogger(ImageServlet.class);
    private static final String NEXT_COMMAND = "nextCommand";
    private static final String USER = "user";
    private static final String HOME = "home";
    private static final String UTF8ENCODING = "UTF-8";

    /**
     * @param request  - request from client that contains photo
     * @param response - response to client
     * @throws IOException      can be caused while using HttpServletRequest and Response
     * @throws ServletException exception in method doPost in Servlet
     */
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws IOException, ServletException {
        request.setCharacterEncoding(UTF8ENCODING);
        FileItemFactory factory = new DiskFileItemFactory();
        ServletFileUpload upload = new ServletFileUpload(factory);
        try {
            List<FileItem> fields = upload.parseRequest(request);
            for (FileItem item : fields) {
                byte[] bytes = item.get();
                User user = (User) request.getSession().getAttribute(USER);
                UserDAO userDao = new UserDaoImpl();
                if (userDao.insertPhoto(user.getId(), bytes)) {
                    request.getSession().setAttribute(NEXT_COMMAND, HOME);
                    response.sendRedirect(ConfigurationManager.PAGE_OPERATION_SUCCESSFUL);
                } else {
                    response.sendRedirect(ConfigurationManager.PAGE_ERROR);
                }
            }
        } catch (FileUploadException e) {
            LOGGER.catching(e);
            response.sendRedirect(ConfigurationManager.PAGE_ERROR);
        }

    }


}
