package by.epam.hrhelper.controller;

import by.epam.hrhelper.command.Command;
import by.epam.hrhelper.command.CommandFactoryClient;
import by.epam.hrhelper.command.CommandResult;
import by.epam.hrhelper.connectionpool.ConnectionPool;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

/**
 * Servlet that can receive requests from application and sends responses
 */
public class MainServlet extends HttpServlet {

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        processRequest(request, response);
    }

    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Method takes command name from request parameters, turns it into command object, execute command
     * gets response page and type of response
     *
     * @param request  - client's request that contains attributes and parameters
     * @param response - server's response to client
     * @throws ServletException - standard exception caused in methods doPost and doGet
     * @throws IOException      -  can be caused while using HttpServletRequest and Response
     */
    private void processRequest(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String page;
        CommandFactoryClient client = new CommandFactoryClient();
        RequestContent requestContent = new RequestContent(request);
        Command command = client.initCommand(requestContent);
        CommandResult commandResult = command.execute(requestContent);
        CommandResult.ResponseType responseType = commandResult.getResponseType();
        page = commandResult.getPage();
        if (responseType.equals(CommandResult.ResponseType.FORWARD)) {
            RequestDispatcher dispatcher = getServletContext().getRequestDispatcher(commandResult.getPage());
            requestContent.insertAttributesInServlet(request);
            dispatcher.forward(request, response);
        } else {
            requestContent.insertAttributesInServlet(request);
            response.sendRedirect(request.getContextPath() + page);
        }
    }

    /**
     * executing when application stops
     */
    @Override
    public void destroy() {
        ConnectionPool.getInstance().closePool();
    }
}

