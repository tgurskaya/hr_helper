package by.epam.hrhelper.controller;

import by.epam.hrhelper.dao.ApplicationDAO;
import by.epam.hrhelper.dao.impl.ApplicationDAOImpl;
import by.epam.hrhelper.entity.Application;
import by.epam.hrhelper.entity.User;
import by.epam.hrhelper.entity.Vacancy;
import by.epam.hrhelper.exception.DAOException;
import by.epam.hrhelper.resource.ConfigurationManager;
import by.epam.hrhelper.resource.MessageManager;
import by.epam.hrhelper.validation.ApplicationValidator;
import org.apache.commons.fileupload.FileItem;
import org.apache.commons.fileupload.FileUploadException;
import org.apache.commons.fileupload.disk.DiskFileItemFactory;
import org.apache.commons.fileupload.servlet.ServletFileUpload;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.MultipartConfig;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

/**
 * servlet that responsible for downloading and inserting pdf file with resume
 */
@WebServlet("/newApplication")
@MultipartConfig(maxFileSize = 1024 * 512 * 3, fileSizeThreshold = 1024 * 512 * 3, maxRequestSize = 1024 * 512 * 3)
public class PDFServlet extends HttpServlet {
    private static final Logger LOGGER = LogManager.getLogger(ImageServlet.class);
    private static final String APPLICATION_LIST = "applicationList";
    private static final String NEXT_COMMAND = "nextCommand";
    private static final String HOME = "home";
    private static final String USER = "user";
    private static final String UTF8ENCODING = "UTF-8";
    private static final String VACANCY_ID = "vacancyId";
    private static final String APPLICATION_ID = "applicationId";
    private static final String MESSAGE = "message";
    private static final String LABEL_WRONG_FORMAT = "label.wrongFormat";


    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        try {
            request.getSession().setAttribute(NEXT_COMMAND, HOME);
            request.setCharacterEncoding(UTF8ENCODING);
            ApplicationDAO applicationDAO = new ApplicationDAOImpl();
            User user = (User) request.getSession().getAttribute(USER);
            String coveringLetter = null;
            byte[] resume = null;
            int vacancyId = 0;
            int applicationId = 0;
            List<FileItem> items = new ServletFileUpload(new DiskFileItemFactory()).parseRequest(request);
            for (FileItem item : items) {
                if (item.isFormField()) {
                    if (item.getFieldName().equals(VACANCY_ID)) {
                        vacancyId = Integer.parseInt(item.getString());
                    } else if (item.getFieldName().equals(APPLICATION_ID)) {
                        applicationId = Integer.parseInt(item.getString());
                    } else {
                        coveringLetter = new String(item.get(), UTF8ENCODING);
                        ApplicationValidator applicationValidator = new ApplicationValidator();
                        if (!applicationValidator.coveringLetterTest(coveringLetter)) {
                            request.getSession().setAttribute(MESSAGE, MessageManager.getProperty(LABEL_WRONG_FORMAT, request.getLocale()));
                            response.sendRedirect(ConfigurationManager.PAGE_OPERATION_FAILED);
                        }
                    }
                } else {
                    resume = item.get();
                }
            }

            if (resume != null) {
                if (applicationId == 0) {
                    if (vacancyId != 0) {

                        if (createApplication(applicationDAO, coveringLetter, resume, user, vacancyId)) {
                            ArrayList<Application> applications = applicationDAO.showCandidateApplication(user, 0, 5);
                            request.setAttribute(APPLICATION_LIST, applications);
                            RequestDispatcher dispatcher = getServletContext().getRequestDispatcher(ConfigurationManager.PAGE_OPERATION_SUCCESSFUL);
                            dispatcher.forward(request, response);
                        } else {
                            response.sendRedirect(ConfigurationManager.PAGE_ERROR);
                        }
                    } else {
                        response.sendRedirect(ConfigurationManager.PAGE_ERROR);
                    }
                } else {
                    if (applicationDAO.changeResume(applicationId, resume)) {
                        response.sendRedirect(ConfigurationManager.PAGE_OPERATION_SUCCESSFUL);
                    } else {
                        response.sendRedirect(ConfigurationManager.PAGE_OPERATION_FAILED);
                    }
                }
            } else {
                response.sendRedirect(ConfigurationManager.PAGE_ERROR);
            }
        } catch (FileUploadException | DAOException e) {
            LOGGER.catching(e);
            response.sendRedirect(ConfigurationManager.PAGE_ERROR);
        }
    }


    /**
     * @param applicationDAO - dao object that uses for inserting application in database
     * @param coveringLetter - String with covering letter
     * @param resume         - byte array that contains pdf document with resume
     * @param user           - user that want's to insert resume into database
     * @param vacancyId      - identification number of vacancy in database
     * @return true if information is inserted in database successfully
     * @throws DAOException can be caused while inserting information into database
     */
    private boolean createApplication(ApplicationDAO applicationDAO, String coveringLetter, byte[] resume, User user, int vacancyId) throws DAOException {
        Application application = new Application();
        application.setCoveringLetter(coveringLetter);
        application.setResume(resume);
        application.setCandidate(user);
        Vacancy vacancy = new Vacancy();
        vacancy.setId(vacancyId);
        application.setVacancy(vacancy);
        return applicationDAO.create(application);
    }

}