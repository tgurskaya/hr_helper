package by.epam.hrhelper.filter;

import by.epam.hrhelper.dao.VacancyDAO;
import by.epam.hrhelper.dao.impl.VacancyDAOImpl;
import by.epam.hrhelper.entity.Vacancy;
import by.epam.hrhelper.exception.DAOException;

import javax.servlet.*;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;

/**
 * filter that is used for filling cards with vacancies in main page
 */
@WebFilter(dispatcherTypes = {
        DispatcherType.REQUEST,
        DispatcherType.FORWARD,
        DispatcherType.INCLUDE},urlPatterns = {"/jsp/main.jsp"})
public class MainPageFilter implements Filter {
    private static final String VACANCY_LIST = "vacancyList";

    @Override
    public void init(FilterConfig filterConfig) throws ServletException {

    }

    @Override
    public void doFilter(ServletRequest servletRequest, ServletResponse servletResponse, FilterChain filterChain) throws IOException, ServletException {
        HttpServletRequest req = (HttpServletRequest) servletRequest;
        HttpServletResponse resp = (HttpServletResponse) servletResponse;
        VacancyDAO vacancyDAO = new VacancyDAOImpl();
        try {
            List<Vacancy> vacancyList = vacancyDAO.showVacancies(0,10,Vacancy.VacancyStatus.OPEN.name());
            req.setAttribute(VACANCY_LIST, vacancyList);
        } catch (DAOException e) {
            e.printStackTrace();
        }
        filterChain.doFilter(req, resp);
    }

    @Override
    public void destroy() {
    }
}
