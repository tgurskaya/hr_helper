package by.epam.hrhelper.entity;

/**
 * entity class that contains fields that a relevant to fields from table application in database
 */
public class Application extends Entity  {
    private User candidate;
    private String coveringLetter;
    private Vacancy vacancy;
    private String decision;
    private byte[] resume;

    public User getCandidate() {
        return candidate;
    }

    public void setCandidate(User candidate) {
        this.candidate = candidate;
    }

    public String getCoveringLetter() {
        return coveringLetter;
    }

    public void setCoveringLetter(String coveringLetter) {
        this.coveringLetter = coveringLetter;
    }

    public Vacancy getVacancy() {
        return vacancy;
    }

    public void setVacancy(Vacancy vacancy) {
        this.vacancy = vacancy;
    }

    public String getDecision() {
        return decision;
    }

    public void setDecision(String decision) {
        this.decision = decision;
    }

    public byte[] getResume() {
        return resume;
    }

    public void setResume(byte[] resume) {
        this.resume = resume;
    }
}
