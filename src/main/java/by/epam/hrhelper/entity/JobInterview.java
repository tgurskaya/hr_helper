package by.epam.hrhelper.entity;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

/**
 * entity class that contains fields that a relevant to fields from table job_interview in database
 */
public class JobInterview extends Entity {


    private String jobInterviewType;
    private Application application;
    private String place;
    private LocalDateTime dateAndTimeOfInterview;
    private String opinionAboutCandidate;

    public String getJobInterviewType() {
        return jobInterviewType;
    }

    public void setJobInterviewType(String jobInterviewType) {
        this.jobInterviewType = jobInterviewType;
    }

    public Application getApplication() {
        return application;
    }

    public void setApplication(Application application) {
        this.application = application;
    }

    public String getPlace() {
        return place;
    }

    public void setPlace(String place) {
        this.place = place;
    }

    public LocalDateTime getDateAndTimeOfInterview() {
        return dateAndTimeOfInterview;
    }

    public void setDateAndTimeOfInterview(String dateAndTimeOfInterview,DateTimeFormatter dateTimeFormatter) {
        this.dateAndTimeOfInterview = LocalDateTime.parse(dateAndTimeOfInterview,dateTimeFormatter);
    }

    public String getOpinionAboutCandidate() {
        return opinionAboutCandidate;
    }

    public void setOpinionAboutCandidate(String opinionAboutCandidate) {
        this.opinionAboutCandidate = opinionAboutCandidate;
    }
}
