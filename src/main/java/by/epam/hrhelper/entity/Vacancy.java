package by.epam.hrhelper.entity;

/**
 * entity class that contains fields that a relevant to fields from table vacancy in database
 */
public class Vacancy extends Entity {
    /**
     * enum that contains vacancyStatus
     */
    public enum VacancyStatus{
        OPEN,CLOSED,PAUSED
    }

    private String name;
    private String salary;
    private String description;
    private String responsibilities;
    private String requirements;
    private VacancyStatus status;
    private int hrId;

    public Vacancy(String name, String salary, String description, String responsibilities, String requirements, VacancyStatus status,  int hrId) {
        this.name = name;
        this.salary = salary;
        this.description = description;
        this.responsibilities = responsibilities;
        this.requirements = requirements;
        this.status = status;
        this.hrId = hrId;
    }

    public Vacancy() {
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSalary() {
        return salary;
    }

    public void setSalary(String salary) {
        this.salary = salary;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getResponsibilities() {
        return responsibilities;
    }

    public void setResponsibilities(String responsibilities) {
        this.responsibilities = responsibilities;
    }

    public String getRequirements() {
        return requirements;
    }

    public void setRequirements(String requirements) {
        this.requirements = requirements;
    }

    public VacancyStatus getStatus() {
        return status;
    }

    public void setStatus(VacancyStatus status) {
        this.status = status;
    }

    public int getHrId() {
        return hrId;
    }

    public void setHrId(int hrId) {
        this.hrId = hrId;
    }

}
