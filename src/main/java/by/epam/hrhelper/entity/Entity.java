package by.epam.hrhelper.entity;

/**
 * base entity class
 */
public class Entity{
    private int id;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }
}
