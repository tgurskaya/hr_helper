package by.epam.hrhelper.entity;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;

/**
 * entity class that contains fields that a relevant to fields from table user in database
 */
public class User extends Entity {

    /**
     * enum that contains types of user
     */
    public enum UserType{
        CANDIDATE,HR,ADMIN
    }

    private String name;
    private LocalDate birthday;
    private String phone;
    private String email;
    private String password;
    private UserType userType;
    private static final DateTimeFormatter FORMATTER = DateTimeFormatter.ofPattern("yyyy-MM-dd");
    private boolean isBlocked;

    public boolean isBlocked() {
        return isBlocked;
    }

    public void setBlocked(boolean blocked) {
        isBlocked = blocked;
    }

    public UserType getUserType() {
        return userType;
    }

    public void setUserType(UserType userType) {
        this.userType = userType;
    }

    public User(){}

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public LocalDate getBirthday() {
        return birthday;
    }

    public void setBirthday(String birthday) {
        this.birthday = LocalDate.parse(birthday,FORMATTER);
    }

    public String getPassword() {
        return password;
    }

    public String getEmail() {
        return email;
    }

}
