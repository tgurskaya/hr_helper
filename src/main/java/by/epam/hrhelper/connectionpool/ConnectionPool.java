package by.epam.hrhelper.connectionpool;

import org.apache.logging.log4j.Level;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.ArrayDeque;
import java.util.Properties;
import java.util.Queue;
import java.util.concurrent.ArrayBlockingQueue;
import java.util.concurrent.BlockingQueue;

/**
 * singleton class that contains connections to database
 */
public final class ConnectionPool {
    private static final Logger LOGGER = LogManager.getLogger(ConnectionPool.class);
    private static final String MESSAGE_DATABASE_ACCESS_ERROR = "Database access error";
    private static final String MESSAGE_INTERRUPTED_EXCEPTION = "Interrupted while waiting for connection";
    private static final String MESSAGE_REMOVING_WILD_CONNECTION = "Trying to remove wild connection";
    private static final String MESSAGE_SUCCESSFUL_CLOSING_POOL = "Connections have been successfully closed";
    private static final String MESSAGE_SQL_EXCEPTION_WHILE_CLOSING_POOL = "A database access error occurs while closing connection";

    private BlockingQueue<ProxyConnection> freeConnections;
    private Queue<ProxyConnection> busyConnections;

    /**
     * Class is uses for holding a single copy of ConnectionPool
     */
    private static class ConnectionPoolHolder {
        private static final ConnectionPool CONNECTION_POOL = new ConnectionPool();
    }

    /**
     * @return singleton object of ConnectionPool
     */
    public static ConnectionPool getInstance() {
        return ConnectionPoolHolder.CONNECTION_POOL;
    }

    /**
     * private constructor for singleton object ConnectionPool. It contains creation of connections
     */
    private ConnectionPool() {
        ConnectionFactory connectionHelper = ConnectionFactory.getInstance();
        Properties properties = connectionHelper.getProperties();
        freeConnections = new ArrayBlockingQueue<>(connectionHelper.getPoolSize());
        busyConnections = new ArrayDeque<>(connectionHelper.getPoolSize());
        for (int i = 0; i < connectionHelper.getPoolSize(); i++) {
            try {
                String url = properties.getProperty(ConnectionParameter.DB_URL);
                String user = properties.getProperty(ConnectionParameter.DB_USER);
                String pass = properties.getProperty(ConnectionParameter.DB_PASSWORD);
                ProxyConnection connection = new ProxyConnection(DriverManager.getConnection(url, user, pass));
                freeConnections.add(connection);
            } catch (SQLException ex) {
                LOGGER.log(Level.ERROR, MESSAGE_DATABASE_ACCESS_ERROR);
            }
        }
        if(freeConnections.size()!=connectionHelper.getPoolSize()){
            LOGGER.error("Pool size is "+freeConnections.size());
        }
    }

    /**
     * @return ProxyConnection from pool. This connection from list with free connections to
     * list with busy connections
     */
    public ProxyConnection takeConnection() {
        try {
            ProxyConnection connection = freeConnections.take();
            busyConnections.add(connection);
            return connection;
        } catch (InterruptedException ex) {
            LOGGER.error(MESSAGE_INTERRUPTED_EXCEPTION);
            return null;
        }
    }

    /**
     * @param connection-moves from list with busy connections to
     * list with free connections
     */
    public void retrieveConnection(ProxyConnection connection) {
        if (busyConnections.remove(connection)) {
            freeConnections.add(connection);
        } else {
            LOGGER.error(MESSAGE_REMOVING_WILD_CONNECTION);
        }
    }

    /**
     * This function is called when application stops and it's necessary to close all connections
     */
    public void closePool() {
        try {
            for (int i = 0; i < freeConnections.size(); i++) {
                freeConnections.take().closeConnection();
            }
            DriverManager.deregisterDriver(ConnectionFactory.getInstance().driver);
            LOGGER.log(Level.INFO, MESSAGE_SUCCESSFUL_CLOSING_POOL);
        } catch (InterruptedException ex) {
            LOGGER.error(MESSAGE_INTERRUPTED_EXCEPTION);
        } catch (SQLException ex) {
            LOGGER.log(Level.ERROR, MESSAGE_SQL_EXCEPTION_WHILE_CLOSING_POOL);
        }
    }
}