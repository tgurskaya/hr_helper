package by.epam.hrhelper.connectionpool;

/**
 * class contains names of parameters in .properties file and default pool size
 */
public class ConnectionParameter {
    static final String DB_PROPERTIES_PATH = "database.mysql";
    static final String DB_URL = "db.url";
    static final String DB_USER = "db.user";
    static final String DB_PASSWORD = "db.password";
    static final String DB_POOL_SIZE = "db.poolsize";
    static final int DB_DEFAULT_POOL_SIZE = 16;

    private ConnectionParameter() {
    }
}
