package by.epam.hrhelper.connectionpool;

import com.mysql.jdbc.Driver;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.sql.DriverManager;
import java.sql.SQLException;
import java.util.MissingResourceException;
import java.util.Properties;
import java.util.ResourceBundle;

import static by.epam.hrhelper.connectionpool.ConnectionParameter.*;

/**
 * subsidiary class that helps to connect to connect to database
 */
class ConnectionFactory {
    private static final Logger LOGGER = LogManager.getLogger(ConnectionFactory.class);
    private static final ConnectionFactory INSTANCE = new ConnectionFactory();
    private static final String MESSAGE_CANT_REGISTER_DRIVER = "Can't register driver";
    private static final String MESSAGE_ERROR_CLASS_CAST = "Object found for the given key is not a string";
    private static final String MESSAGE_ERROR_MISSING_RESOURCE = "No resource bundle for the specified base name can be found";
    private static final String MESSAGE_ERROR_WRONG_FORMAT = "The string in db.poolsize does not contain a parsable integer";
    final Driver driver;

    /**
     * @return singleton object of ConnectionFactory
     */
    static ConnectionFactory getInstance() {
        return INSTANCE;
    }

    private final Properties connectionInfo;

    /**
     * private constructor for singleton object ConnectionFactory. It contains registration of drivers and
     * initializing properties of connection to database
     *
     * @throws RuntimeException if there is a problem with connecting to database
     */
    private ConnectionFactory() throws RuntimeException {
        connectionInfo = new Properties();
        try {
            ResourceBundle resourceBundle = ResourceBundle.getBundle(DB_PROPERTIES_PATH);
            driver = new Driver();
            DriverManager.registerDriver(driver);
            connectionInfo.put(DB_URL, resourceBundle.getString(DB_URL));
            connectionInfo.put(DB_USER, resourceBundle.getString(DB_USER));
            connectionInfo.put(DB_PASSWORD, resourceBundle.getString(DB_PASSWORD));
            connectionInfo.put(DB_POOL_SIZE, resourceBundle.getString(DB_POOL_SIZE));
        } catch (MissingResourceException ex) {
            LOGGER.fatal(MESSAGE_ERROR_MISSING_RESOURCE);
            throw new RuntimeException(ex);
        } catch (ClassCastException ex) {
            LOGGER.fatal(MESSAGE_ERROR_CLASS_CAST);
            throw new RuntimeException(ex);
        } catch (SQLException ex) {
            LOGGER.fatal(MESSAGE_CANT_REGISTER_DRIVER);
            throw new RuntimeException(ex);
        }
    }

    /**
     * @return quantity of connections in ConnectionPool.
     * If quantity is empty in properties file it returns default size
     */
    int getPoolSize() {
        try {
            return Integer.parseInt(connectionInfo.getProperty(DB_POOL_SIZE));
        } catch (NumberFormatException ex) {
            LOGGER.error(MESSAGE_ERROR_WRONG_FORMAT);
            return DB_DEFAULT_POOL_SIZE;
        }
    }

    /**
     * @return url, password, user and poolsize from properties
     */
    Properties getProperties() {
        return connectionInfo;
    }

}