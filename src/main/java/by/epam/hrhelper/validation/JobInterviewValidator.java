package by.epam.hrhelper.validation;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * class that contains methods for validation data inserting in table job_interview
 */
public class JobInterviewValidator {
    /**
     * Regular expression for data and time inserted in column dateTimeOfInterview
     */
    private static final String DATETIME_TEXT_PATTERN = "(20[1-9])[\\d]-(0?[1-9]|1[012])-(0[1-9]|[12][0-9]|3[01])T([01][\\d]|2[0-3]):[0-5][\\d]";

    /**
     * @param dateTime-date and time inserted in table job_interview column dateTimeOfInterview
     * @param place-        place of interview inserted in table job_interview column place
     * @param type-         type of interview inserted in table type column place
     * @return true if data is valid and can be inserted in table job_interview
     */
    public boolean checkInterviewData(String dateTime, String place, String type) {
        boolean result = place.length() < 46 && type.length() < 31;
        Pattern dateTimePattern = Pattern.compile(DATETIME_TEXT_PATTERN);
        Matcher dateTimeMatcher = dateTimePattern.matcher(dateTime);
        return result && dateTimeMatcher.matches();
    }
}
