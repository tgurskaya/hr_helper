package by.epam.hrhelper.validation;

import by.epam.hrhelper.entity.Vacancy;

/**
 *class that contains methods for validation data inserting in table vacancy
 */
public class VacancyValidator {
    /**
     * maximum length of string with description,responcibilities,requirements
     */
    private static final int TEXT_MAX_LENGTH = 2001;
    /**
     * maximum length of string with salary
     */
    private static final int SALARY_MAX_LENGTH = 16;
    /**
     * maximum length of string with name of vacancy
     */
    private static final int NAME_MAX_LENGTH = 46;

    /**
     * @param name             - vacancy name
     * @param salary           - salary of employee who will be hired for this vacancy
     * @param description      - vacancy description
     * @param requirements     - requirements for the candidate
     * @param responcibilities - requirements of the employee who will be hired for this vacancy
     * @return true if inserted data about vacancy is valid
     */
    public boolean checkVacancyData(String name, String salary, String description, String requirements, String responcibilities) {
        boolean result = name.length() < NAME_MAX_LENGTH;
        if (!salary.isEmpty()) {
            result &= salary.length() < SALARY_MAX_LENGTH;
        }
        result &= description.length() < TEXT_MAX_LENGTH;
        if (!requirements.isEmpty()) {
            result &= requirements.length() < TEXT_MAX_LENGTH;
        }
        if (!responcibilities.isEmpty()) {
            result &= responcibilities.length() < TEXT_MAX_LENGTH;
        }
        return result;
    }

    /**
     * @param vacancy-object of class Vacancy that contains information about vacancy
     * @return true if inserted data about vacancy is valid
     */
    public boolean checkVacancy(Vacancy vacancy) {
        boolean result = checkVacancyData(vacancy.getName(), vacancy.getSalary(), vacancy.getDescription(), vacancy.getRequirements(), vacancy.getResponsibilities());
        return result;
    }

    /**
     * @param vacancyStatus - string that will be transformed to VacancyStatus object
     * @return true if vacancy status is exists in enumVacancyStatus
     */
    public boolean checkStatus(String vacancyStatus) {
        try {
            Vacancy.VacancyStatus.valueOf(vacancyStatus.toUpperCase());
            return true;
        } catch (IllegalArgumentException e) {
            return false;
        }
    }


}
