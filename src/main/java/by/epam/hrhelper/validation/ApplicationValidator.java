package by.epam.hrhelper.validation;

import by.epam.hrhelper.dao.ApplicationDAO;
import by.epam.hrhelper.dao.impl.ApplicationDAOImpl;
import by.epam.hrhelper.entity.Application;
import by.epam.hrhelper.exception.DAOException;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

/**
 * class that contains methods for validation data inserting in table application
 */
public class ApplicationValidator {

    private static final Logger LOGGER = LogManager.getLogger(ApplicationValidator.class);

    /**
     * @param candidateId-id of user who applies for job
     * @param vacancyId-id   of vacancy
     * @return true if the combination of candidateId and vacancyId is exists in table application
     */
    public boolean isApplicationExists(int candidateId, int vacancyId) {
        ApplicationDAO applicationDAO = new ApplicationDAOImpl();
        try {
            Application application = applicationDAO.findApplicationByCandidateAndVacancy(candidateId, vacancyId);
            return application != null;
        } catch (DAOException e) {
            LOGGER.catching(e);
            return false;
        }
    }

    public boolean coveringLetterTest(String coveringLetter) {
        return coveringLetter.length() < 2001;
    }


}
