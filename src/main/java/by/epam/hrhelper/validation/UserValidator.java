package by.epam.hrhelper.validation;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * class that contains methods for validation data inserting in table user
 */
public class UserValidator {

    /**
     * Regular expression for data and time inserted in column name
     */
    private static final String NAME_TEXT_PATTERN = "^[А-ЯA-Z][a-zA-ZА-Яа-я\\-іў'\\s]{4,50}$";


    /**
     * Regular expression for data and time inserted in column birthday
     */
    private static final String BIRTHDAY_TEXT_PATTERN = "(19[3-9])[\\d]-(0?[1-9]|1[012])-(0[1-9]|[12][0-9]|3[01])";


    /**
     * Regular expression for data and time inserted in column phone
     */
    private static final String PHONE_TEXT_PATTERN = "[0-9\\+\\-]{4,20}$";


    /**
     * Regular expression for data and time inserted in column email
     */
    private static final String EMAIL_TEXT_PATTERN = "[a-z0-9._%+-]+@[a-z0-9.-]+\\.[a-z]{2,4}$";

    /**
     * Regular expression for data and time inserted in column password
     */
    private static final String PASSWORD_TEXT_PATTERN = "(?=^.{8,}$)((?=.*\\d)|(?=.*\\W+))(?![.\\n])(?=.*[A-Z])(?=.*[a-z]).*$";

    /**
     * @param email-email     of user that used as login
     * @param password-user's password
     * @return true if login and password are valid and can be inserted in table user
     */
    public boolean checkLoginAndPassword(String email, String password) {
        Pattern emailPattern = Pattern.compile(EMAIL_TEXT_PATTERN);
        Matcher emailMatcher = emailPattern.matcher(email);
        return emailMatcher.matches() && checkPasswordWithPattern(password);
    }

    /**
     * @param password-user's password
     * @return true if password is valid and can be inserted in table user
     */
    public boolean checkPasswordWithPattern(String password) {
        Pattern passwordPattern = Pattern.compile(PASSWORD_TEXT_PATTERN);
        Matcher passwordMatcher = passwordPattern.matcher(password);
        return passwordMatcher.matches();
    }

    /**
     * @param password-first        inserted password
     * @param repeatPassword-second inserted password
     * @return true if two times inserted passwords are equal
     */
    public boolean checkPasswords(String password, String repeatPassword) {
        return password.equals(repeatPassword);
    }

    /**
     * @param name     - name of user
     * @param birthday - user's birthday
     * @param email    - email of user. Should be unique.
     * @param phone    - user's phone
     * @param password - user's password
     * @return true if inserted for registration data is valid
     */
    public boolean checkDataForRegistration(String name, String birthday, String email, String phone, String password) {
        boolean result = checkLoginAndPassword(email, password);
        Pattern namePattern = Pattern.compile(NAME_TEXT_PATTERN);
        Pattern birthdayPattern = Pattern.compile(BIRTHDAY_TEXT_PATTERN);
        Pattern phonePattern = Pattern.compile(PHONE_TEXT_PATTERN);
        Matcher nameMatcher = namePattern.matcher(name);
        Matcher birthdayMatcher = birthdayPattern.matcher(birthday);
        Matcher phoneMatcher = phonePattern.matcher(phone);
        return result && nameMatcher.matches() && birthdayMatcher.matches() && phoneMatcher.matches();
    }
}
