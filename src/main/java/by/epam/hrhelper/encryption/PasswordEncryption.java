package by.epam.hrhelper.encryption;

import org.apache.commons.codec.digest.DigestUtils;

/**
 * class using for encrypt password
 */
public class PasswordEncryption {
    /**
     * @param pass password witch was entered by user
     * @return encrypted password
     */
    public static String encrypt(String pass) {
        return DigestUtils.md5Hex(pass);
    }
}
