package by.epam.hrhelper.exception;

/**
 * exception that can be caused in DAO layer
 */
public class DAOException extends Exception {

    /**
     * error code from SQL Exception
     */
    private int errorCode;

    public DAOException(){
        super();
    }

    public DAOException(String message){
        super(message);
    }

    public DAOException(String message,Throwable throwable){
        super(message,throwable);
    }

    public DAOException(Throwable throwable){
        super(throwable);
    }
    public DAOException(Throwable throwable,int code){
        super(throwable);
        errorCode=code;
    }

    public int getErrorCode() {
        return errorCode;
    }
}
