package by.epam.hrhelper.tag;

import by.epam.hrhelper.dao.ApplicationDAO;
import by.epam.hrhelper.dao.impl.ApplicationDAOImpl;
import by.epam.hrhelper.exception.DAOException;
import sun.misc.BASE64Encoder;

import javax.servlet.jsp.JspException;
import javax.servlet.jsp.tagext.TagSupport;
import java.io.IOException;

/**
 * custom tag for showing resume
 */
public class FilePDFTag extends TagSupport {
    /**
     * attribute of tag
     */
    private int applicationId;

    private static final String TAG_FIRST_PART="<embed width=\"100%\" height=\"100%\" name=\"plugin\" src=\"data:application/pdf;base64,";
    private static final String TAG_LAST_PART="\">";
    public void setApplicationId(int applicationId) {
        this.applicationId = applicationId;
    }

    @Override
    public int doStartTag() throws JspException {
        try {
            ApplicationDAO applicationDAO = new ApplicationDAOImpl();
            byte[] file = applicationDAO.uploadResume(applicationId);
            BASE64Encoder base64Encoder = new BASE64Encoder();
            String pdfTag =TAG_FIRST_PART;
            pdfTag+=base64Encoder.encode(file);
            pdfTag+=TAG_LAST_PART;
            pageContext.getOut().write(pdfTag);
        } catch (IOException |DAOException e) {
            throw new JspException(e.getMessage());
        }
        return SKIP_BODY;
    }
}
