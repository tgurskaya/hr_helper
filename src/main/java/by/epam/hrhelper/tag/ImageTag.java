package by.epam.hrhelper.tag;

import by.epam.hrhelper.dao.UserDAO;
import by.epam.hrhelper.dao.impl.UserDaoImpl;
import by.epam.hrhelper.exception.DAOException;
import sun.misc.BASE64Encoder;

import javax.servlet.jsp.JspException;
import javax.servlet.jsp.tagext.TagSupport;
import java.io.IOException;

/**
 * tag that used to show user's photo
 */
public class ImageTag extends TagSupport {
    /**
     * attribute of tag, id of user  whose photo will be shown
     */
    private int userId;
    private static final String NO_IMAGE="<img src=\"../img/no-image.png\" alt=\"user\" class=\"img-circle profile-images img-responsive\" >";
    private static final String TAG_FIRST_PART="<img src=\"data:image/png;base64,";
    private static final String TAG_LAST_PART="\"alt=\"user\" class=\"img-circle profile-images img-responsive\">";

    public void setUserId(int id) {
        userId = id;
    }

    @Override
    public int doStartTag() throws JspException {

        try {
            UserDAO userDAO = new UserDaoImpl();
            byte[] photo = userDAO.uploadPhoto(userId);
            if (photo == null) {
                pageContext.getOut().write(NO_IMAGE);
            } else {
                BASE64Encoder base64Encoder = new BASE64Encoder();
                String imageString =TAG_FIRST_PART;
                imageString+=base64Encoder.encode(photo);
                imageString+=TAG_LAST_PART;
                pageContext.getOut().write(imageString);
            }
        } catch (IOException | DAOException e) {
            throw new JspException(e.getMessage());
        }

        return SKIP_BODY;
    }
}
