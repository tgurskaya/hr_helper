package by.epam.hrhelper.validation;

import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import static org.testng.AssertJUnit.assertFalse;
import static org.testng.AssertJUnit.assertTrue;

public class JobInterviewValidatorTest {
    private static final String LONG_TYPE = "primary primary primary primary";
    private static final String CORRECT_DATE="2018-12-12T12:12";
    private static final String INCORRECT_DATE="1999-12-12T12:12";
    private static final String DATE_WITH_WRONG_SYMBOL="A2018-12-12T12:12";
    private static final String CORRECT_ADDRESS ="Minsk, Brovki st.,4";
    private static final String CORRECT_TYPE="primary";
    private static final String LONG_ADDRESS="Minsk, Brovki st.,4Minsk, Brovki st.,4Minsk, Brovki st.,4Minsk, Brovki st.,4";
    JobInterviewValidator jobInterviewValidator;

    @BeforeMethod
    public void setUp() throws Exception {
        jobInterviewValidator=new JobInterviewValidator();
    }

    @AfterMethod
    public void tearDown() throws Exception {
        jobInterviewValidator=null;
    }

    @Test
    public void testCheckInterviewData() throws Exception {
        assertTrue(jobInterviewValidator.checkInterviewData(CORRECT_DATE, CORRECT_ADDRESS,CORRECT_TYPE));
    }

    @Test
    public void testCheckInterviewDataWrongDate() throws Exception {
        assertFalse(jobInterviewValidator.checkInterviewData(INCORRECT_DATE,CORRECT_ADDRESS,CORRECT_TYPE));
    }

    @Test
    public void testCheckInterviewDataWrongSymbolInDate() throws Exception {
        assertFalse(jobInterviewValidator.checkInterviewData(DATE_WITH_WRONG_SYMBOL,CORRECT_ADDRESS,CORRECT_TYPE));
    }
    @Test
    public void testCheckInterviewDataWrongType() throws Exception {
        assertFalse(jobInterviewValidator.checkInterviewData(CORRECT_DATE,CORRECT_ADDRESS,LONG_TYPE));
    }

    @Test
    public void testCheckInterviewDataWrongAddress() throws Exception {
        assertFalse(jobInterviewValidator.checkInterviewData(CORRECT_DATE,LONG_ADDRESS,CORRECT_TYPE));
    }

}