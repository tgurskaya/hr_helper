package by.epam.hrhelper.validation;

import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import static org.testng.AssertJUnit.*;

public class UserValidatorTest {
    UserValidator userValidator;
    @BeforeMethod
    public void setUp() throws Exception {
        userValidator=new UserValidator();
    }

    @AfterMethod
    public void tearDown() throws Exception {
        userValidator=null;
    }

    @Test
    public void testCheckLoginAndPasswordValid() throws Exception {
        assertTrue(userValidator.checkLoginAndPassword("tgur@mail.ru","Capitalandnums123"));
    }

    @Test
    public void testCheckLoginAndPasswordWrongLogin() throws Exception {
        assertFalse(userValidator.checkLoginAndPassword("mail.ru","Capitalandnums123"));
    }

    @Test
    public void testPasswordWithoutNumbers() throws Exception {
        assertFalse(userValidator.checkPasswordWithPattern("Capitalandnums"));
    }

    @Test
    public void testPasswordWithoutCapitalLetter() throws Exception {
        assertFalse(userValidator.checkPasswordWithPattern("capitalandnums123"));
    }

    @Test
    public void testCheckPasswordWithPattern() throws Exception {
        assertTrue(userValidator.checkPasswordWithPattern("Capital123"));
    }

    @Test
    public void testCheckPasswords() throws Exception {
        assertEquals("Password123","Password123");
    }

    @Test
    public void testCheckPasswordsWithDifferent() throws Exception {
        assertNotSame("Password123","PASSWORD123");
    }

    @Test
    public void testCheckDataForRegistration() throws Exception {
      assertTrue(userValidator.checkDataForRegistration("Correct name","1990-10-10","email@mail.com","+375-29-777-89-98","Password1234"));
    }

    @Test
    public void testCheckDataForRegistrationWithWrongDate() throws Exception {
        assertFalse(userValidator.checkDataForRegistration("Correct name","2017-13-13","email@gmail.com","+375-29-99-79-008","Password1234"));
    }

    @Test
    public void testCheckDataForRegistrationWithWrongName() throws Exception {
        assertFalse(userValidator.checkDataForRegistration("incorrect name","1990-10-10","email@gmail.com","+375-29-99-79-008","Password1234"));
    }

    @Test
    public void testCheckDataForRegistrationWithWrongNameLength() throws Exception {
        assertFalse(userValidator.checkDataForRegistration("Correct name name name name name name name name name name","2017-10-10","email@gmail.com","+375-29-99-79-008","Password1234"));
    }

    @Test
    public void testCheckDataForRegistrationWithWrongPhone() throws Exception {
        assertFalse(userValidator.checkDataForRegistration("Correct name","1990-10-10","email@gmail.com","phone","Password1234"));
    }

    @Test
    public void testCheckDataForRegistrationWithWrongPhoneLength() throws Exception {
        assertFalse(userValidator.checkDataForRegistration("Correct name","1990-10-10","email@gmail.com","8989888434837483473847","Password1234"));
    }

}