window.onload = function () {
    document.getElementById("password").onchange = validatePassword;
    document.getElementById("repeatPassword").onchange = validatePassword;
}

function validatePassword() {
    var pass2 = document.getElementById("repeatPassword").value;
    var pass1 = document.getElementById("password").value;
    if (pass1 != pass2)
        document.getElementById("repeatPassword").setCustomValidity("Passwords Don't Match");
    else {
        document.getElementById("repeatPassword").setCustomValidity('');
    }
}


var password = document.getElementById("password"),
    repeatPassword = document.getElementById("repeatPassword");

function validatePassword() {
    if (password.value != repeatPassword.value) {
        repeatPassword.setCustomValidity("Passwords Don't Match");
    } else {
        repeatPassword.setCustomValidity('');
    }
}

password.onchange = validatePassword;
repeatPassword.onkeyup = validatePassword;