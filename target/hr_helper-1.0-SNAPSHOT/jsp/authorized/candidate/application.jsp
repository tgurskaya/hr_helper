<%@ page contentType="text/html;charset=UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<c:set var="language"
       value="${not empty param.language ? param.language : not empty language ? language : pageContext.request.locale}"
       scope="session"/>
<fmt:setLocale value="${language}" scope="session"/>
<fmt:setBundle basename="property.label" var="loc"/>
<fmt:message bundle="${loc}" key="label.obligatoryField" var="obligatoryfield"/>
<fmt:message bundle="${loc}" key="label.coveringLetter" var="coveringLetter"/>
<fmt:message bundle="${loc}" key="label.apply" var="apply"/>
<fmt:message bundle="${loc}" key="label.applying" var="applying"/>
<fmt:message bundle="${loc}" key="label.resume" var="resume"/>
<fmt:message bundle="${loc}" key="label.choseFile" var="choseFile"/>
<fmt:message bundle="${loc}" key="label.uploadResume" var="uploadResume"/>
<html>
<head>
    <title>${applying}</title>
    <link rel="stylesheet" href="${pageContext.servletContext.contextPath}/css/bootstrap.min.css"/>
    <link rel="stylesheet" href="${pageContext.servletContext.contextPath}/css/registration.css"/>
</head>
<body>
<c:choose>
    <c:when test="${(not empty sessionScope.user)and (sessionScope.user.userType=='CANDIDATE') }">
        <div class="container">
            <div id="wrapper">
                <div class="overlay"></div>
                <%@ include file="../leftMenu.jsp" %>
                <div id="page-content-wrapper">
                    <button type="button" class="hamburger is-closed" data-toggle="offcanvas">
                        <span class="hamb-top"></span>
                        <span class="hamb-middle"></span>
                        <span class="hamb-bottom"></span>
                    </button>
                    <div class="container">
                        <h1 class="title">${applying}</h1>
                        <div id="login">
                            <form method="POST" action="newApplication" enctype="multipart/form-data">
                                <input type="hidden" name="vacancyId" value="${param.vacancyId}"/>
                                <label for="covering-letter"><p>${coveringLetter}</p></label>
                                <textarea class="form-control" name="covering_letter" rows="5"
                                          id="covering-letter" maxlength="2000"></textarea>
                                <label for="resume"><p>${uploadResume} ${choseFile} PDF</p></label>
                                <input type="file" id="resume" name="file" required accept="application/pdf">
                                <input type="submit" class="btn btn-secondary" value="${apply}">
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </c:when>
    <c:otherwise>
        <jsp:forward page="${pageContext.servletContext.contextPath}/error/error403.jsp"/>
    </c:otherwise>
</c:choose>
</body>
</html>
