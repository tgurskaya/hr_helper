<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="ctg" uri="customtags" %>
<c:set var="currentPage" value="${(requestScope.pageNumber==null) ? 1 : requestScope.pageNumber}" scope="page"/>
<c:set var="lastPage" value="${(requestScope.pageCount==null) ? 1 : requestScope.pageCount}" scope="page"/>
<c:set var="vacancyList" value="${requestScope.vacancyList}" scope="page"/>
<c:set var="language"
       value="${not empty param.language ? param.language : not empty language ? language : pageContext.request.locale}"
       scope="session"/>
<fmt:setLocale value="${language}" scope="session"/>
<fmt:setBundle basename="property.label" var="loc"/>
<fmt:message bundle="${loc}" key="label.requirements" var="requirements"/>
<fmt:message bundle="${loc}" key="label.responsibilities" var="responsibilities"/>
<fmt:message bundle="${loc}" key="label.salary" var="salary"/>
<fmt:message bundle="${loc}" key="label.vacancyStatus" var="status"/>
<fmt:message bundle="${loc}" key="label.hrVacancies" var="hrVacancies"/>
<fmt:message bundle="${loc}" key="label.changeVacancy" var="changeVacancy"/>
<fmt:message bundle="${loc}" key="label.vacancyName" var="vacancyName"/>
<fmt:message bundle="${loc}" key="label.description" var="description"/>
<fmt:message bundle="${loc}" key="button.deleteVacancy" var="deleteVacancy"/>
<fmt:message bundle="${loc}" key="button.viewApplications" var="viewApplications"/>
<fmt:message bundle="${loc}" key="label.close" var="close"/>
<script src="https://code.jquery.com/jquery.min.js"></script>
<link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>

<c:if test="${vacancyList.size() > 0}">
    <h1>${hrVacancies}</h1>
    <div class="table-responsive">
    <table class="table table-hover ">
        <thead>
        <tr>
            <th>${vacancyName}</th>
            <th>${salary}</th>
            <th>${description}</th>
            <th>${requirements}</th>
            <th>${responsibilities}</th>
            <th>${status}</th>
        </tr>
        </thead>
        <tbody>

        <c:forEach var="i" begin="0" end="${vacancyList.size()-1}">
            <tr>
                <td>${vacancyList.get(i).name}
                    <a class="btn btn-success btn-block"
                       href="controller?command=view_vacancy_applications&vacancyId=${vacancyList.get(i).id}">${viewApplications}
                    </a>
                    <button class="btn btn-warning btn-block" data-toggle="modal"
                            data-vacancy-id="${vacancyList.get(i).id}"
                            data-vacancy-name="${vacancyList.get(i).name}"
                            data-vacancy-salary="${vacancyList.get(i).salary}"
                            data-vacancy-requirements="${vacancyList.get(i).requirements}"
                            data-vacancy-responsibilities="${vacancyList.get(i).responsibilities}"
                            data-vacancy-description="${vacancyList.get(i).description}"
                            data-vacancy-status="${vacancyList.get(i).status}"
                            data-target="#changeVacancyModal">
                            ${changeVacancy}
                    </button>
                    <a class="btn btn-danger btn-block"
                       href="controller?command=delete_vacancy&vacancyId=${vacancyList.get(i).id}">${deleteVacancy}
                    </a>
                </td>
                <td>${vacancyList.get(i).salary}</td>
                <td>${vacancyList.get(i).description}</td>
                <td>${vacancyList.get(i).requirements}</td>
                <td>${vacancyList.get(i).responsibilities}</td>
                <td>${vacancyList.get(i).status}</td>
            </tr>
        </c:forEach>
        </tbody>
    </table>
    </div>
    <nav aria-label="Page navigation example">
        <ul class="pagination justify-content-center">
            <c:choose>
                <c:when test="${currentPage==1}">
                    <li class="page-item disabled">
                        <a class="page-link" href="#" aria-label="Previous">
                            <span aria-hidden="true">&laquo;</span>
                            <span class="sr-only">Previous</span>
                        </a>
                    </li>
                </c:when>
                <c:otherwise>
                    <li class="page-item">
                        <a class="page-link"
                           href="controller?command=all_hr_vacancies&pageNumber=${currentPage-1}"
                           aria-label="Previous">
                            <span aria-hidden="true">&laquo;</span>
                            <span class="sr-only">Previous</span>
                        </a>
                    </li>
                </c:otherwise>
            </c:choose>
            <c:forEach begin="${(currentPage-3<0) ? 1 : currentPage-1}"
                       end="${(currentPage+1>lastPage) ? lastPage : currentPage+1}"
                       var="i">
                <c:choose>
                    <c:when test="${currentPage eq i}">
                        <li class="page-item active">
                            <a class="page-link"
                               href="controller?command=all_hr_vacancies&pageNumber=${i}">${i}</a>
                        </li>
                    </c:when>
                    <c:otherwise>
                        <li class="page-item">
                            <a class="page-link"
                               href="controller?command=all_hr_vacancies&pageNumber=${i}">${i}</a>
                        </li>
                    </c:otherwise>
                </c:choose>
            </c:forEach>
            <c:choose>
                <c:when test="${currentPage eq lastPage}">
                    <li class="page-item disabled">
                        <a class="page-link" href="#" aria-label="Next">
                            <span aria-hidden="true">&raquo;</span>
                            <span class="sr-only">Next</span>
                        </a>
                    </li>
                </c:when>
                <c:otherwise>
                    <li class="page-item">
                        <a class="page-link"
                           href="controller?command=all_hr_vacancies&pageNumber=${currentPage+1}"
                           aria-label="Next">
                            <span aria-hidden="true">&raquo;</span>
                            <span class="sr-only">Next</span>
                        </a>
                    </li>
                </c:otherwise>
            </c:choose>
        </ul>
    </nav>
</c:if>

<div class="modal fade" id="changeVacancyModal" tabindex="-1" role="dialog"
     aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <form method="POST" action="controller" >
                <div class="modal-header">
                    <h5 class="modal-title" id="changeVacancyLabel">${changeVacancy}</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
                <div class="modal-body">
                    <input type="hidden" name="command" value="change_vacancy">
                    <input type="hidden" name="vacancyId" value="">
                    <label for="name"><p>${vacancyName}</p></label>
                    <input type="text" maxlength="45" name="name" id="name" required>
                    <label for="salary"><p>${salary}</p></label>
                    <input type="text" maxSize="15" name="salary" id="salary">
                    <label for="description"><p>${description}</p></label>
                    <textarea class="form-control" maxlength="2000" name="description" rows="5" id="description"
                              required></textarea>
                    <label for="responsibilities"><p>${responsibilities}</p></label>
                    <textarea class="form-control" maxlength="2000" name="responsibilities" rows="5"
                              id="responsibilities"></textarea>
                    <label for="requirements"><p>${requirements}</p></label>
                    <textarea class="form-control" name="requirements" rows="5"
                              id="requirements" maxlength="2000"></textarea>
                    <select name="status">
                        <option name="status" value="OPEN">OPEN</option>
                        <option name="status" value="PAUSED">PAUSED</option>
                        <option name="status" value="PAUSED">CLOSED</option>
                    </select>

                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-secondary" data-dismiss="modal">${close} </button>
                    <input type="submit" class="btn btn-secondary" value="${changeVacancy}">
                </div>
            </form>
            </form>
        </div>
    </div>
</div>

<script>
    $('#changeVacancyModal').on('show.bs.modal', function (e) {
        var vacancyId = $(e.relatedTarget).data('vacancy-id');
        $(e.currentTarget).find('input[name="vacancyId"]').val(vacancyId);
        var vacancyName = $(e.relatedTarget).data('vacancy-name');
        $(e.currentTarget).find('input[name="name"]').val(vacancyName);
        var salary = $(e.relatedTarget).data('vacancy-salary');
        $(e.currentTarget).find('input[name="salary"]').val(salary);
        var requirements = $(e.relatedTarget).data('vacancy-requirements');
        $(e.currentTarget).find('textarea[name="requirements"]').val(requirements);
        var responsibilities = $(e.relatedTarget).data('vacancy-responsibilities');
        $(e.currentTarget).find('textarea[name="responsibilities"]').val(responsibilities);
        var status = $(e.relatedTarget).data('vacancy-salary');
        $(e.currentTarget).find('select[name="status"]').val(status);
        var description = $(e.relatedTarget).data('vacancy-description');
        $(e.currentTarget).find('textarea[name="description"]').val(description);
    });
</script>