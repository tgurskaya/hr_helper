<%@ page contentType="text/html;charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="ctg" uri="customtags" %>
<c:set var="applicationList" value="${requestScope.applicationList}" scope="page"/>
<c:set var="currentPage" value="${(requestScope.pageNumber==null) ? 1 : requestScope.pageNumber}" scope="page"/>
<c:set var="lastPage" value="${(requestScope.pageCount==null) ? 1 : requestScope.pageCount}" scope="page"/>
<fmt:setBundle basename="property.label" var="loc"/>
<fmt:message bundle="${loc}" key="label.vacancyName" var="vacancyName"/>
<fmt:message bundle="${loc}" key="label.description" var="description"/>
<fmt:message bundle="${loc}" key="label.coveringLetter" var="coveringLetter"/>
<fmt:message bundle="${loc}" key="label.resume" var="resume"/>
<fmt:message bundle="${loc}" key="button.viewJobInterviews" var="viewJobInterviews"/>
<fmt:message bundle="${loc}" key="button.deleteApplication" var="deleteApplication"/>
<fmt:message bundle="${loc}" key="button.viewResume" var="viewResume"/>
<fmt:message bundle="${loc}" key="button.viewCandidateProfile" var="viewCandidateProfile"/>
<fmt:message bundle="${loc}" key="label.close" var="close"/>
<fmt:message bundle="${loc}" key="label.vacancyApplications" var="vacancyApplications"/>
<fmt:message bundle="${loc}" key="label.decision" var="decision"/>
<fmt:message bundle="${loc}" key="label.noApplications" var="noApplications"/>
<fmt:message bundle="${loc}" key="label.newDecision" var="newDecision"/>
<fmt:message bundle="${loc}" key="label.noApplications" var="noApplications"/>

<html>
<head>
    <title>${vacancyApplications}</title>
    <script src="https://code.jquery.com/jquery.min.js"></script>
    <link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" rel="stylesheet"
          type="text/css"/>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>
    <script src="../../../js/vacancy_applications.js"></script>
</head>
<body>
<c:choose>
    <c:when test="${(not empty sessionScope.user)and (sessionScope.user.userType=='HR') }">
        <div class="container">
            <div id="wrapper">
                <div class="overlay"></div>
                <%@ include file="../leftMenu.jsp" %>
                <div id="page-content-wrapper">
                    <button type="button" class="hamburger is-closed" data-toggle="offcanvas">
                        <span class="hamb-top"></span>
                        <span class="hamb-middle"></span>
                        <span class="hamb-bottom"></span>
                    </button>
                    <div class="container">
                        <c:choose>
                            <c:when test="${applicationList.size() > 0}">
                                <jsp:include page="tableVacancyApplications.jsp"/>
                            </c:when>
                            <c:otherwise>
                                <h1>${noApplications}</h1>
                            </c:otherwise>
                        </c:choose>
                    </div>
                </div>
            </div>
        </div>

    </c:when>
    <c:otherwise>
        <jsp:forward page="${pageContext.servletContext.contextPath}/error/error403.jsp"/>
    </c:otherwise>
</c:choose>
</body>
</html>


