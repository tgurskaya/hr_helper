<%@ page contentType="text/html;charset=UTF-8" pageEncoding="UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="ctg" uri="customtags" %>

<c:set var="jobInterviewList" value="${requestScope.jobInterviewList}" scope="page"/>
<c:set var="currentPage" value="${(requestScope.pageNumber==null) ? 1 : requestScope.pageNumber}" scope="page"/>
<c:set var="lastPage" value="${(requestScope.pageCount==null) ? 1 : requestScope.pageCount}" scope="page"/>
<c:set var="language"
       value="${not empty param.language ? param.language : not empty language ? language : pageContext.request.locale}"
       scope="session"/>
<fmt:setLocale value="${language}" scope="session"/>
<fmt:setBundle basename="property.label" var="loc"/>
<fmt:message bundle="${loc}" key="label.newJobInterview" var="newJobInterview"/>
<fmt:message bundle="${loc}" key="label.opinion" var="opinion"/>
<fmt:message bundle="${loc}" key="label.dateTimeOfInterview" var="dateTimeOfInterview"/>
<fmt:message bundle="${loc}" key="label.dateTimeOfInterview" var="dateTimeOfInterview"/>
<fmt:message bundle="${loc}" key="label.jobInterviewType" var="jobInterviewType"/>
<fmt:message bundle="${loc}" key="label.place" var="place"/>
<fmt:message bundle="${loc}" key="button.createJobInterview" var="createJobInterview"/>

<fmt:message bundle="${loc}" key="label.applicationInterviews" var="applicationInterviews"/>
<fmt:message bundle="${loc}" key="label.place" var="place"/>
<fmt:message bundle="${loc}" key="button.addJobInterview" var="addJobInterview"/>
<fmt:message bundle="${loc}" key="label.noInterviews" var="noInterviews"/>


<html>
<head>
    <title>${applicationInterviews}</title>
    <script src="https://code.jquery.com/jquery.min.js"></script>
    <link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css" rel="stylesheet"
          type="text/css"/>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>
    <script src="${pageContext.servletContext.contextPath}/js/jquery-3.2.1.min.js"></script>
    <link href="${pageContext.servletContext.contextPath}/css/bootstrap.css" rel="stylesheet" type="text/css"/>
    <link href="${pageContext.servletContext.contextPath}/css/bootstrap.min.css" rel="stylesheet" type="text/css">
</head>
<body>

<c:choose>
    <c:when test="${(not empty sessionScope.user)and ((sessionScope.user.userType=='HR') or(sessionScope.user.userType=='CANDIDATE')) }">
        <div class="container">
            <div id="wrapper">
                <div class="overlay"></div>
                <%@ include file="leftMenu.jsp" %>
                <div id="page-content-wrapper">
                    <button type="button" class="hamburger is-closed" data-toggle="offcanvas">
                        <span class="hamb-top"></span>
                        <span class="hamb-middle"></span>
                        <span class="hamb-bottom"></span>
                    </button>
                    <div class="container">

                        <c:if test="${(sessionScope.user.userType=='HR')}">
                        <a class="btn btn-info"
                           href="controller?command=add_job_interview&applicationId=${requestScope.applicationId}">
                                ${addJobInterview}
                        </a>
                        </c:if>

                        <c:choose>
                            <c:when test="${jobInterviewList.size() > 0}">
                                <jsp:include page="table_application_interview.jsp"/>
                            </c:when>
                            <c:otherwise>
                                <h1>${noInterviews}</h1>
                            </c:otherwise>
                        </c:choose>

                    </div>
                </div>
            </div>
        </div>
    </c:when>
    <c:otherwise>
        <jsp:forward page="${pageContext.servletContext.contextPath}/error/error403.jsp"/>
    </c:otherwise>
</c:choose>
</body>
</html>
</body>
</html>