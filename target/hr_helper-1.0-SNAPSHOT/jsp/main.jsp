<%@ page contentType="text/html;charset=UTF-8" %>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<c:set var="language"
       value="${not empty param.language ? param.language : not empty language ? language : pageContext.request.locale}"
       scope="session"/>
<fmt:setLocale value="${language}" scope="session"/>
<fmt:setBundle basename="property.label" var="loc"/>
<fmt:message bundle="${loc}" key="label.vacancy" var="vacancy"/>
<fmt:message bundle="${loc}" key="label.topTenVacancies" var="topTenVacancies"/>
<fmt:message bundle="${loc}" key="label.vacancyName" var="vacancyName"/>
<fmt:message bundle="${loc}" key="label.description" var="description"/>
<fmt:message bundle="${loc}" key="label.requirements" var="requirements"/>
<fmt:message bundle="${loc}" key="label.responsibilities" var="responsibilities"/>
<fmt:message bundle="${loc}" key="label.salary" var="salary"/>
<fmt:message bundle="${loc}" key="label.title" var="title"/>
<!DOCTYPE HTML>
<html lang="${language}">
<head>
    <meta charset="utf-8">
    <title>${title}</title>
    <link href="${pageContext.servletContext.contextPath}/css/bootstrap.css" rel="stylesheet">
    <link href="${pageContext.servletContext.contextPath}/css/bootstrap-theme.css" rel="stylesheet">
    <link href="${pageContext.servletContext.contextPath}/font-awesome-4.7.0/css/font-awesome.min.css" rel="stylesheet">
    <link href="${pageContext.servletContext.contextPath}/css/main.css" rel="stylesheet">
</head>
<body>
<div class="container">
    <c:choose>
        <c:when test="${sessionScope.user!=null}">
            <%@ include file="headerAuthorized.jsp" %>
        </c:when>
        <c:otherwise>
            <%@ include file="header.jsp" %>
        </c:otherwise>
    </c:choose>
    <div id="decorative1" style="position:relative">
        <div class="container">
            <div class="divPanel headerArea">
                <div class="row-fluid">
                    <div class="span12">
                        <div id="headerSeparator"></div>
                        <div id="divHeaderText" class="page-content">
                            <div id="divHeaderLine1">${title}</div>
                        </div>
                        <div id="headerSeparator2"></div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <h1>${topTenVacancies}</h1>
    <div class="row">
        <c:if test="${requestScope.vacancyList.size() > 0}">
            <c:forEach var="i" begin="0" end="${requestScope.vacancyList.size()-1}">
                <div class="col-sm-6">
                    <div class="card text-white bg-inverse mb-3">
                        <div class="card-header bg-inverse">
                                ${requestScope.vacancyList.get(i).name}
                        </div>
                        <div class="card-body">
                            <blockquote class="blockquote mb-0">
                                <p>${requestScope.vacancyList.get(i).description}</p>
                            </blockquote>
                        </div>
                    </div>
                </div>
            </c:forEach>
        </c:if>
    </div>
    <div class="navbar navbar-inverse">
        <ul class="socialIcons">
            <li><a href="https://www.facebook.com/EPAM.Belarus" class="fa fa-facebook" aria-hidden="true"></a></li>
            <li><a href="https://vk.com/epam.belarus" class="fa fa-vk" aria-hidden="true"></a></li>
            <li><a href="https://twitter.com/epam_minsk" class="fa fa-twitter" aria-hidden="true"></a></li>
            <li><a href="https://plus.google.com/+epamby" class="fa fa-google-plus" aria-hidden="true"></a></li>
        </ul>
    </div>
</div>
</div>
</body>
</html>
