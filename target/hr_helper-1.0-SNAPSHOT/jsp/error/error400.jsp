<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<%@ page isErrorPage="true" %>
<html>
<head>
    <link href="../../css/error.css" rel="stylesheet" type="text/css"/>
    <title>Error</title>
</head>
<body>
<div id="clouds">
    <div class="cloud x1"></div>
    <div class="cloud x1_5"></div>
    <div class="cloud x2"></div>
    <div class="cloud x3"></div>
    <div class="cloud x4"></div>
    <div class="cloud x5"></div>
</div>
<div class='c'>
    <div class='_404'>400</div>
    <hr>
    <div class='_1'>BAD REQUEST</div>
    <div class="_2">Message from exception: ${pageContext.exception.message}</div>
</div>
</body>
</html>
