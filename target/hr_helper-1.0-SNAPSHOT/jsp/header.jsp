<link href="../css/bootstrap.css" rel="stylesheet">
<link href="../css/bootstrap-theme.css" rel="stylesheet">
<link href="../css/header.css" rel="stylesheet">
<script src="../js/header.js" type="text/javascript"></script>

<fmt:setLocale value="${language}" scope="session"/>
<fmt:setBundle basename="property.label" var="loc"/>
<fmt:message bundle="${loc}" key="label.login" var="login"/>
<fmt:message bundle="${loc}" key="label.title" var="title"/>
<fmt:message bundle="${loc}" key="label.registration" var="registration"/>
<fmt:message bundle="${loc}" key="label.vacancy" var="vacancy"/>
<body>
<div class="navbar navbar-inverse navbar-fixed-top">
    <div class="container">
        <div class="navbar-inner">
            <div class="pull-left">
                <div class="topnav">
                    <a href="../index.jsp">
                        <header>${title}</header>
                    </a>
                </div>
            </div>
            <div class="pull-right">
                <div class="topnav" id="myTopnav">
                            <a href="../jsp/login.jsp">${login}</a>
                            <a href="../jsp/registration.jsp">${registration}</a>
                    <a href="javascript:void(0);" style="font-size:15px;" class="icon"
                       onclick="myFunction()">&#9776;</a>
                </div>
                <div class="flags">
                    <a href="${pageContext.request.requestURI}?language=en_US"> <img class="lang" src="../img/gbr_flag.png"></a>
                    <a href="${pageContext.request.requestURI}?language=ru_RU"><img src="../img/rus_flag.png" class="lang"></a>
                    <a href="${pageContext.request.requestURL}?language=be_BY"><img src="../img/bel_flag.png" class="lang"></a>
                </div>
            </div>
        </div>
    </div>
</div>


</body>