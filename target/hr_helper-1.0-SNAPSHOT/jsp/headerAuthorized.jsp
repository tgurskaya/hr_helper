<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<fmt:setLocale value="${language}" scope="session"/>
<fmt:setBundle basename="property.label" var="loc"/>
<fmt:message bundle="${loc}" key="label.title" var="title"/>
<fmt:message bundle="${loc}" key="label.logout" var="logout"/>
<fmt:message bundle="${loc}" key="label.home" var="home"/>
<fmt:message bundle="${loc}" key="label.vacancy" var="vacancy"/>
<html>
<head>
    <link href="${pageContext.servletContext.contextPath}/css/bootstrap.css" rel="stylesheet">
    <link href="${pageContext.servletContext.contextPath}/css/bootstrap-theme.css" rel="stylesheet">
    <link href="${pageContext.servletContext.contextPath}/css/header.css" rel="stylesheet">
    <script src="${pageContext.servletContext.contextPath}/js/header.js" type="text/javascript"></script>
</head>
<body>
<div class="navbar navbar-inverse navbar-fixed-top">
    <div class="container">
        <div class="navbar-inner">
            <div class="pull-left">
                <div class="topnav">
                    <a href="${pageContext.servletContext.contextPath}/index.jsp">
                        <header>${title}</header>
                    </a>
                </div>
            </div>
            <div class="pull-right">
                <div class="topnav" id="myTopnav">
                    <a href="${pageContext.servletContext.contextPath}/controller?command=logout">${logout}</a>
                    <a href="${pageContext.servletContext.contextPath}/controller?command=home">${home}</a>
                    <c:if test="${not sessionScope.user.blocked}">
                        <a href="${pageContext.servletContext.contextPath}/controller?command=all_open_vacancies&pageNumber=1">${vacancy}</a>
                    </c:if>
                    <a href="javascript:void(0);" style="font-size:15px;" class="icon"
                               onclick="myFunction()">&#9776;</a>
                </div>
                <div class="flags">
                    <a href="${pageContext.request.requestURL}?language=en_US"> <img class="lang"
                                                                                     src="${pageContext.servletContext.contextPath}/img/gbr_flag.png"></a>
                    <a href="${pageContext.request.requestURL}?language=ru_RU"><img
                            src="${pageContext.servletContext.contextPath}/img/rus_flag.png" class="lang"></a>
                    <a href="${pageContext.request.requestURL}?language=be_BY"><img
                            src="${pageContext.servletContext.contextPath}/img/bel_flag.png" class="lang"></a>
                </div>
            </div>
        </div>
    </div>
</div>
</body>
</html>